# Changelog for "storagehub"

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.1.0-SNAPSHOT]
- improved containerization 
- JackRabbit indexes removed
- regex-based search added

## [v2.0.1-SNAPSHOT]
- improved enunciate api-docs
- added sphinx docs
- incident solved [#28539]
- @Produces added/improved in some cases
- removed content-type constraint on group admin delete
- removed DocsManager, docs are now served by the default Tomcat servlet
- project structure refactored with docs in "standard" locations

## [v2.0.0]

 - ceph as default storage
 - vre folders can define specific bucket as backend
 - enunciate docs
 - dockerization of the service 

## [v1.4.0] 2021-10-07

- slow query removed from VRE retrieving and recents
- incident solved [#22184]

## [v1.3.2] - [2021-09-28]
- fix 22087

## [v1.3.1] - [2021-09-08]

- solved bug on attachment rights

## [v1.3.0] - [2021-03-31]

possibility to impersonate people added

## [v1.2.5] - [2021-03-11]

use of query (indexes are not working well in jackrabbit) to retrieve shared folder removed on delete user 

## [v1.2.4] - [2021-01-11]

mimetype set also on non parsable pdf

method add and remove admin on a VREFolder enabled also for admin role

fixed bug on unsharing folder on user removal 

added method exist user

## [v1.2.2] - [2020-10-12]

method for description update added

## [v1.2.1] - [2020-06-20]

bug on Archive uploader solved

## [v1.2.0] - [2020-04-15]

trash items changes owner on restore

restore with new destination folder added

move between shared and private or different shared folder enabled

## [v1.0.8] - [2019-09-20]

Bug on ushare owner fixed

## [v1.0.5] - [2019-04-04]

Active wait for lock in case of item creation added

## [v1.0.0] - [2015-07-01]

First commit
