FROM harbor.d4science.org/gcube/smartgears-distribution:4.0.1-SNAPSHOT-java17-tomcat10.1.19
   COPY ./target/storagehub.war /tomcat/webapps/
   COPY ./docker/storagehub.xml /tomcat/conf/Catalina/localhost/
