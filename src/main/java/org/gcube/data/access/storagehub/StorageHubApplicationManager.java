package org.gcube.data.access.storagehub;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.jcr.RepositoryException;

import org.apache.jackrabbit.api.JackrabbitSession;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.common.storagehub.model.exporter.DumpData;
import org.gcube.data.access.storagehub.handlers.DataHandler;
import org.gcube.data.access.storagehub.repository.StoragehubRepository;
import org.gcube.smartgears.ApplicationManager;
import org.gcube.smartgears.ContextProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StorageHubApplicationManager implements ApplicationManager {

	private static Logger logger = LoggerFactory.getLogger(StorageHubApplicationManager.class);

	private boolean alreadyShutDown = false;
	private boolean alreadyInit = false;

	
	private StoragehubRepository repository;

	// private static NotificationClient notificationClient;

	/*
	 * public static NotificationClient getNotificationClient() { return
	 * notificationClient; }
	 */

	public StorageHubApplicationManager() {
		repository = StoragehubRepository.repository;
		
	}

	@Override
	public synchronized void onInit() {

		logger.info("onInit Called on storagehub");
		try {
			if (!alreadyInit) {
				logger.info("jackrabbit initialization started");

				repository.initContainerAtFirstStart();
				
				DataHandler dh = new DataHandler();

				Path shubSpecificConf = ContextProvider.get().appSpecificConfigurationFolder();
				Path importPath = Paths.get(shubSpecificConf.toString(), "import");
				Path mainFileImportPath = Paths.get(importPath.toString(), "data.json");
				if (importPath.toFile().exists() && mainFileImportPath.toFile().exists()) {
					JackrabbitSession session = null;
					try {
						ObjectMapper mapper = new ObjectMapper();
						DumpData data = mapper.readValue(mainFileImportPath.toFile(), DumpData.class);

						session = (JackrabbitSession) repository.getRepository().login(Constants.JCR_CREDENTIALS);

						dh.importData((JackrabbitSession) session, data);
						session.save();
					} catch (RepositoryException e) {
						logger.error("error importing data", e);
					} catch (IOException je) {
						logger.error("error parsing json data, invalid schema file", je);
					} finally {
						if (session != null)
							session.logout();
					}
				}
				

				alreadyInit = true;
			}
		} catch (Throwable e) {
			logger.error("unexpected error initiliazing storagehub", e);
		}

	}

	@Override
	public synchronized void onShutdown() {
		if (!alreadyShutDown)
			try {
				logger.info("jackrabbit is shutting down");
				repository.shutdown();
				alreadyShutDown = true;
			} catch (Exception e) {
				logger.warn("the database was not shutdown properly", e);
			}
	}

}
