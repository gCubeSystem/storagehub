package org.gcube.data.access.storagehub.types;

import org.gcube.common.storagehub.model.items.nodes.Content;
import org.gcube.common.storagehub.model.storages.StorageBackend;

public class ContentPair {
	
	private Content content;
	private StorageBackend storageBackend;
	public ContentPair(Content content, StorageBackend storageBackend) {
		super();
		this.content = content;
		this.storageBackend = storageBackend;
	}
	public Content getContent() {
		return content;
	}
	public StorageBackend getStorageBackend() {
		return storageBackend;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((content.getStorageId() == null) ? 0 : content.getStorageId().hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContentPair other = (ContentPair) obj;
		if (content.getStorageId() == null) {
			if (other.content.getStorageId() != null)
				return false;
		} else if (!content.getStorageId().equals(other.content.getStorageId()))
			return false;
		return true;
	}
	
	
}
