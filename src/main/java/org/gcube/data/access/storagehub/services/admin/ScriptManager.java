package org.gcube.data.access.storagehub.services.admin;

import static org.gcube.data.access.storagehub.Roles.INFRASTRUCTURE_MANAGER_ROLE;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.UUID;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.jackrabbit.api.JackrabbitSession;
import org.gcube.common.authorization.control.annotations.AuthorizationControl;
import org.gcube.common.security.AuthorizedTasks;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.storagehub.model.Paths;
import org.gcube.common.storagehub.model.exporter.DumpData;
import org.gcube.data.access.storagehub.Constants;
import org.gcube.data.access.storagehub.PathUtil;
import org.gcube.data.access.storagehub.accounting.AccountingHandler;
import org.gcube.data.access.storagehub.handlers.DataHandler;
import org.gcube.data.access.storagehub.handlers.items.ItemHandler;
import org.gcube.data.access.storagehub.handlers.items.builders.FileCreationParameters;
import org.gcube.data.access.storagehub.handlers.items.builders.ItemsParameterBuilder;
import org.gcube.data.access.storagehub.repository.StoragehubRepository;
import org.gcube.data.access.storagehub.scripting.AbstractScript;
import org.gcube.data.access.storagehub.scripting.ScriptUtil;
import org.gcube.data.access.storagehub.services.admin.ScriptStatus.Status;
import org.gcube.smartgears.ContextProvider;
import org.gcube.smartgears.context.application.ApplicationContext;
import org.gcube.smartgears.utils.InnerMethodName;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webcohesion.enunciate.metadata.DocumentationExample;
import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResourceMethodSignature;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;

/**
 * Manage "script" classes
 */
@Path("admin/script")
@RequestHeaders({
	  @RequestHeader( name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>"),
	})
public class ScriptManager {


	private static Logger log = LoggerFactory.getLogger(ScriptManager.class);

	private final StoragehubRepository repository = StoragehubRepository.repository;

	@Inject 
	AccountingHandler accountingHandler;

	@Inject
	ScriptUtil scriptUtil;

	@Inject
	ItemHandler itemHandler;

	@Inject
	DataHandler dataHandler;
	
	
	@Inject
	PathUtil pathUtil;

	protected static HashMap<String, ScriptStatus> scriptStatusMap = new HashMap<String, ScriptStatus>();

	/**
	 * Execute a "script" class <br>
	 * <strong>Only users with <code>Infrastructure-Manager</code> role allowed</strong>
	 * 
	 * @param name name of the script
	 * @param asynch if true, execute script asynchronously <br>
	 *   <strong>Possible values:</strong> <code>true</code>, <code>false</code> <br>
	 *   <strong>Optional</strong> default: <code>false</code>
	 * @param writeResult if true, write the result in the workspace <br>
	 *   <strong>Possible values:</strong> <code>true</code>, <code>false</code> <br>
	 *   <strong>Optional</strong> default: <code>false</code>
	 * @param destinationFolderId id of the destination folder
	 * @param file  multipart/form-data file parameter, with optional
	 *              'filename' and 'size' (see example below)
	 * @return outcome of the script execution
	 */
	@ResourceMethodSignature(output = ScriptStatus.class, formParams = { @FormParam("name"), 
		@FormParam("asynch"), @FormParam("writeResult"), @FormParam("destinationFolderId"), 
		@FormParam("file") })
	@DocumentationExample(value = "...\n\n--------boundaryString\n" +
		"Content-Disposition: form-data; name=\"file\"; filename=\"MoveFiles.class\"; size=171018;\n" +
		"Content-Type: application/octet-stream\n\n" +
		"(binary data)\n" +
		"--------boundaryString\n" +
		"Content-Disposition: form-data; name=\"name\"\n\n" +
		"MoveFile\n" +
		"--------boundaryString\n" +
		"Content-Disposition: form-data; name=\"async\"\n\n" +
		"false\n" +
		"--------boundaryString\n" +
		"Content-Disposition: form-data; name=\"writeResult\"\n\n" +
		"true\n" +
		"--------boundaryString\n" +
		"Content-Disposition: form-data; name=\"destinationFolderId=\"\n\n" +
		"5f4b3b4e-4b3b- ... -4b3b4e4b3b4e\n" +
		"--------boundaryString--")
	@POST
	@Path("execute")
	@AuthorizationControl(allowedRoles = {INFRASTRUCTURE_MANAGER_ROLE})
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "Script correctly loaded."),
		@ResponseCode ( code = 403, condition = "You're not allowed to run scripts."),
		@ResponseCode ( code = 500, condition = "Error loading the script."),
	})
	public ScriptStatus run( @FormDataParam("name") String name,
			@FormDataParam("asynch") @DefaultValue("false") Boolean asynch,
			@FormDataParam("writeResult") @DefaultValue("false") Boolean writeResult ,
			@FormDataParam("destinationFolderId") String destinationFolderId,
			@FormDataParam("file") InputStream file,
			@FormDataParam("file") FormDataContentDisposition fileDetail) {
		try {
			InnerMethodName.set("executeScript");
			ScriptClassLoader scriptClassLoader = new ScriptClassLoader(Thread.currentThread().getContextClassLoader());
			Class<?> scriptClass = uploadClass(file, scriptClassLoader, fileDetail.getFileName().replace(".class", ""));
			return internalRun(scriptClass, name, destinationFolderId, asynch, writeResult);
		}catch(Throwable e) {
			log.error("error executing script {}", name,e);
			throw new WebApplicationException("error loading class",e);
		}
	}

	/**
	 * Get the status of a script <br>
	 * <strong>Only users with <code>Infrastructure-Manager</code> role allowed</strong>
	 * 
	 * @param id id of the script
	 * @return outcome of the script execution
	 */
	@ResourceMethodSignature(output = ScriptStatus.class, pathParams = { @PathParam("id") })
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "Script status retrieved."),
		@ResponseCode ( code = 403, condition = "You're not allowed to inquire script status."),
		@ResponseCode ( code = 404, condition = "Script not found."),
	})
	@GET
	@Path("{id}/status")
	@AuthorizationControl(allowedRoles = {INFRASTRUCTURE_MANAGER_ROLE})
	@Produces(MediaType.APPLICATION_JSON)
	public ScriptStatus getStatus(@PathParam("id") String runningId) {
		InnerMethodName.set("getScriptStatus");
		if (!scriptStatusMap.containsKey(runningId)) {
			log.error("script with id {} not found",runningId);
			throw new WebApplicationException("id "+runningId+" not found", 404);
		}
		ScriptStatus status = scriptStatusMap.get(runningId);
		if (status.getStatus()!= Status.Running)
			scriptStatusMap.remove(runningId);
		return status;	
	}


	/**
	 * Export the data <br>
	 * <strong>Only users with <code>Infrastructure-Manager</code> role allowed</strong>
	 * 
	 * @return outcome of the export
	 */
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "Script status retrieved."),
		@ResponseCode ( code = 403, condition = "You're not allowed to inquire script status."),
		@ResponseCode ( code = 404, condition = "Script not found."),
	})
	@GET
	@Path("export")
	@AuthorizationControl(allowedRoles = {INFRASTRUCTURE_MANAGER_ROLE})
	@Produces(MediaType.APPLICATION_JSON)
	public ScriptStatus export() {
		InnerMethodName.set("export");
		
		try {
			
			String runningId = UUID.randomUUID().toString();
			ApplicationContext appContext = ContextProvider.get();
			String serverHost = appContext.container().configuration().hostname();
			final ScriptStatus status = new ScriptStatus(runningId, null, serverHost);
			scriptStatusMap.put(runningId, status);
			Runnable execution = new Runnable() {
				
				@Override
				public void run() {
					JackrabbitSession session = null;
					try {
						session = (JackrabbitSession) repository.getRepository().login(Constants.JCR_CREDENTIALS);
						DumpData dd = dataHandler.exportData(session);
						ObjectMapper om = new ObjectMapper();
						File result = Files.createTempFile("export",".json").toFile();
						status.setSuccess(result.getAbsolutePath());
						om.writeValue(result, dd);
					} catch (Throwable t) {
						StringWriter sw = new StringWriter();
						PrintWriter pw = new PrintWriter(sw, true);
						t.printStackTrace(pw);
						status.setFailed(sw.toString());
						log.warn("export failed", t);
					}  finally {
						if (session!=null)
							session.logout();
					}
					
				}
			};
			
			new Thread(execution).start();
			
			return status;
			
		}catch(Exception e) {
			throw new WebApplicationException("error starting export", e);
		}
	}

	private Class<?> uploadClass(InputStream stream, ScriptClassLoader classLoader, String name) throws Throwable {
		try(ByteArrayOutputStream buffer = new ByteArrayOutputStream()){
			int nRead;
			byte[] data = new byte[1024];
			while ((nRead = stream.read(data, 0, data.length)) != -1) 
				buffer.write(data, 0, nRead);

			buffer.flush();
			byte[] byteArray = buffer.toByteArray();
			return classLoader.findClass(name, byteArray);
		}

	}

	private ScriptStatus internalRun(Class<?> clazz, String name, String destinationFolderId, boolean asynch, boolean writeResult) throws Throwable {
		String login = SecretManagerProvider.get().getOwner().getId();
		log.info("script {} called by {}", clazz.getSimpleName(), login);
		try {


			String resultPath= null;
			Node parentNode = null;

			if (writeResult) {
				JackrabbitSession ses = null;
				try {
					ses = (JackrabbitSession) repository.getRepository().login(Constants.JCR_CREDENTIALS);
					String parentId = destinationFolderId!=null ? destinationFolderId : ses.getNode(pathUtil.getWorkspacePath(login).toPath()).getIdentifier();
					parentNode = ses.getNodeByIdentifier(parentId);
					resultPath = Paths.append(Paths.getPath(parentNode.getPath()), name).toPath();
				}finally {
					if (ses!=null)
						ses.logout();
				}
			}

			if (AbstractScript.class.isAssignableFrom(clazz)) {
				AbstractScript scriptInstance = (AbstractScript) clazz.getDeclaredConstructor().newInstance();

				ApplicationContext appContext = ContextProvider.get();
				String serverHost = appContext.container().configuration().hostname();


				String runningId = UUID.randomUUID().toString();
				ScriptStatus status = new ScriptStatus(runningId, resultPath, serverHost);

				RealRun realRun = new RealRun(scriptInstance, login, parentNode, name, writeResult, status);
				if (asynch) {  
					scriptStatusMap.put(runningId, status);
					new Thread(AuthorizedTasks.bind(realRun)).start();
					return status;
				}else {
					realRun.run();
					return status;					
				}

			} else throw new Exception("class "+clazz.getSimpleName()+" not implements AbstractScript");



		}catch (Throwable e) {
			throw e;
		}


	}


	class RealRun implements Runnable{

		AbstractScript instance;
		String login;
		Node parentNode;
		String name;
		boolean writeResult = true;
		ScriptStatus status;

		public RealRun(AbstractScript instance, String login, Node parentNode, String name, boolean writeResult, ScriptStatus status) {
			super();
			this.instance = instance;
			this.login = login;
			this.parentNode = parentNode;
			this.name = name;
			this.writeResult = writeResult;
			this.status = status;
		}


		@Override
		public void run() {
			String result ="";
			try {
				JackrabbitSession executeSession = null;
				try {
					executeSession = (JackrabbitSession) repository.getRepository().login(Constants.JCR_CREDENTIALS);
					result = instance.run(executeSession, null, scriptUtil);
					status.setSuccess(result);
				}catch(Throwable t) {
					StringWriter sw = new StringWriter();
					PrintWriter pw = new PrintWriter(sw, true);
					t.printStackTrace(pw);
					status.setFailed(sw.toString());
					result+= "\n"+sw.toString();
					log.warn("error executing script {}",instance.getClass().getSimpleName(), t);
				}finally {
					if (executeSession !=null && executeSession.isLive())
						executeSession.logout();
				}

				if (this.writeResult) {
					Session writeSession = null;
					try( InputStream stream = new ByteArrayInputStream(result.getBytes())){
						writeSession = repository.getRepository().login(Constants.JCR_CREDENTIALS);
						ItemsParameterBuilder<FileCreationParameters> builder = FileCreationParameters.builder().name(name).description("result of script execution "+name)
								.stream(stream).on(parentNode.getIdentifier()).with(writeSession).author(login);
						itemHandler.create(builder.build());
					} catch (Throwable e) {
						log.error("error saving script result {} in the Workspace",name, e);
					} finally {
						if (writeSession!=null)
							writeSession.logout();
					}
				}
			}catch (Exception e) {
				log.error("unexpected error executing script {}",instance.getClass().getSimpleName(),e);
			}

		}
	}

	class ScriptClassLoader extends ClassLoader{

		public ScriptClassLoader(ClassLoader parent) {
			super(parent);
		}

		public Class<?> findClass(String name, byte[] b) {
			return defineClass(name, b, 0, b.length);
		}
	}

}



