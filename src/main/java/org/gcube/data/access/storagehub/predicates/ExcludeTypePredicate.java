package org.gcube.data.access.storagehub.predicates;

import java.util.Collections;
import java.util.List;

import org.gcube.common.storagehub.model.items.RootItem;

public class ExcludeTypePredicate implements ItemTypePredicate {

	List<Class<? extends RootItem>> classes;
	
	public ExcludeTypePredicate(List<Class<? extends RootItem>> classes) {
		this.classes = classes;
	}
	
	public ExcludeTypePredicate(Class<? extends RootItem> clazz) {
		this.classes = Collections.singletonList(clazz);
	}
	
	@Override
	public boolean test(Class<? extends RootItem> t) {
		for (Class<? extends RootItem> _class : classes)
			if (_class.isAssignableFrom(t)) return false;
		return true;

	}

}
