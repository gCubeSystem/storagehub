package org.gcube.data.access.storagehub.handlers.items.builders;

import java.util.Objects;

import javax.jcr.Session;

import org.gcube.common.storagehub.model.exceptions.InvalidCallParameters;

public abstract class ItemsParameterBuilder<T extends CreateParameters> {

	T parameters;
	
	protected ItemsParameterBuilder(T parameters) {
		super();
		this.parameters = parameters;
	}
	
	public ItemsParameterBuilder<T> on(String parentId) {
		parameters.parentId = parentId;
		return this;
	}
	
	public ItemsParameterBuilder<T> author(String author) {
		parameters.user = author;
		return this;
	}
	
	public ItemsParameterBuilder<T> with(Session session){
		parameters.session = session;
		return this;
	}
	
	public T build() throws InvalidCallParameters {
		if (!(parameters.isValid() && Objects.nonNull(parameters.parentId)))
			throw new InvalidCallParameters("invalid call");
		return parameters;
	}
	
}
