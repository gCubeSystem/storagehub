package org.gcube.data.access.storagehub.handlers;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.zip.Deflater;
import java.util.zip.ZipOutputStream;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.version.Version;

import org.apache.commons.io.FilenameUtils;
import org.gcube.common.storagehub.model.Excludes;
import org.gcube.common.storagehub.model.Paths;
import org.gcube.common.storagehub.model.exceptions.InvalidItemException;
import org.gcube.common.storagehub.model.exceptions.PluginInitializationException;
import org.gcube.common.storagehub.model.exceptions.PluginNotFoundException;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.exceptions.StorageIdNotFoundException;
import org.gcube.common.storagehub.model.items.AbstractFileItem;
import org.gcube.common.storagehub.model.items.FolderItem;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.common.storagehub.model.items.nodes.Content;
import org.gcube.common.storagehub.model.items.nodes.PayloadBackend;
import org.gcube.common.storagehub.model.storages.StorageBackend;
import org.gcube.common.storagehub.model.storages.StorageBackendFactory;
import org.gcube.common.storagehub.model.storages.StorageNames;
import org.gcube.data.access.storagehub.SingleFileStreamingOutput;
import org.gcube.data.access.storagehub.accounting.AccountingHandler;
import org.gcube.data.access.storagehub.handlers.items.Node2ItemConverter;
import org.gcube.data.access.storagehub.handlers.plugins.StorageBackendHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.StreamingOutput;

@Singleton
public class DownloadHandler {

	private static final Logger log = LoggerFactory.getLogger(DownloadHandler.class);
	
	@Inject 
	private AccountingHandler accountingHandler;
	
	@Inject
	private StorageBackendHandler storageBackendHandler;
	
	@Inject
	private CompressHandler compressHandler;

	@Inject
	private VersionHandler versionHandler;
	
	@Inject 
	private Node2ItemConverter node2Item;
	
	public Response downloadFolderItem(Session ses, String login, FolderItem item, boolean withAccounting ) throws StorageHubException, RepositoryException {
		try {	
			final Deque<Item> allNodes = compressHandler.getAllNodesForZip((FolderItem)item, login, ses, accountingHandler, Excludes.GET_ONLY_CONTENT);
			final org.gcube.common.storagehub.model.Path originalPath = Paths.getPath(item.getParentPath());
			StreamingOutput so = new StreamingOutput() {

				@Override
				public void write(OutputStream os) {

					try(ZipOutputStream zos = new ZipOutputStream(os)){
						long start = System.currentTimeMillis();
						zos.setLevel(Deflater.BEST_COMPRESSION);
						log.debug("writing StreamOutput");
						compressHandler.zipNode(zos, allNodes, originalPath);	
						log.debug("StreamOutput written in {}",(System.currentTimeMillis()-start));
					} catch (Exception e) {
						log.error("error writing stream",e);
					}

				}
			};

			Response response = Response
					.ok(so)
					.header("content-disposition","attachment; filename = "+item.getTitle()+".zip")
					.header("Content-Type", "application/zip")
					.header("Content-Length", -1l)
					.build();

			if (withAccounting)
				accountingHandler.createReadObj(item.getTitle(), null, ses, (Node) item.getRelatedNode(), login, false);
			return response;
		}finally {
			if (ses!=null) ses.save();
		}
	}
	
	
	public Response downloadFileItem(Session ses, AbstractFileItem fileItem, String login, boolean withAccounting) throws RepositoryException, PluginInitializationException, PluginNotFoundException, StorageHubException {

		Content content = fileItem.getContent();

		StorageBackendFactory sbf = storageBackendHandler.get(content.getPayloadBackend());

		StorageBackend sb = sbf.create(content.getPayloadBackend());

		InputStream streamToWrite = sb.download(content);

		if (withAccounting) {
			String versionName = null;
			try {
				Version version = versionHandler.getCurrentVersion((Node) fileItem.getRelatedNode());
				versionName = version.getName();
			}catch(RepositoryException e) {
				log.warn("current version of {} cannot be retreived", fileItem.getId());
			}
			accountingHandler.createReadObj(fileItem.getTitle(), versionName, ses,  (Node) fileItem.getRelatedNode(), login, true);
		}
		StreamingOutput so = new SingleFileStreamingOutput(streamToWrite);

		return Response
				.ok(so)
				.header("content-disposition","attachment; filename = "+fileItem.getName())
				.header("Content-Length", fileItem.getContent().getSize())
				.header("Content-Type", fileItem.getContent().getMimeType())
				.build();

	}
	
	public Response downloadVersionedItem(Session ses, String login, AbstractFileItem currentItem, String versionName, boolean withAccounting) throws RepositoryException, StorageHubException{
		

		List<Version> jcrVersions = versionHandler.getContentVersionHistory((Node)currentItem.getRelatedNode());

		for (Version version: jcrVersions) {
			log.debug("retrieved version id {}, name {}", version.getIdentifier(), version.getName());
			if (version.getName().equals(versionName)) {
				Content content = node2Item.getContentFromVersion(version);

				StorageBackendFactory sbf = storageBackendHandler.get(content.getPayloadBackend());
				StorageBackend sb = sbf.create(content.getPayloadBackend());

				InputStream streamToWrite = null;
				try { 
					streamToWrite = sb.download(content);
				}catch (StorageIdNotFoundException e) {
					//TODO: temporary code, it will last until the MINIO porting will not finish
					if (sbf.getName().equals(StorageNames.MONGO_STORAGE)) {
						sbf = storageBackendHandler.get(StorageNames.DEFAULT_S3_STORAGE);
						sbf.create(new PayloadBackend(StorageNames.DEFAULT_S3_STORAGE, null));
					} else 
						throw e;
				}
				
				log.debug("retrieved storage id is {} with storageBackend {} (stream is null? {})",content.getStorageId(), sbf.getName(), streamToWrite==null );

				String oldfilename = FilenameUtils.getBaseName(currentItem.getTitle());
				String ext = FilenameUtils.getExtension(currentItem.getTitle());

				String fileName = String.format("%s_v%s.%s", oldfilename, version.getName(), ext);

				if (withAccounting)
					accountingHandler.createReadObj(currentItem.getTitle(), versionName, ses,  (Node) currentItem.getRelatedNode(), login, true);
				

				StreamingOutput so = new SingleFileStreamingOutput(streamToWrite);

				return Response
						.ok(so)
						.header("content-disposition","attachment; filename = "+fileName)
						.header("Content-Length", content.getSize())
						.header("Content-Type", content.getMimeType())
						.build();
			}
		}
		throw new InvalidItemException("the version is not valid");
	}
	
	
	public Response downloadFileFromStorageBackend(String storageId, String storageName) throws RepositoryException, PluginInitializationException, PluginNotFoundException, StorageHubException {

		StorageBackendFactory sbf = storageBackendHandler.get(storageName);

		StorageBackend sb = sbf.create(new PayloadBackend(storageName, null));

		InputStream streamToWrite = sb.download(storageId);

		Map<String, String> userMetadata = sb.getFileMetadata(storageId);

		log.info("returned metadata from storageBackend are: {}", userMetadata);
		
		long size = Long.parseLong(userMetadata.get("size"));
		
		String title = userMetadata.get("title");
		String contentType = userMetadata.get("content-type");
				
		StreamingOutput so = new SingleFileStreamingOutput(streamToWrite);

		return Response
				.ok(so)
				.header("content-disposition","attachment; filename = "+title)
				.header("Content-Length", size)
				.header("Content-Type", contentType)
				.build();

	}
	
	
}
