package org.gcube.data.access.storagehub;

import java.util.Iterator;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;

import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.data.access.storagehub.handlers.ClassHandler;

public class NodeChildrenFilterIterator implements Iterator<Node> {

	private NodeIterator it;

	public NodeChildrenFilterIterator(Node node) throws BackendGenericError{
		super();
		try {
			it = node.getNodes();
		} catch (RepositoryException e) {
			throw new BackendGenericError(e);
		}
	}

	public NodeChildrenFilterIterator(NodeIterator iterator) throws BackendGenericError{
		it = iterator;
		
	}
	
	private Node currentNode = null;

	@Override
	public boolean hasNext() {
		try { 
			while (it.hasNext()) {
				currentNode=it.nextNode();
				if (ClassHandler.instance().get(currentNode.getPrimaryNodeType().getName())!=null) 
					return true;
			}
			return false;			
		}catch (RepositoryException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Node next() {
		return currentNode;
	}

}
