package org.gcube.data.access.storagehub.handlers.items;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.lock.LockException;
import javax.jcr.version.Version;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.detect.Detector;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.gcube.common.storagehub.model.Excludes;
import org.gcube.common.storagehub.model.NodeConstants;
import org.gcube.common.storagehub.model.Paths;
import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.IdNotFoundException;
import org.gcube.common.storagehub.model.exceptions.InvalidCallParameters;
import org.gcube.common.storagehub.model.exceptions.InvalidItemException;
import org.gcube.common.storagehub.model.exceptions.ItemLockedException;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.items.AbstractFileItem;
import org.gcube.common.storagehub.model.items.FolderItem;
import org.gcube.common.storagehub.model.storages.MetaInfo;
import org.gcube.common.storagehub.model.storages.StorageBackend;
import org.gcube.common.storagehub.model.storages.StorageBackendFactory;
import org.gcube.common.storagehub.model.types.ItemAction;
import org.gcube.data.access.storagehub.AuthorizationChecker;
import org.gcube.data.access.storagehub.Utils;
import org.gcube.data.access.storagehub.accounting.AccountingHandler;
import org.gcube.data.access.storagehub.handlers.VersionHandler;
import org.gcube.data.access.storagehub.handlers.content.ContentHandler;
import org.gcube.data.access.storagehub.handlers.content.ContentHandlerFactory;
import org.gcube.data.access.storagehub.handlers.items.builders.ArchiveStructureCreationParameter;
import org.gcube.data.access.storagehub.handlers.items.builders.CreateParameters;
import org.gcube.data.access.storagehub.handlers.items.builders.FileCreationParameters;
import org.gcube.data.access.storagehub.handlers.items.builders.FolderCreationParameters;
import org.gcube.data.access.storagehub.handlers.items.builders.GCubeItemCreationParameters;
import org.gcube.data.access.storagehub.handlers.items.builders.URLCreationParameters;
import org.gcube.data.access.storagehub.handlers.plugins.StorageBackendHandler;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class ItemHandler {

	@Inject
	AccountingHandler accountingHandler;

	@Inject
	ContentHandlerFactory contenthandlerFactory;

	@Inject
	AuthorizationChecker authChecker;

	@Inject
	VersionHandler versionHandler;

	@Inject
	StorageBackendHandler storageBackendHandler;

	// private static ExecutorService executor = Executors.newFixedThreadPool(100);

	@Inject
	Node2ItemConverter node2Item;
	@Inject
	Item2NodeConverter item2Node;

	private static Logger log = LoggerFactory.getLogger(ItemHandler.class);

	// TODO: accounting
	// provider URI host
	// resourceOwner user
	// consumer write
	// in caso di versione - update con -delta
	public <T extends CreateParameters> String create(T parameters) throws Exception {
		Session ses = parameters.getSession();

		Node destination;
		try {
			destination = ses.getNodeByIdentifier(parameters.getParentId());
		} catch (RepositoryException inf) {
			throw new IdNotFoundException(parameters.getParentId());
		}

		if (!node2Item.checkNodeType(destination, FolderItem.class))
			throw new InvalidItemException("the destination item is not a folder");

		authChecker.checkWriteAuthorizationControl(ses, parameters.getUser(), destination.getIdentifier(), true);

		try {
			Node newNode = switch (parameters.getMangedType()) {
                case FILE -> create((FileCreationParameters) parameters, destination);
                case FOLDER -> create((FolderCreationParameters) parameters, destination);
                case ARCHIVE -> create((ArchiveStructureCreationParameter) parameters, destination);
                case URL -> create((URLCreationParameters) parameters, destination);
                case GCUBEITEM -> create((GCubeItemCreationParameters) parameters, destination);
                default -> throw new InvalidCallParameters("Item not supported");
            };
            log.debug("item with id {} correctly created", newNode.getIdentifier());
			return newNode.getIdentifier();
		} finally {
			if (parameters.getSession().getWorkspace().getLockManager().isLocked(destination.getPath()))
				parameters.getSession().getWorkspace().getLockManager().unlock(destination.getPath());
		}

	}

	private Node create(FolderCreationParameters params, Node destination) throws Exception {
		Utils.acquireLockWithWait(params.getSession(), destination.getPath(), false, params.getUser(), 10);
		Node newNode = Utils.createFolderInternally(params, accountingHandler, false);
		params.getSession().save();
		return newNode;
	}

	private Node create(FileCreationParameters params, Node destination) throws Exception {
		Node newNode = createFileItemInternally(params.getSession(), destination, params.getStream(), params.getName(),
				params.getDescription(), params.getFileDetails(), params.getUser(), true);
		params.getSession().save();
		versionHandler.checkinContentNode(newNode);
		return newNode;
	}

	private Node create(URLCreationParameters params, Node destination) throws Exception {
		Utils.acquireLockWithWait(params.getSession(), destination.getPath(), false, params.getUser(), 10);
		Node newNode = Utils.createURLInternally(params.getSession(), destination, params.getName(), params.getUrl(),
				params.getDescription(), params.getUser(), accountingHandler);
		params.getSession().save();
		return newNode;
	}

	private Node create(ArchiveStructureCreationParameter params, Node destination) throws Exception {
		Utils.acquireLockWithWait(params.getSession(), destination.getPath(), false, params.getUser(), 10);
		FolderCreationParameters folderParameters = FolderCreationParameters.builder()
				.name(params.getParentFolderName()).author(params.getUser()).on(destination.getIdentifier())
				.with(params.getSession()).build();
		Node parentDirectoryNode = Utils.createFolderInternally(folderParameters, accountingHandler, false);
		params.getSession().save();
		try {
			if (params.getSession().getWorkspace().getLockManager().isLocked(destination.getPath()))
				params.getSession().getWorkspace().getLockManager().unlock(destination.getPath());
		} catch (Throwable t) {
			log.warn("error unlocking {}", destination.getPath(), t);
		}

		Set<Node> fileNodes = new HashSet<>();

		HashMap<String, Node> directoryNodeMap = new HashMap<>();

		ArchiveInputStream input = new ArchiveStreamFactory()
				.createArchiveInputStream(new BufferedInputStream(params.getStream()));

		ArchiveEntry entry;
		while ((entry = input.getNextEntry()) != null) {
			String entirePath = entry.getName();
			log.debug("reading new entry ------> {} ", entirePath);
			if (entry.isDirectory()) {
				log.debug("creating directory with entire path {} ", entirePath);
				createPath(entirePath, directoryNodeMap, parentDirectoryNode, params.getSession(), params.getUser());
			} else
				try {
					String name = entirePath.replaceAll("([^/]*/)*(.*)", "$2");
					String parentPath = entirePath.replaceAll("(([^/]*/)*)(.*)", "$1");
					log.debug("creating file with entire path {}, name {}, parentPath {} ", entirePath, name,
							parentPath);
					Node fileNode = null;
					long fileSize = entry.getSize();
					FormDataContentDisposition fileDetail = FormDataContentDisposition.name(name).size(fileSize)
							.build();
					//this code has been added for a bug on s3 client(v1) that closes the stream
					InputStream notClosableIS = new BufferedInputStream(input) {
						@Override
						public void close() throws IOException { }
					};

					if (parentPath.isEmpty()) {
						fileNode = createFileItemInternally(params.getSession(), parentDirectoryNode, notClosableIS, name, "",
								fileDetail, params.getUser(), false);
					} else {
						Node parentNode = directoryNodeMap.get(parentPath);
						if (parentNode == null)
							parentNode = createPath(parentPath, directoryNodeMap, parentDirectoryNode,
									params.getSession(), params.getUser());
						fileNode = createFileItemInternally(params.getSession(), parentNode, notClosableIS, name, "",
								fileDetail, params.getUser(), false);
					}
					fileNodes.add(fileNode);
				} catch (Throwable e) {
					log.warn("error getting file {}", entry.getName(), e);
				}
		}

		log.info("archive {} uploading finished ", params.getParentFolderName());
		params.getSession().save();
		for (Node node : fileNodes)
			versionHandler.checkinContentNode(node);
		return parentDirectoryNode;
	}



	private Node createPath(String parentPath, Map<String, Node> directoryNodeMap, Node rootNode, Session ses,
			String user) throws StorageHubException, RepositoryException {
		String[] parentPathSplit = parentPath.split("/");
		String name = parentPathSplit[parentPathSplit.length - 1];
		StringBuilder relParentPath = new StringBuilder();
		for (int i = 0; i <= parentPathSplit.length - 2; i++)
			relParentPath.append(parentPathSplit[i]).append("/");

		if (relParentPath.toString().isEmpty()) {
			FolderCreationParameters folderParameters = FolderCreationParameters.builder().name(name).author(user)
					.on(rootNode.getIdentifier()).with(ses).build();
			Node createdNode = Utils.createFolderInternally(folderParameters, accountingHandler, false);
			directoryNodeMap.put(name + "/", createdNode);
			return createdNode;
		} else {
			Node relParentNode = directoryNodeMap.get(relParentPath.toString());
			if (relParentNode == null) {
				relParentNode = createPath(relParentPath.toString(), directoryNodeMap, rootNode, ses, user);
			}

			FolderCreationParameters folderParameters = FolderCreationParameters.builder().name(name).author(user)
					.on(relParentNode.getIdentifier()).with(ses).build();
			Node createdNode = Utils.createFolderInternally(folderParameters, accountingHandler, false);
			directoryNodeMap.put(relParentPath.append(name).append("/").toString(), createdNode);
			return createdNode;
		}
	}

	private Node create(GCubeItemCreationParameters params, Node destination) throws Exception {
		Utils.acquireLockWithWait(params.getSession(), destination.getPath(), false, params.getUser(), 10);
		Node newNode = Utils.createGcubeItemInternally(params.getSession(), destination, params.getItem().getName(),
				params.getItem().getDescription(), params.getUser(), params.getItem(), accountingHandler);
		params.getSession().save();
		return newNode;
	}

	private Node createFileItemInternally(Session ses, Node destinationNode, InputStream stream, String name,
			String description, FormDataContentDisposition fileDetails, String login, boolean withLock)
			throws RepositoryException, StorageHubException {

		log.trace("UPLOAD: starting preparing file");

		Node newNode;
		FolderItem destinationItem = node2Item.getItem(destinationNode, Excludes.ALL);

		StorageBackendFactory sbf = storageBackendHandler.get(destinationItem.getBackend());
		
		StorageBackend sb = sbf.create(destinationItem.getBackend());

		String relativePath = destinationNode.getPath();

		String newNodePath = Paths.append(Paths.getPath(destinationNode.getPath()), name).toPath();
		log.info("new node path is {}", newNodePath);

		if (ses.nodeExists(newNodePath)) {

			newNode = ses.getNode(newNodePath);
			authChecker.checkWriteAuthorizationControl(ses, login, newNode.getIdentifier(), false);

			AbstractFileItem item = fillItemWithContent(stream, sb, name, description, fileDetails, relativePath,
					login);

			if (withLock) {
				try {
					ses.getWorkspace().getLockManager().lock(newNode.getPath(), true, true, 0, login);
				} catch (LockException le) {
					throw new ItemLockedException(le);
				}
			}
			try {
				versionHandler.checkoutContentNode(newNode);
				log.trace("replacing content of class {}", item.getContent().getClass());
				item2Node.replaceContent(newNode, item, ItemAction.UPDATED);
				String versionName = null;
				try {
					Version version = versionHandler.getCurrentVersion(newNode);
					versionName = version.getName();
				} catch (RepositoryException e) {
					log.warn("current version of {} cannot be retreived", item.getId());
				}
				accountingHandler.createFileUpdated(item.getTitle(), versionName, ses, newNode, login, false);
				ses.save();
			} catch (Throwable t) {
				log.error("error saving item", t);
			} finally {
				if (withLock) {
					if (ses != null && ses.hasPendingChanges())
						ses.save();
					ses.getWorkspace().getLockManager().unlock(newNode.getPath());
				}
			}
		} else {
			authChecker.checkWriteAuthorizationControl(ses, login, destinationNode.getIdentifier(), true);
			AbstractFileItem item = fillItemWithContent(stream, sb, name, description, fileDetails, relativePath,
					login);
			if (withLock) {
				try {
					log.debug("trying to acquire lock");
					Utils.acquireLockWithWait(ses, destinationNode.getPath(), false, login, 10);
				} catch (LockException le) {
					throw new ItemLockedException(le);
				}
			}
			try {
				newNode = item2Node.getNode(destinationNode, item);
				accountingHandler.createEntryCreate(item.getTitle(), ses, newNode, login, false);
				ses.save();
			} catch (Throwable t) {
				log.error("error saving item", t);
				throw new BackendGenericError(t);
			} finally {
				if (withLock)
					ses.getWorkspace().getLockManager().unlock(destinationNode.getPath());
			}
			versionHandler.makeVersionableContent(newNode);
			accountingHandler.createFolderAddObj(name, item.getClass().getSimpleName(), item.getContent().getMimeType(),
					ses, login, destinationNode, false);
		}

		// TODO: Utils.updateParentSize()

		return newNode;
	}

	private AbstractFileItem fillItemWithContent(InputStream stream, StorageBackend storageBackend, String name,
			String description, FormDataContentDisposition fileDetails, String relPath, String login)
			throws BackendGenericError {
		log.trace("UPLOAD: filling content");
		ContentHandler handler = getContentHandler(stream, storageBackend, name, fileDetails, relPath, login);
		return handler.buildItem(name, description, login);
	}

	private ContentHandler getContentHandler(InputStream stream, StorageBackend storageBackend, String name,
			FormDataContentDisposition fileDetails, String relPath, String login) throws BackendGenericError {

		log.trace("UPLOAD: handling content");

		long start = System.currentTimeMillis();
		log.trace("UPLOAD: writing the stream - start");
		try {

			MetaInfo info = null;
			try {
				log.debug("UPLOAD: upload on {} - start", storageBackend.getClass());
				if (fileDetails != null && fileDetails.getSize() > 0) {
					log.debug("UPLOAD: file size set is {} Byte", fileDetails.getSize());
					info = storageBackend.upload(stream, relPath, name, fileDetails.getSize(), login);
				} else
					info = storageBackend.upload(stream, relPath, name, login);
				log.debug("UPLOAD: upload on storage - stop");
			} catch (Throwable e) {
				log.error("error writing content", e);
				throw e;
			}
			ContentHandler handler = null;
			String mimeType;
			log.debug("UPLOAD: reading the mimetype - start");
			try (InputStream is1 = new BufferedInputStream(storageBackend.download(info.getStorageId()))) {
				org.apache.tika.mime.MediaType mediaType = null;
				TikaConfig config = TikaConfig.getDefaultConfig();
				Detector detector = config.getDetector();
				TikaInputStream tikastream = TikaInputStream.get(is1);
				Metadata metadata = new Metadata();
				mediaType = detector.detect(tikastream, metadata);
				mimeType = mediaType.getBaseType().toString();
				handler = contenthandlerFactory.create(mimeType);

				log.debug("UPLOAD: reading the mimetype {} - finished in {}", mimeType,
						System.currentTimeMillis() - start);
			} catch (Throwable e) {
				log.error("error retrieving mimeType", e);
				throw new RuntimeException(e);
			}

			if (handler.requiresInputStream())
				try (InputStream is1 = new BufferedInputStream(storageBackend.download(info.getStorageId()))) {
					log.debug("UPLOAD: the file type requires input stream");
					handler.initiliseSpecificContent(is1, name, mimeType, info.getSize());
				}
			else {
				log.debug("UPLOAD: the file type doesn't requires input stream");
				handler.initiliseSpecificContent(name, mimeType);
			}
			log.debug("UPLOAD: writing the stream - finished in {}", System.currentTimeMillis() - start);

			handler.getContent().setData(NodeConstants.CONTENT_NAME);
			handler.getContent().setStorageId(info.getStorageId());
			handler.getContent().setSize(info.getSize());
			handler.getContent().setRemotePath(info.getRemotePath());

			handler.getContent().setPayloadBackend(info.getPayloadBackend());
			log.debug("UPLOAD: content payload set as {} ", handler.getContent().getPayloadBackend());

			return handler;
		} catch (Throwable e) {
			log.error("error writing file", e);
			throw new BackendGenericError(e);
		}

	}
}
