package org.gcube.data.access.storagehub;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.lock.Lock;
import javax.jcr.lock.LockException;

import org.apache.commons.io.FilenameUtils;
import org.apache.jackrabbit.util.Text;
import org.gcube.common.storagehub.model.Excludes;
import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.IdNotFoundException;
import org.gcube.common.storagehub.model.exceptions.ItemLockedException;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.exceptions.UserNotAuthorizedException;
import org.gcube.common.storagehub.model.items.AbstractFileItem;
import org.gcube.common.storagehub.model.items.ExternalLink;
import org.gcube.common.storagehub.model.items.FolderItem;
import org.gcube.common.storagehub.model.items.GCubeItem;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.common.storagehub.model.items.SharedFolder;
import org.gcube.common.storagehub.model.storages.MetaInfo;
import org.gcube.common.storagehub.model.types.ItemAction;
import org.gcube.common.storagehub.model.types.NodeProperty;
import org.gcube.common.storagehub.model.types.FolderInfoType;
import org.gcube.data.access.storagehub.accounting.AccountingHandler;
import org.gcube.data.access.storagehub.handlers.items.Item2NodeConverter;
import org.gcube.data.access.storagehub.handlers.items.Node2ItemConverter;
import org.gcube.data.access.storagehub.handlers.items.builders.FolderCreationParameters;
import org.gcube.data.access.storagehub.handlers.plugins.StorageBackendHandler;
import org.gcube.data.access.storagehub.predicates.ItemTypePredicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utils {

	public final static String SERVICE_NAME  = "home-library";	
	public final static String SERVICE_CLASS = "org.gcube.portlets.user";
	private static final String FOLDERS_TYPE = "nthl:workspaceItem";

	private static final Logger logger = LoggerFactory.getLogger(Utils.class);

	public static String getSecurePassword(String user) throws Exception {
		String digest = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] hash = md.digest(user.getBytes("UTF-8"));

			//converting byte array to Hexadecimal String
			StringBuilder sb = new StringBuilder(2*hash.length);
			for(byte b : hash){
				sb.append(String.format("%02x", b&0xff));
			}
			digest = sb.toString();

		} catch (Exception e) {
			logger.error("error getting secure password",e);
		} 
		return digest;
	}

	public static long getItemCount(Node parent, boolean showHidden, ItemTypePredicate itemTypePredicate) throws RepositoryException, BackendGenericError{
		return getItemList(parent, Excludes.ALL, null, showHidden, itemTypePredicate).size();
	}
	
	public static FolderInfoType getFolderInfo(Node parent) throws RepositoryException, BackendGenericError{
		FolderInfoType info = new FolderInfoType(0, 0);
		List<Item> items = getItemList(parent, Excludes.GET_ONLY_CONTENT, null, false, null);
		for (Item item: items) 
			if (item instanceof FolderItem) {
				FolderInfoType fit = getFolderInfo((Node) item.getRelatedNode());
				info.setCount(info.getCount() + fit.getCount());
				info.setSize(info.getSize() + fit.getSize());
			} else if (item instanceof AbstractFileItem gfi)
				info.setSize(info.getSize() + gfi.getContent().getSize());
		info.setCount(info.getCount() + items.size());
		return info;
	}
	
	
	public static void acquireLockWithWait(Session ses, String nodePath, boolean isDeep, String login, int maxTries) throws RepositoryException, ItemLockedException {

		Lock lock = null;
		int tries = 0;
		while(lock==null && tries<=maxTries) {
			try {
				lock = ses.getWorkspace().getLockManager().lock(nodePath, isDeep, true, 0,login);
				logger.info("lock acquired(try n. {})", tries);
			}catch (LockException e) {
				try {
					if (maxTries>=tries) {
						int waitTime = (int)(Math.random()*5000);
						logger.info("lock NOT acquired, waiting (try n. {}) for {}", tries, waitTime);
						Thread.sleep(waitTime);
					}
				} catch (InterruptedException e1) {}
				tries++;
			}
		}

		if (lock==null) throw new ItemLockedException("the item is locked");
	}

	public static <T extends Item> List<T> getItemList(Node parent, List<String> excludes, Range range, boolean showHidden, ItemTypePredicate itemTypePredicate) throws RepositoryException, BackendGenericError{
		return getItemList(null, parent, excludes, range, showHidden, itemTypePredicate);
	}

	public static <T extends Item> List<T> getItemList(Predicate<Node> checker, Node parent, List<String> excludes, Range range, boolean showHidden, ItemTypePredicate itemTypePredicate) throws RepositoryException, BackendGenericError{
		logger.trace("getting children of node {}", parent.getIdentifier());
		long start = System.currentTimeMillis();
		NodeChildrenFilterIterator iterator = new NodeChildrenFilterIterator(parent);
		logger.trace("time to get iterator {}",(System.currentTimeMillis()-start));
		return getItemListFromNodeIterator(checker, iterator, excludes, range, showHidden, false, itemTypePredicate);
	}

	private static <T extends Item> List<T> getItemListFromNodeIterator(Predicate<Node> checker, NodeChildrenFilterIterator iterator, List<String> excludes, Range range, boolean showHidden, boolean excludeTrashed, ItemTypePredicate itemTypePredicate) throws RepositoryException, BackendGenericError{
		List<T> returnList = new ArrayList<T>();

		int count =0;
		logger.trace("selected range is {}", range);
		Node2ItemConverter node2Item= new Node2ItemConverter();
		while (iterator.hasNext()){
			Node current = iterator.next();

			logger.trace("[SEARCH] evaluating node {} ",current.hasProperty(NodeProperty.TITLE.toString())? current.getProperty(NodeProperty.TITLE.toString()):current.getName());

			//EXCLUDES node from predicate
			if (checker!=null && !checker.test(current))
				continue;

			if (isToExclude(current, showHidden))
				continue;

			logger.trace("[SEARCH] current node not excluded {} ",current.hasProperty(NodeProperty.TITLE.toString())? current.getProperty(NodeProperty.TITLE.toString()):current.getName());

			if (range==null || (count>=range.getStart() && returnList.size()<range.getLimit())) {
				T item = node2Item.getFilteredItem(current, excludes, itemTypePredicate);
				if (item==null || (item.isTrashed() && excludeTrashed)) continue;
				returnList.add(item);
			} 
			count++;
		}
		return returnList;
	}

	public static boolean isToExclude(Node node, boolean showHidden) throws RepositoryException{
		return ((node.getName().startsWith("rep:") || (node.getName().startsWith("hl:"))) || 
				(!showHidden && node.hasProperty(NodeProperty.HIDDEN.toString()) && node.getProperty(NodeProperty.HIDDEN.toString()).getBoolean()) ||
				(node.getPrimaryNodeType().getName().equals(FOLDERS_TYPE) && Constants.FOLDERS_TO_EXLUDE.contains(node.getName())));
	}




	public static void copyStream(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[2048];
		int readcount = 0;
		while ((readcount=in.read(buffer))!=-1) {
			out.write(buffer, 0, readcount);
		}
	}


	public static boolean hasSharedChildren(Node node) throws RepositoryException, BackendGenericError{
		Node2ItemConverter node2Item = new Node2ItemConverter();
		NodeChildrenFilterIterator children = new NodeChildrenFilterIterator(node);

		while (children.hasNext()) {
			Node child= children.next();
			if (node2Item.checkNodeType(child, SharedFolder.class)) return true;
			if (node2Item.checkNodeType(child, FolderItem.class) && hasSharedChildren(child)) return true;
		}
		return false;
	}


	public static String checkExistanceAndGetUniqueName(Session ses, Node destination, String name) throws BackendGenericError{
		String escapedName = Text.escapeIllegalJcrChars(name);
		try {
			destination.getNode(escapedName);
		}catch(PathNotFoundException pnf) {
			return  escapedName;
		} catch (Exception e) {
			throw new BackendGenericError(e);
		}

		try {
			String filename = FilenameUtils.getBaseName(escapedName);
			String ext = FilenameUtils.getExtension(escapedName);

			String nameTocheck = ext.isEmpty()? String.format("%s(*)",filename): String.format("%s(*).%s",filename, ext);

			logger.trace("filename is {}, extension is {} , and name to check is {}", filename, ext, nameTocheck);

			NodeIterator ni = destination.getNodes(nameTocheck);
			int maxval = 0;
			while (ni.hasNext()) {
				Node n = ni.nextNode();
				int actual = Integer.parseInt(n.getName().replaceAll(String.format("%s\\((\\d*)\\).*", filename), "$1"));
				if (actual>maxval)
					maxval = actual;
			}


			String newName = ext.isEmpty()? String.format("%s(%d)", filename,maxval+1) : String.format("%s(%d).%s", filename,maxval+1, ext) ;
			return newName;
		} catch (Exception e) {
			throw new BackendGenericError(e);
		}
	}


	public static Node createFolderInternally(FolderCreationParameters params, AccountingHandler accountingHandler, boolean isInternalWSFolder) throws StorageHubException {
		logger.debug("creating folder {} in {}", params.getName(), params.getParentId());

		Node destinationNode;
		FolderItem destinationItem;
		try {
			destinationNode = params.getSession().getNodeByIdentifier(params.getParentId());
			destinationItem = (FolderItem) new Node2ItemConverter().getItem(destinationNode, Excludes.ALL);
		}catch (RepositoryException  e) {
			logger.error("id not found",e);
			throw new IdNotFoundException(params.getParentId());
		}
		String uniqueName = Utils.checkExistanceAndGetUniqueName(params.getSession(), destinationNode, params.getName());

		FolderItem item = new FolderItem();
		Calendar now = Calendar.getInstance();
		item.setName(uniqueName);
		item.setTitle(uniqueName);
		item.setDescription(params.getDescription());

		if (isInternalWSFolder) {
			item.setBackend(StorageBackendHandler.getDefaultPayloadForFolder());
		} else {

			if (params.getBackend() != null) 
				item.setBackend(params.getBackend());
			else {
				if (destinationItem.getBackend() != null) 
					item.setBackend(destinationItem.getBackend());
				else 
					item.setBackend(StorageBackendHandler.getDefaultPayloadForFolder());
			}
		}
		//TODO: item.setExternalStorage();
		//item.setCreationTime(now);

		boolean hiddenDestNode= false;
		try {
			hiddenDestNode = destinationNode.getProperty(NodeProperty.HIDDEN.toString()).getBoolean();
		}catch (Throwable e) {}

		item.setHidden(params.isHidden() || hiddenDestNode);
		item.setLastAction(ItemAction.CREATED);
		item.setLastModificationTime(now);
		item.setLastModifiedBy(params.getUser());
		item.setOwner(params.getUser());
		item.setPublicItem(false);

		Node newNode = new Item2NodeConverter().getNode(destinationNode, item);
		if (accountingHandler!=null) {
			accountingHandler.createFolderAddObj(uniqueName, item.getClass().getSimpleName(), null, params.getSession(), params.getUser(), destinationNode, false);
			accountingHandler.createEntryCreate(item.getTitle(), params.getSession(), newNode, params.getUser(), false);
		}
		return newNode;
	}

	public static Node createURLInternally(Session ses, Node destinationNode, String name, URL value, String description, String login, AccountingHandler accountingHandler) throws BackendGenericError {

		String uniqueName = Utils.checkExistanceAndGetUniqueName(ses, destinationNode, name);

		ExternalLink item = new ExternalLink();
		Calendar now = Calendar.getInstance();
		item.setName(uniqueName);
		item.setTitle(uniqueName);
		item.setDescription(description);
		//item.setCreationTime(now);
		item.setLastAction(ItemAction.CREATED);
		item.setLastModificationTime(now);
		item.setLastModifiedBy(login);
		item.setOwner(login);
		item.setPublicItem(false);
		item.setValue(value);

		try {
			item.setHidden(destinationNode.getProperty(NodeProperty.HIDDEN.toString()).getBoolean());
		} catch (Throwable e) {
			item.setHidden(false);
		} 



		Node newNode = new Item2NodeConverter().getNode(destinationNode, item);
		
		if (accountingHandler!=null) {
			accountingHandler.createFolderAddObj(name, item.getClass().getSimpleName(), null, ses, login,  destinationNode, false);
			accountingHandler.createEntryCreate(item.getTitle(), ses, newNode,login, false);
		}
		return newNode;
	}

	public static Node createGcubeItemInternally(Session ses, Node destinationNode, String name, String description, String login, GCubeItem gcubeItem, AccountingHandler accountingHandler) throws BackendGenericError {

		Calendar now = Calendar.getInstance();
		gcubeItem.setName(name);
		gcubeItem.setTitle(name);
		gcubeItem.setDescription(description);
		//item.setCreationTime(now);
		gcubeItem.setHidden(false);
		gcubeItem.setLastAction(ItemAction.CREATED);
		gcubeItem.setLastModificationTime(now);
		gcubeItem.setLastModifiedBy(login);
		gcubeItem.setOwner(login);
		//to inherit hidden property
		//item.setHidden(destinationItem.isHidden());

		Node newNode = new Item2NodeConverter().getNode(destinationNode, gcubeItem);
		//TODO: accounting for GCUBEITEM
		//accountingHandler.createFolderAddObj(name, item.getClass().getSimpleName(), null, ses, newNode, false);
		return newNode;
	}

	public static void setPropertyOnChangeNode(Node node, String login, ItemAction action) throws RepositoryException {
		node.setProperty(NodeProperty.LAST_MODIFIED.toString(), Calendar.getInstance());
		node.setProperty(NodeProperty.LAST_MODIFIED_BY.toString(), login);
		node.setProperty(NodeProperty.LAST_ACTION.toString(), action.name());
	}

	public static void setContentFromMetaInfo(AbstractFileItem item, MetaInfo contentInfo) {
		item.getContent().setSize(contentInfo.getSize());
		item.getContent().setRemotePath(contentInfo.getRemotePath());
		item.getContent().setSize(contentInfo.getSize());
		item.getContent().setPayloadBackend(contentInfo.getPayloadBackend());
	}

}
