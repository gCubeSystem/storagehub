package org.gcube.data.access.storagehub.health;

import org.gcube.common.health.api.HealthCheck;
import org.gcube.common.health.api.ReadinessChecker;
import org.gcube.common.health.api.response.HealthCheckResponse;
import org.gcube.common.storagehub.model.items.nodes.PayloadBackend;
import org.gcube.data.access.storagehub.handlers.plugins.StorageBackendHandler;
import org.gcube.data.access.storagehub.storage.backend.impl.GcubeDefaultS3StorageBackendFactory;
import org.gcube.data.access.storagehub.storage.backend.impl.S3Backend;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ReadinessChecker
public class DefaultStorageCheck implements HealthCheck{

	private static Logger log = LoggerFactory.getLogger(DefaultStorageCheck.class);
	
	PayloadBackend defaultPayload = StorageBackendHandler.getDefaultPayloadForFolder(); 
		
	@Override
	public String getName() {
		return String.format("default storage (%s)",defaultPayload.getStorageName());
	}

	@Override
	public HealthCheckResponse check() {
		try {
			GcubeDefaultS3StorageBackendFactory storageFactory =new GcubeDefaultS3StorageBackendFactory();
			storageFactory.init();
 			if (((S3Backend)storageFactory.create(defaultPayload)).isAlive())
				return HealthCheckResponse.builder(getName()).up().build();
			else
				return HealthCheckResponse.builder(getName()).down().error("error contacting storage").build();
		} catch (Exception e) {
			log.error("error checking defaultStorage",e);
			return HealthCheckResponse.builder(getName()).down().error(e.getMessage()).build(); 
		} 
		
	}

}
