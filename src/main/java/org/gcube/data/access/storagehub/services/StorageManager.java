package org.gcube.data.access.storagehub.services;

import java.util.ArrayList;
import java.util.List;

import org.gcube.common.storagehub.model.storages.StorageDescriptor;
import org.gcube.data.access.storagehub.StorageHubApplicationManager;
import org.gcube.data.access.storagehub.handlers.plugins.StorageBackendHandler;
import org.gcube.smartgears.annotations.ManagedBy;
import org.gcube.smartgears.utils.InnerMethodName;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

/**
 * Manage underlying storage backends.
 */
@Path("storages")
@ManagedBy(StorageHubApplicationManager.class)
@RequestHeaders({
	  @RequestHeader( name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>"),
	})
public class StorageManager {

	@Inject
	StorageBackendHandler storageBackendHandler;
	
	/**
	 * Get a list of available storages
	 *
	 * @return list of available storages 
	 */
	@GET
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "Success."),
	})
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<StorageDescriptor> getStorages(){
		InnerMethodName.set("getStorages");
		List<StorageDescriptor> storages = new ArrayList<>();
		storageBackendHandler.getAllImplementations().forEach( f -> storages.add(new StorageDescriptor(f.getName())));
		return storages;
	}
}
