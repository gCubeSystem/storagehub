package org.gcube.data.access.storagehub.types;

public interface PublicLink {
		
	
	LinkType getType();
	
	String getId();
	
	default String getVersion()  {
		return null;
	}
	
	default String getStorageName()  {
		return null;
	}
	
}
