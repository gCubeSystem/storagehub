package org.gcube.data.access.storagehub.handlers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.jackrabbit.api.JackrabbitSession;
import org.gcube.common.storagehub.model.acls.ACL;
import org.gcube.common.storagehub.model.acls.AccessType;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.exporter.DumpData;
import org.gcube.common.storagehub.model.exporter.GroupData;
import org.gcube.common.storagehub.model.exporter.UserData;
import org.gcube.common.storagehub.model.items.FolderItem;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.common.storagehub.model.types.SHUBUser;
import org.gcube.data.access.storagehub.PathUtil;
import org.gcube.data.access.storagehub.Utils;
import org.gcube.data.access.storagehub.handlers.vres.VREManager;
import org.gcube.data.access.storagehub.services.delegates.ACLManagerDelegate;
import org.gcube.data.access.storagehub.services.delegates.GroupManagerDelegate;
import org.gcube.data.access.storagehub.services.delegates.UserManagerDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

@Singleton
public class DataHandler {

	public static final Logger logger = LoggerFactory.getLogger(DataHandler.class);

	@Inject
	UserManagerDelegate userHandler;

	@Inject
	GroupManagerDelegate groupHandler;

	@Inject
	ACLManagerDelegate aclHandler;

	@Inject
	PathUtil pathUtil;
	
	@Inject
	VREManager vreManager;

	public DumpData exportData(JackrabbitSession session) throws RepositoryException,StorageHubException {
		DumpData data = new DumpData();
		
		List<SHUBUser> users = userHandler.getAllUsers(session); 
		
		List<UserData> usersData = users.stream().map(u -> new UserData(u.getUserName()))
				.collect(Collectors.toList());
		
		List<String> groups = groupHandler.getGroups(session);

		logger.debug("found users {} ",usersData);
		logger.debug("found groups {} ",groups);
		
		List<GroupData> groupsData = new ArrayList<GroupData>(groups.size());
		for (String group : groups) {
			logger.debug("searching for group {}",group);
			Item vreFolderItem = vreManager.getVreFolderItemByGroupName(session, group, null).getVreFolder();
			String owner = vreFolderItem.getOwner();
			List<ACL> acls = aclHandler.getByItem(vreFolderItem, session);

			AccessType accessType = AccessType.READ_ONLY;
			List<ACL> otherAccess = new ArrayList<ACL>(acls.size() - 1);
			for (ACL acl : acls)
				if (acl.getPrincipal().equals(group))
					accessType = acl.getAccessTypes().get(0);
				else
					otherAccess.add(acl);

			List<String> members = groupHandler.getUsersBelongingToGroup(session, group);
			
			groupsData.add(new GroupData(group, owner, members, accessType, otherAccess));
		}
		
		Map<String, List<Item>> itemsPerUser = new HashMap<String, List<Item>>();
		for (SHUBUser user : users ) {
			logger.debug("getting all the files in {} workspace folder ",user.getUserName());
			String homePath = pathUtil.getWorkspacePath(user.getUserName()).toPath();
			Node homeNode = session.getNode(homePath);
			List<Item> items = Utils.getItemList(homeNode, Collections.emptyList(), null, true, null);
			items.forEach(i -> i.setParentId(null));
			itemsPerUser.put(user.getUserName(), retrieveSubItems(items));
			
		}

		data.setUsers(usersData);
		data.setGroups(groupsData);
		data.setItemPerUser(itemsPerUser);
		
		return data;
	}
	
	
	List<Item> retrieveSubItems(List<Item> items) throws StorageHubException, RepositoryException {
		List<Item> toReturn = new ArrayList<Item>();
		for (Item item: items ) 
			if (item instanceof FolderItem f) {
				if (f.isShared()) continue;
				toReturn.addAll(retrieveSubItems(Utils.getItemList((Node) f.getRelatedNode(), Collections.emptyList(), null, true, null)));
			} else 
				toReturn.add(item);
		
		return toReturn;
	}

	public void importData(JackrabbitSession session, DumpData data) {
		data.getUsers().forEach(u -> {
			try {
				userHandler.createUser(session, u.getUserName(), "pwd");
			} catch (RepositoryException | StorageHubException e) {
				logger.warn("error importing user {} ", u.getUserName(), e);
			}
		});
		
		data.getGroups().forEach(g -> {
			try {
				groupHandler.createGroup(session, g.getName(), g.getAccessType(), g.getFolderOwner(), true);
				for (String member : g.getMembers())
					try {
						groupHandler.addUserToGroup(session, member, g.getName());
					}catch(Throwable t ) {
						logger.warn("error adding user {} to group {}", member, g.getName(), t);
					}
				
				Item vreFolderItem = vreManager.getVreFolderItemByGroupName(session, g.getName(), null).getVreFolder();
				
				for (ACL acl : g.getAcls()) {
					aclHandler.update(acl.getPrincipal(), (Node)vreFolderItem.getRelatedNode(), acl.getAccessTypes().get(0), session);
				}
				
			} catch (Throwable e) {
				logger.warn("error importing group {} ", g.getName(), e);
			}
		});
	}

}
