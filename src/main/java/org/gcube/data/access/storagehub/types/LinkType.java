package org.gcube.data.access.storagehub.types;

public enum LinkType {

	VOLATILE,
	VERSIONED,
	STANDARD
}
