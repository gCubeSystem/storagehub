package org.gcube.data.access.storagehub.services.admin;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.common.storagehub.model.items.nodes.Content;
import org.gcube.common.storagehub.model.storages.StorageBackendFactory;
import org.gcube.data.access.storagehub.Utils;
import org.gcube.data.access.storagehub.handlers.TrashHandler;
import org.gcube.data.access.storagehub.handlers.items.Item2NodeConverter;
import org.gcube.data.access.storagehub.handlers.items.Node2ItemConverter;
import org.gcube.data.access.storagehub.handlers.items.builders.FolderCreationParameters;
import org.gcube.data.access.storagehub.handlers.plugins.StorageBackendHandler;
import org.gcube.data.access.storagehub.predicates.IncludeTypePredicate;
import org.gcube.data.access.storagehub.predicates.ItemTypePredicate;
import org.gcube.data.access.storagehub.scripting.ScriptUtil;

@Singleton
public class ScriptUtilImpl implements ScriptUtil {
	
	@Inject Node2ItemConverter node2Item;
	
	@Inject Item2NodeConverter item2Node;
	
	@Inject TrashHandler trashHandler;
	
	@Inject StorageBackendHandler backendHandler;
	
	@Override
	public List<Item> getChildren(Predicate<Node> checker, Node parent, List<String> excludes, boolean showHidden, Class<? extends Item> nodeTypeToInclude) throws RepositoryException, BackendGenericError {
		ItemTypePredicate itemPredicate = new IncludeTypePredicate(nodeTypeToInclude);
		return Utils.getItemList(checker, parent, excludes, null, showHidden, itemPredicate);
	}

	@Override
	public Item getItem(Node node, List<String> excludes) throws RepositoryException, BackendGenericError {
		return node2Item.getItem(node, excludes);
	}
	
	@Override
	public void removeNodes(Session ses, List<Item> itemsToDelete) throws RepositoryException, StorageHubException{
		trashHandler.removeNodes(ses, itemsToDelete);
	}
	
	@Override
	public Collection<StorageBackendFactory> getStorageBackendHandler() {
		return backendHandler.getAllImplementations();
	}

	@Override
	public void updateContentNode(Content content, Node node) throws Exception {
		item2Node.replaceContentNodeInternal(node, content.getClass(), content);
	}
	
	@Override
	public Node createInternalFolder(Session ses, String name, String description, String userOwner, String parentNodeIdentifier) throws StorageHubException {
		FolderCreationParameters parameters = FolderCreationParameters.builder().name(name).description(description).author(userOwner).on(parentNodeIdentifier).with(ses).build();
		return Utils.createFolderInternally(parameters, null, true);
	}
	
}
