package org.gcube.data.access.storagehub.types;

import org.gcube.common.storagehub.model.annotations.RootNode;
import org.gcube.common.storagehub.model.messages.Message;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RootNode("nthl:itemSentRequestSH")
public class MessageSharable extends Message {

}
