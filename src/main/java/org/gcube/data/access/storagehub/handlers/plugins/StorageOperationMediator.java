package org.gcube.data.access.storagehub.handlers.plugins;

import java.io.InputStream;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.items.nodes.Content;
import org.gcube.common.storagehub.model.items.nodes.PayloadBackend;
import org.gcube.common.storagehub.model.storages.MetaInfo;
import org.gcube.common.storagehub.model.storages.StorageBackend;
import org.gcube.common.storagehub.model.storages.StorageBackendFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class StorageOperationMediator {

	Logger log = LoggerFactory.getLogger(StorageOperationMediator.class);

	@Inject
	StorageBackendHandler storageBackendHandler;

	public MetaInfo copy(Content source, PayloadBackend destination, String newName, String newParentPath, String login) throws StorageHubException{

		log.info("creating Storages for source {} and destination {}", source.getPayloadBackend(), destination.getStorageName());

		StorageBackendFactory sourceSBF = storageBackendHandler.get(source.getPayloadBackend());
		//TODO: add metadata taken from content node
		StorageBackend sourceSB = sourceSBF.create(source.getPayloadBackend());

		StorageBackendFactory destSBF = storageBackendHandler.get(destination);
		StorageBackend destSB = destSBF.create(destination);

		log.info("source factory is {} and destination factory is {}", sourceSBF.getName(), destSBF.getName());

		if (sourceSB.equals(destSB)) {
			log.info("source and destination are the same storage");
			return sourceSB.onCopy(source, newParentPath, newName);
		}else {
			log.info("source and destination are different storage");
			InputStream stream = sourceSB.download(source);
			return destSB.upload(stream, newParentPath, newName, source.getSize(), login);
		}
	}

	public boolean move(){
		return true;
	}
}
