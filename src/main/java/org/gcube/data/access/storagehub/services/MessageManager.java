package org.gcube.data.access.storagehub.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.user.User;
import org.gcube.common.gxrest.response.outbound.GXOutboundErrorResponse;
import org.gcube.common.storagehub.model.Excludes;
import org.gcube.common.storagehub.model.Paths;
import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.IdNotFoundException;
import org.gcube.common.storagehub.model.exceptions.InvalidCallParameters;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.exceptions.UserNotAuthorizedException;
import org.gcube.common.storagehub.model.items.AbstractFileItem;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.common.storagehub.model.items.nodes.Content;
import org.gcube.common.storagehub.model.items.nodes.Owner;
import org.gcube.common.storagehub.model.messages.Message;
import org.gcube.common.storagehub.model.service.ItemList;
import org.gcube.common.storagehub.model.storages.MetaInfo;
import org.gcube.common.storagehub.model.storages.StorageBackend;
import org.gcube.common.storagehub.model.storages.StorageBackendFactory;
import org.gcube.common.storagehub.model.types.ItemAction;
import org.gcube.common.storagehub.model.types.MessageList;
import org.gcube.common.storagehub.model.types.NodeProperty;
import org.gcube.data.access.storagehub.Constants;
import org.gcube.data.access.storagehub.PathUtil;
import org.gcube.data.access.storagehub.StorageHubApplicationManager;
import org.gcube.data.access.storagehub.Utils;
import org.gcube.data.access.storagehub.accounting.AccountingHandler;
import org.gcube.data.access.storagehub.handlers.TrashHandler;
import org.gcube.data.access.storagehub.handlers.items.Item2NodeConverter;
import org.gcube.data.access.storagehub.handlers.items.Item2NodeConverter.Values;
import org.gcube.data.access.storagehub.handlers.items.Node2ItemConverter;
import org.gcube.data.access.storagehub.handlers.plugins.StorageBackendHandler;
import org.gcube.data.access.storagehub.predicates.IncludeTypePredicate;
import org.gcube.data.access.storagehub.predicates.ItemTypePredicate;
import org.gcube.data.access.storagehub.repository.StoragehubRepository;
import org.gcube.data.access.storagehub.types.MessageSharable;
import org.gcube.smartgears.annotations.ManagedBy;
import org.gcube.smartgears.utils.InnerMethodName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.webcohesion.enunciate.metadata.Ignore;
import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

@Ignore
@Path("messages")
@ManagedBy(StorageHubApplicationManager.class)
@RequestHeaders({
	  @RequestHeader( name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>"),
	})
public class MessageManager extends Impersonable{

	private static final Logger log = LoggerFactory.getLogger(MessageManager.class);

	private final StoragehubRepository repository = StoragehubRepository.repository;
	
	@Inject 
	AccountingHandler accountingHandler;

	@RequestScoped
	@PathParam("id") 
	String id;

	@Inject PathUtil pathUtil;

	@Inject Node2ItemConverter node2Item;
	@Inject Item2NodeConverter item2Node;

	@Inject TrashHandler trashHandler;
	
	@Inject
	StorageBackendHandler storageBackendHandler;
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Message getById(){
		InnerMethodName.set("getMessageById");
		Session ses = null;
		Message toReturn = null;
		try{
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			Node messageNode = ses.getNodeByIdentifier(id);
			toReturn = node2Item.getMessageItem(messageNode);
			checkRights(currentUser, toReturn);
		}catch (ItemNotFoundException e) {
			log.error("id {} not found",id,e);
			GXOutboundErrorResponse.throwException(new IdNotFoundException(id, e), Status.NOT_FOUND);
		}catch(RepositoryException re){
			log.error("jcr error getting item", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error getting item", re));
		}catch(StorageHubException she ){
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		}finally{
			if (ses!=null)
				ses.logout();
		}

		return toReturn;
	}

	@DELETE
	@Path("{id}")
	public void deleteById(){
		InnerMethodName.set("deleteMessageById");
		Session ses = null;
		try{
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			Node messageNode = ses.getNodeByIdentifier(id);
			Message message = node2Item.getMessageItem(messageNode);
			Node personalNode = checkRights(currentUser, message);

			if (countSharedSet(messageNode)>1) {
				log.debug("removing node message "+personalNode.getPath());
				personalNode.remove();
			}else {
				if (message.isWithAttachments()) {
					Node attachmentNode = messageNode.getNode(Constants.ATTACHMENTNODE_NAME);
					ItemTypePredicate itemPredicate = new IncludeTypePredicate(AbstractFileItem.class);
					List<Item> attachments = Utils.getItemList(attachmentNode, Excludes.GET_ONLY_CONTENT, null, true, itemPredicate);
					trashHandler.removeOnlyNodesContent(ses, attachments);
				}
				messageNode.removeSharedSet();
			}
			ses.save();
			log.debug("removing node message saved");
		}catch (ItemNotFoundException e) {
			log.error("id {} not found",id,e);
			GXOutboundErrorResponse.throwException(new IdNotFoundException(id, e), Status.NOT_FOUND);
		}catch(RepositoryException re){
			log.error("jcr error getting item", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error getting item", re));
		}catch(StorageHubException she ){
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		}finally{
			if (ses!=null)
				ses.logout();
		}
	}

	@GET
	@Path("{id}/attachments")
	@Produces(MediaType.APPLICATION_JSON)
	public ItemList getAttachments(){
		InnerMethodName.set("getAttachmentsByMessageId");
		Session ses = null;
		List<Item> attachments = new ArrayList<>();
		try{
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			Node messageNode = ses.getNodeByIdentifier(id);
			Message messageItem = node2Item.getMessageItem(messageNode);
			checkRights(currentUser, messageItem);
			Node attachmentNode = messageNode.getNode(Constants.ATTACHMENTNODE_NAME);
			ItemTypePredicate itemPredicate = new IncludeTypePredicate(AbstractFileItem.class);
			attachments = Utils.getItemList(attachmentNode, Excludes.GET_ONLY_CONTENT, null, true, itemPredicate);
		}catch (ItemNotFoundException e) {
			log.error("id {} not found",id,e);
			GXOutboundErrorResponse.throwException(new IdNotFoundException(id, e), Status.NOT_FOUND);
		}catch(RepositoryException re){
			log.error("jcr error getting item", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error getting item", re));
		}catch(StorageHubException she ){
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		}finally{
			if (ses!=null)
				ses.logout();
		}

		return new ItemList(attachments);
	}

	@GET
	@Path("inbox")
	@Produces(MediaType.APPLICATION_JSON)
	public MessageList getReceivedMessages(@QueryParam("reduceBody") Integer reduceBody){
		InnerMethodName.set("getReceivedMessages");
		Session ses = null;
		List<Message> toReturn = null;
		try{
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);

			Node node = ses.getNode(pathUtil.getInboxPath(currentUser).toPath());

			//return sorted for createdTime
			toReturn = getMessages(node, reduceBody);
		}catch(RepositoryException re){
			log.error("jcr error getting item", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error getting item", re));
		}finally{
			if (ses!=null)
				ses.logout();
		}

		return new MessageList(toReturn);
	}

	@GET
	@Path("sent")
	@Produces(MediaType.APPLICATION_JSON)
	public MessageList getSentMessages(@QueryParam("reduceBody") Integer reduceBody){
		InnerMethodName.set("getSentMessages");
		Session ses = null;
		List<Message> toReturn = null;
		try{
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);

			Node node = ses.getNode(pathUtil.getOutboxPath(currentUser).toPath());

			toReturn = getMessages(node, reduceBody);
		}catch(RepositoryException re){
			log.error("jcr error getting item", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error getting item", re));
		}finally{
			if (ses!=null)
				ses.logout();
		}

		return new MessageList(toReturn);
	}

	@PUT
	@Path("{id}/{prop}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void setProperty(@PathParam("prop") String property,Object value){
		InnerMethodName.set("setPropertyOnMessage("+property+")");
		Session ses = null;
		try{
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			Node messageNode = ses.getNodeByIdentifier(id);
			Message messageItem = node2Item.getMessageItem(messageNode);
			checkRights(currentUser, messageItem);
			Values val = Item2NodeConverter.getObjectValue(value.getClass(), value);
			messageNode.setProperty(property, val.getValue());
			ses.save();
		}catch (ItemNotFoundException e) {
			log.error("id {} not found",id,e);
			GXOutboundErrorResponse.throwException(new IdNotFoundException(id, e), Status.NOT_FOUND);
		}catch(StorageHubException she ){
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		}catch( Exception re){
			log.error("jcr error getting item", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error getting item", re));
		}finally{
			if (ses!=null)
				ses.logout();
		}
	}



	@POST
	@Path("send")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String sendMessage(@FormParam("to[]") List<String> addresses, 
			@FormParam("subject") String subject, @FormParam("body") String body,
			@FormParam("attachments[]") List<String> attachments){
		InnerMethodName.set("sendMessage");
		JackrabbitSession ses = null;
		String messageId = null;
		try{
			if (addresses.size()==0 || body==null || subject==null) 
				throw new InvalidCallParameters();

			log.debug("attachments send are {}",attachments);

			ses = (JackrabbitSession) repository.getRepository().login(Constants.JCR_CREDENTIALS);

			Message message = new MessageSharable();
			message.setAddresses(addresses.toArray(new String[0]));
			message.setSubject(subject);
			message.setBody(body);
			message.setName(UUID.randomUUID().toString());
			User user = ses.getUserManager().getAuthorizable(currentUser, User.class);
			
			if (user ==null) 
				throw new InvalidCallParameters("invalid storagehub user: "+currentUser);
			Owner owner = new Owner();
			owner.setUserId(user.getID());
			owner.setUserName(user.getPrincipal().getName());
			message.setSender(owner);
			Node outbox = ses.getNode(pathUtil.getOutboxPath(currentUser).toPath());
			Node messageNode = item2Node.getNode(outbox, message);
			ses.save();
			if (attachments!=null && !attachments.isEmpty()) {
				saveAttachments(ses, messageNode, attachments);
				ses.save();
			}
			for (String to: addresses)
				try {
					String userMessagePath = Paths.append(pathUtil.getInboxPath(to), messageNode.getName()).toPath();
					ses.getWorkspace().clone(ses.getWorkspace().getName(), messageNode.getPath(), userMessagePath, false);
				}catch (Exception e) {
					log.warn("message not send to {}",to,e);
				}

			ses.save();
			messageId = messageNode.getIdentifier();
		}catch(RepositoryException re){
			log.error("jcr error getting item", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error getting item", re));
		}catch(StorageHubException she ){
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		}finally{
			if (ses!=null)
				ses.logout();
		}

		return messageId;
	}



	private Node saveAttachments(Session ses, Node messageNode , List<String> attachments) throws RepositoryException, StorageHubException{
		Node attachmentNode = messageNode.getNode(Constants.ATTACHMENTNODE_NAME);

		for (String itemId: attachments) {
			Node node = ses.getNodeByIdentifier(itemId);
			Item item = node2Item.getItem(node, Excludes.GET_ONLY_CONTENT);
			Node newNode = copyNode(ses, attachmentNode, item);
			//removes accounting if exists
			if (newNode.hasNode(NodeProperty.ACCOUNTING.toString()))
				newNode.getNode(NodeProperty.ACCOUNTING.toString()).remove();
		}
		return messageNode;
	}

	//returns Messages sorted by createdTime
	private List<Message> getMessages(Node node, Integer reduceBody) throws RepositoryException{
		List<Message> messages = new ArrayList<Message>();
		NodeIterator nodeIt = node.getNodes();
		while(nodeIt.hasNext()) {
			Node child = nodeIt.nextNode();
			log.trace("message type "+child.getPrimaryNodeType().getName());
			Message message = node2Item.getMessageItem(child);
			if (message == null) {
				log.info("message discarded");
				continue;
			}

			if (reduceBody != null && reduceBody>0 && message.getBody().length()>reduceBody )
				message.setBody(message.getBody().substring(0, reduceBody));
			insertOrdered(messages, message);
		}
		return messages;
	}

	private void insertOrdered(List<Message> messages, Message toInsert) {
		if (messages.isEmpty()) 
			messages.add(toInsert);
		else {
			int i;
			for ( i=0  ; i<messages.size(); i++) 
				if (messages.get(i).getCreationTime().getTimeInMillis()<=toInsert.getCreationTime().getTimeInMillis())
					break;
			messages.add(i, toInsert);
		}
	}


	private Node checkRights(String user, Message messageItem) throws RepositoryException, StorageHubException{
		Node personalNode = null;
		Node messageNode = (Node) messageItem.getRelatedNode();
		if (messageNode.getPath().startsWith(pathUtil.getInboxPath(user).toPath()))
			return messageNode;

		NodeIterator nodeIt = messageNode.getSharedSet();
		while (nodeIt.hasNext()) {
			Node node = nodeIt.nextNode();
			if (node.getPath().startsWith(pathUtil.getInboxPath(user).toPath()))
				personalNode = node;
		}
		if (personalNode == null && 
				!messageItem.getSender().getUserName().equals(user) && !Arrays.asList(messageItem.getAddresses()).contains(user))
			throw new UserNotAuthorizedException("user "+currentUser+"cannot read message with id "+id);
		return personalNode== null ? messageNode : personalNode;
	}

	
	private Node copyNode(Session session, Node destination, Item itemToCopy) throws RepositoryException, StorageHubException{
		//it needs to be locked ??
		Node nodeToCopy = ((Node)itemToCopy.getRelatedNode());
		String uniqueName = Utils.checkExistanceAndGetUniqueName(session, destination,itemToCopy.getName() );				
		String newPath= String.format("%s/%s", destination.getPath(), uniqueName);
		session.getWorkspace().copy(nodeToCopy.getPath(), newPath);
		Node newNode = session.getNode(newPath);

		if (itemToCopy instanceof AbstractFileItem) {
			AbstractFileItem newNodeItem = node2Item.getItem(newNode, Excludes.EXCLUDE_ACCOUNTING); 
			
			Content contentToCopy = newNodeItem.getContent();
			
			StorageBackendFactory sbf = storageBackendHandler.get(contentToCopy.getPayloadBackend());
			StorageBackend sb = sbf.create(contentToCopy.getPayloadBackend());
			
			MetaInfo contentInfo = sb.onCopy(contentToCopy, destination.getPath(), uniqueName);
						
			Utils.setContentFromMetaInfo(newNodeItem, contentInfo);
			item2Node.replaceContent(newNode, newNodeItem, ItemAction.CLONED);
		} 

		Utils.setPropertyOnChangeNode(newNode, currentUser, ItemAction.CLONED);
		newNode.setProperty(NodeProperty.PORTAL_LOGIN.toString(), currentUser);
		newNode.setProperty(NodeProperty.IS_PUBLIC.toString(), false);
		newNode.setProperty(NodeProperty.TITLE.toString(), uniqueName);
		return newNode;
	}

	private int countSharedSet(Node node) throws RepositoryException{
		int count =0;
		NodeIterator it = node.getSharedSet();
		while (it.hasNext()) {
			count ++;
			it.next();
		}
		return count;
	}
}
