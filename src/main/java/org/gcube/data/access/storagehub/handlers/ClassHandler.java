package org.gcube.data.access.storagehub.handlers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.gcube.common.storagehub.model.annotations.RootNode;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.common.storagehub.model.items.RootItem;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClassHandler {

	private static Logger log = LoggerFactory.getLogger(ClassHandler.class);

	private static ClassHandler instance = null;
	
	public static ClassHandler instance() {
		if (instance == null)
			instance = new ClassHandler();
		return instance;
	}
	
	private Reflections reflection = new Reflections();

	private List<String> deprecatedNode = Arrays.asList("nthl:query", "nthl:aquamapsItem", "nthl:timeSeriesItem", "nthl:report", "nthl:reportTemplate", "nthl:workflowReport",
			"nthl:workflowTemplate", "nthl:gCubeMetadata", "nthl:gCubeDocument", "nthl:gCubeDocumentLink", "nthl:gCubeImageDocumentLink", "nthl:gCubePDFDocumentLink",
			"nthl:gCubeImageDocument", "nthl:gCubePDFDocument", "nthl:gCubeURLDocument", "nthl:gCubeAnnotation", "nthl:externalResourceLink", "nthl:tabularDataLink");
	
	private Map<String, Class<? extends RootItem>> classMap = new HashMap<String, Class<? extends RootItem>>();
	private Map<Class<? extends RootItem>, String> typeMap = new HashMap<Class<? extends RootItem>, String>();


	@SuppressWarnings("unchecked")
	private ClassHandler() {

		Set<Class<?>> classesAnnotated = reflection.getTypesAnnotatedWith(RootNode.class);
		for (Class<?> clazz: classesAnnotated ){
			if (RootItem.class.isAssignableFrom(clazz) && clazz.isAnnotationPresent(RootNode.class)) {
				String[] values =  clazz.getAnnotation(RootNode.class).value();
				log.debug("loading class {} with values {} ", clazz, values );
				for (String value: values)
					classMap.put(value, (Class<? extends RootItem>) clazz);
				typeMap.put((Class<? extends RootItem>) clazz, values[0]);
			}
		}
	}
	
	
	public Class<? extends RootItem> get(String nodeType){
		if (classMap.containsKey(nodeType)) return classMap.get(nodeType);
		if (deprecatedNode.contains(nodeType)) return Item.class;
		return null;
		//throw new RuntimeException("mapping not found for nodetype "+ nodeType);
	}

	public String getNodeType(Class<? extends RootItem> clazz){
		if (typeMap.containsKey(clazz)) return typeMap.get(clazz);
		throw new RuntimeException("mapping not found for nodetype "+ clazz.getSimpleName());
	}


}


