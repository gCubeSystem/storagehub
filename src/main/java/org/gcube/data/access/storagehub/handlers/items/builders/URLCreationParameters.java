package org.gcube.data.access.storagehub.handlers.items.builders;

import java.net.URL;
import java.util.Objects;

public class URLCreationParameters extends CreateParameters {

	String name;
	String description; 
	URL url;
	
	
	protected URLCreationParameters() {}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public URL getUrl() {
		return url;
	}

	@Override
	protected boolean isValid() {
		return Objects.nonNull(name) && Objects.nonNull(description) && Objects.nonNull(url);
	}

	@Override
	public String toString() {
		return "URLCreationParameters [name=" + name + ", description=" + description + ", url=" + url + ", parentId="
				+ parentId + ", user=" + user + "]";
	}

	public static URLCreationBuilder builder() {
		return new URLCreationBuilder();
	}
	
	public static class URLCreationBuilder extends ItemsParameterBuilder<URLCreationParameters>{

		private URLCreationBuilder() {
			super(new URLCreationParameters());
		}
		
		public URLCreationBuilder name(String name) {
			parameters.name = name;
			return this;		
		}

		public URLCreationBuilder description(String description) {
			parameters.description = description;
			return this;
		}
		
		public URLCreationBuilder url(URL url) {
			parameters.url = url;
			return this;
		}

	}
	
	@Override
	public ManagedType getMangedType() {
		return ManagedType.URL;
	}
}
