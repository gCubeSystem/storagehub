package org.gcube.data.access.storagehub.handlers.content;

import java.io.InputStream;
import java.util.Calendar;

import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.microsoft.ooxml.OOXMLParser;
import org.apache.tika.sax.BodyContentHandler;
import org.gcube.common.storagehub.model.items.GenericFileItem;
import org.gcube.common.storagehub.model.items.nodes.Content;
import org.gcube.common.storagehub.model.types.ItemAction;


public class OfficeAppHandler implements ContentHandler{

	Content content = new Content();

	@Override
	public boolean requiresInputStream() {
		return true;
	}	
	
	@Override
	public void initiliseSpecificContent(InputStream is, String filename, String mimeType, long size) throws Exception {
		//detecting the file type
		BodyContentHandler handler = new BodyContentHandler();
		Metadata metadata = new Metadata();
		ParseContext pcontext = new ParseContext();

		//OOXml parser
		OOXMLParser  msofficeparser = new OOXMLParser (); 
		msofficeparser.parse(is, handler, metadata,pcontext);
		String appname = metadata.get("Application-Name");
		content.setMimeType("application/"+appname);
	}

	@Override
	public Content getContent() {
		return content;
	}

	@Override
	public GenericFileItem buildItem(String name, String description, String login) {
		GenericFileItem item = new GenericFileItem();
		Calendar now = Calendar.getInstance();
		item.setName(name);
		item.setTitle(name);
		item.setDescription(description);
		//item.setCreationTime(now);
		item.setHidden(false);
		item.setLastAction(ItemAction.CREATED);
		item.setLastModificationTime(now);

		item.setLastModifiedBy(login);
		item.setOwner(login);
		item.setContent(this.content);
		return item;
	}

}