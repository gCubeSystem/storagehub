package org.gcube.data.access.storagehub.services.interfaces;

import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.gcube.common.storagehub.model.acls.ACL;
import org.gcube.common.storagehub.model.acls.AccessType;
import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.InvalidCallParameters;
import org.gcube.common.storagehub.model.exceptions.InvalidItemException;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.exceptions.UserNotAuthorizedException;
import org.gcube.common.storagehub.model.items.Item;

public interface ACLManagerInterface {

	/**
	 * returns the AccessType for all the users in a shared folder
	 *
	 * @exception {@link RepositoryException} when a generic jcr error occurs
	 * @exception {@link UserNotAuthorizedException} when the caller is not authorized to access to the shared folder
	 */
	List<ACL> getByItem(Item item, Session session) throws RepositoryException, BackendGenericError;

	List<ACL> get(Node node, Session session) throws RepositoryException, BackendGenericError;
	
	/**
	 * Set a new AccessType for a user in a shared folder or VRE folder
	 * 
	 * 
	 * @param String user
	 * @param accessType accessType
	 * 
	 * @exception {@link RepositoryException} when a generic jcr error occurs
	 * @exception {@link UserNotAuthorizedException} when the caller is not ADMINISTRATOR of the shared folder
	 * @exception {@link InvalidCallParameters} when the folder is not shared with the specified user
	 * @exception {@link InvalidItemException} when the folder is not share
	 */
	void update(String targetUser, Node node, AccessType accessType, Session session) throws RepositoryException, StorageHubException;

	void delete(String targetUser, Node node, Session session)
			throws RepositoryException, StorageHubException;

	

}