package org.gcube.data.access.storagehub;

import java.util.Arrays;
import java.util.List;

import javax.jcr.SimpleCredentials;

public class Constants {

	public static final String OLD_VRE_FOLDER_PARENT_NAME = "MySpecialFolders";
	
	public static final String PERSONAL_VRES_FOLDER_PARENT_NAME = "VREs";
	
	public static final String INBOX_FOLDER_NAME = "InBox";
	
	public static final String OUTBOX_FOLDER_NAME = "OutBox";
	
	public static final String ATTACHMENTNODE_NAME = "hl:attachments";
	
	public static final String SHARED_WITH_ME_PARENT_NAME = "SharedWithMe";
	
	//public static final String MYSHARED_PARENT_NAME = "MyShared";
	
	public static final String SHARED_FOLDER_PATH = "/Share";
	
	public static final String WORKSPACE_ROOT_FOLDER_NAME ="Workspace";
	
	public static final String TRASH_ROOT_FOLDER_NAME ="Trash";
	
	public static final String QUERY_LANGUAGE ="JCR-SQL2";
	
	public static final String HOME_VERSION_PROP = "hl:version";
	
	public static final List<String> FOLDERS_TO_EXLUDE = Arrays.asList(Constants.OLD_VRE_FOLDER_PARENT_NAME, Constants.TRASH_ROOT_FOLDER_NAME);
	
	public static final List<String> WRITE_PROTECTED_FOLDER = Arrays.asList(Constants.OLD_VRE_FOLDER_PARENT_NAME, Constants.TRASH_ROOT_FOLDER_NAME);
	
	public static final List<String> PROTECTED_FOLDER = Arrays.asList(Constants.WORKSPACE_ROOT_FOLDER_NAME, Constants.OLD_VRE_FOLDER_PARENT_NAME, Constants.TRASH_ROOT_FOLDER_NAME);
	
	public static final String ADMIN_USER ="admin";
	
	public static final SimpleCredentials JCR_CREDENTIALS = new SimpleCredentials(ADMIN_USER,"admin".toCharArray());

}
