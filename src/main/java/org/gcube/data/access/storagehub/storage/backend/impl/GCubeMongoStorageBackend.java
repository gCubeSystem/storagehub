package org.gcube.data.access.storagehub.storage.backend.impl;

import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.storagehub.model.exceptions.StorageIdNotFoundException;
import org.gcube.common.storagehub.model.items.nodes.Content;
import org.gcube.common.storagehub.model.items.nodes.PayloadBackend;
import org.gcube.common.storagehub.model.storages.MetaInfo;
import org.gcube.common.storagehub.model.storages.StorageBackend;
import org.gcube.contentmanagement.blobstorage.service.IClient;
import org.gcube.contentmanager.storageclient.wrapper.AccessType;
import org.gcube.contentmanager.storageclient.wrapper.MemoryType;
import org.gcube.contentmanager.storageclient.wrapper.StorageClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GCubeMongoStorageBackend extends StorageBackend {
	
	
	private static final Logger log = LoggerFactory.getLogger(GCubeMongoStorageBackend.class);
	
	private final static String SERVICE_NAME 				= "home-library";	
	private final static String SERVICE_CLASS 				= "org.gcube.portlets.user";

	
	public GCubeMongoStorageBackend(PayloadBackend payloadConf) {
		super(payloadConf);
	}
	
	@Override
	public InputStream download(Content content) throws StorageIdNotFoundException {
		return download(content.getStorageId());
	}

	@Override
	public InputStream download(String id) throws StorageIdNotFoundException{
		IClient storageClient = getStorageClient(SecretManagerProvider.get().getOwner().getId()).getClient();
		if (!storageClient.exist().RFile(id))
			throw new StorageIdNotFoundException(id, this.getPayloadConfiguration().getStorageName());
		return storageClient.get().RFileAsInputStream(id);
	}

	protected StorageClient getStorageClient(String login){
		return new StorageClient(SERVICE_CLASS, SERVICE_NAME, login, AccessType.SHARED, MemoryType.PERSISTENT);
	}

	
	@Override
	public MetaInfo onCopy(Content content, String newParentPath, String newName) {
		log.info("copying storage Id {} to newPath {}", content.getStorageId(), newParentPath);	
		String newRemotePath = Paths.get(newParentPath, newName).toString();
		String newStorageID = getStorageClient(SecretManagerProvider.get().getOwner().getId()).getClient().copyFile(true).from(content.getStorageId()).to(newRemotePath);
		log.info("The id returned by storage is {}", newStorageID);	
		return new MetaInfo(content.getSize(),newStorageID, newRemotePath, getPayloadConfiguration());		
	}

	@Override
	public MetaInfo onMove(Content content,  String newParentPath) {
		//new contentPath can be set as remotePath to the storage backend ?
		return new MetaInfo(content.getSize(),content.getStorageId(), content.getRemotePath(), getPayloadConfiguration());
	}

	@Override
	public MetaInfo upload(InputStream stream, String relPath, String name, String user) {
		log.debug("uploading file");
		IClient storageClient = getStorageClient(user).getClient();
		String uid = UUID.randomUUID().toString();
		String remotePath= String.format("%s/%s-%s",relPath,uid,name);
		String storageId =storageClient.put(true).LFile(stream).RFile(remotePath);
		long size = storageClient.getSize().RFileById(storageId);
		MetaInfo info = new MetaInfo(size, storageId, remotePath, getPayloadConfiguration());
		return info;
	}
	
		
	@Override
	public MetaInfo upload(InputStream stream, String relPath, String name, Long size, String user) {
		return this.upload(stream, relPath, name, user);
	}
		
	
	@Override
	public MetaInfo upload(InputStream stream, String relativePath, String name, String storageId, Long size, String user) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void delete(String storageId) {
		log.debug("deleting object {} ",storageId);
		IClient storageClient = getStorageClient(SecretManagerProvider.get().getOwner().getId()).getClient();
		storageClient.remove().RFileById(storageId);
	}

	@Override
	public String getTotalSizeStored() {
		IClient storageClient = getStorageClient(SecretManagerProvider.get().getOwner().getId()).getClient();
		return storageClient.getTotalUserVolume();
	}

	@Override
	public String getTotalItemsCount() {
		IClient storageClient = getStorageClient(SecretManagerProvider.get().getOwner().getId()).getClient();
		return storageClient.getUserTotalItems();
	}

	@Override
	public Map<String, String> getFileMetadata(String id) {
		return Collections.emptyMap();
	}


}