package org.gcube.data.access.storagehub.handlers.content;

import java.io.InputStream;

import org.gcube.common.storagehub.model.items.AbstractFileItem;
import org.gcube.common.storagehub.model.items.nodes.Content;

public interface ContentHandler {
	
	boolean requiresInputStream();
	
	default void initiliseSpecificContent(InputStream is, String fileName, String mimeType, long size) throws Exception{
		throw new UnsupportedOperationException();
	}
	
	default void initiliseSpecificContent(String fileName, String mimeType) throws Exception {
		throw new UnsupportedOperationException();
	}
	
	Content getContent();
			
	AbstractFileItem buildItem(String name, String description, String login);

}
