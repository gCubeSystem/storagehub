package org.gcube.data.access.storagehub.services;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.RepositoryException;

import org.apache.jackrabbit.api.JackrabbitSession;
import org.gcube.common.authorization.control.annotations.AuthorizationControl;
import org.gcube.common.gxrest.response.outbound.GXOutboundErrorResponse;
import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.IdNotFoundException;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.service.UsersList;
import org.gcube.common.storagehub.model.types.SHUBUser;
import org.gcube.data.access.storagehub.Constants;
import org.gcube.data.access.storagehub.StorageHubApplicationManager;
import org.gcube.data.access.storagehub.repository.StoragehubRepository;
import org.gcube.data.access.storagehub.services.delegates.UserManagerDelegate;
import org.gcube.smartgears.annotations.ManagedBy;
import org.gcube.smartgears.utils.InnerMethodName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.webcohesion.enunciate.metadata.DocumentationExample;
import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * Manage users
 */
@Path("users")
@ManagedBy(StorageHubApplicationManager.class)
@RequestHeaders({
		@RequestHeader(name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>"), })
public class UserManager {

	private static final String INFRASTRUCTURE_MANAGER_ROLE = "Infrastructure-Manager";

	private static final Logger log = LoggerFactory.getLogger(UserManager.class);

	private final StoragehubRepository repository = StoragehubRepository.repository;

	@Inject
	UserManagerDelegate userHandler;

	/**
	 * Get a list of users
	 *
	 * @return list of users 
	 */
	@GET
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "Success."),
	})
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public UsersList getUsers() {
		InnerMethodName.set("getUsers");
		JackrabbitSession session = null;
		try {
			session = (JackrabbitSession) repository.getRepository().login(Constants.JCR_CREDENTIALS);
			return new UsersList(userHandler.getAllUsers(session));
		} catch (Throwable e) {
			log.error("jcr error getting users", e);
			GXOutboundErrorResponse.throwException(new BackendGenericError(e));
		} finally {
			if (session != null)
				session.logout();
		}
		return null;
	}

	/**
	 * Get user details 
	 *
	 * @param user user name
	 * @return user detail
	 */
	@GET
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "User found."),
		@ResponseCode ( code = 406, condition = "User does not exist."),
	})
	@Path("{user}")
	@Produces(MediaType.APPLICATION_JSON)
	public SHUBUser getUser(@PathParam("user") String user) {

		InnerMethodName.set("getUser");


		JackrabbitSession session = null;
		try {
			session = (JackrabbitSession) repository.getRepository().login(Constants.JCR_CREDENTIALS);
			return userHandler.getUser(session, user);
		} catch (StorageHubException se) {
			log.error("error getting user", se);
			GXOutboundErrorResponse.throwException(se);
		} catch (Exception e) {
			log.error("jcr error getting user", e);
			GXOutboundErrorResponse.throwException(new BackendGenericError(e));
		} finally {
			if (session != null)
				session.logout();
		}

		GXOutboundErrorResponse.throwException(new IdNotFoundException(user));

		return null;
	}

	/**
	 * Create a new user <br>
	 * <strong>Only users with <code>Infrastructure-Manager</code> role allowed</strong>
	 *
	 * @param user user name
	 * @param password user password
	 * @return user name
	 * @responseExample name.surname
	 */
	@DocumentationExample(" ...\n\nuser=nome.utente&password=passw0rd")
	@POST
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "User created."),
		@ResponseCode ( code = 400, condition = "Wrong set of parameters."),
		@ResponseCode ( code = 403, condition = "You're not allowed to create users."),
		@ResponseCode ( code = 415, condition = "Wrong content type."),
	})
	@Path("")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	@AuthorizationControl(allowedRoles = { INFRASTRUCTURE_MANAGER_ROLE })
	public String createUser(@FormParam("user") String user, @FormParam("password") String password) {

		InnerMethodName.set("createUser");

		JackrabbitSession session = null;
		String userId = null;
		try {
			session = (JackrabbitSession) repository.getRepository().login(Constants.JCR_CREDENTIALS);

			userId = userHandler.createUser(session, user, password);

			session.save();
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} catch (RepositoryException re) {
			log.error("jcr error creating item", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error creating item", re));
		} finally {
			if (session != null)
				session.logout();
		}

		return userId;
	}

	/**
	 * Update user to the last 'home' version <br>
	 * <strong>Only users with <code>Infrastructure-Manager</code> role allowed</strong>
	 *
	 * @param user user name
	 * @return user id
	 */
	@PUT
	@Path("{user}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "Home update done."),
		@ResponseCode ( code = 403, condition = "You're not allowed to create users."),
		@ResponseCode ( code = 415, condition = "Wrong content type."),
	})
	@AuthorizationControl(allowedRoles = { INFRASTRUCTURE_MANAGER_ROLE })
	public String updateHomeUserToLatestVersion(@PathParam("user") String user) {

		InnerMethodName.set("updateHomeUserToLatestVersion");

		JackrabbitSession session = null;
		String userId = null;
		try {
			session = (JackrabbitSession) repository.getRepository().login(Constants.JCR_CREDENTIALS);

			userId = userHandler.updateHomeUserToLatestVersion(session, userId);
			
			session.save();
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} catch (RepositoryException re) {
			log.error("jcr error creating item", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error creating item", re));
		} finally {
			if (session != null)
				session.logout();
		}

		return userId;
	}

	/**
	 * Delete a user <br>
	 * <strong>Only users with <code>Infrastructure-Manager</code> role allowed</strong>
	 *
	 * @param user user name
	 * @return user name
	 */
	@DELETE
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "User deleted."),
		@ResponseCode ( code = 403, condition = "You're not allowed to delete users."),
		@ResponseCode ( code = 406, condition = "User does not exist."),
	})
	@Path("{user}")
	@Produces(MediaType.TEXT_PLAIN)
	@AuthorizationControl(allowedRoles = { INFRASTRUCTURE_MANAGER_ROLE })
	public String deleteUser(@PathParam("user") final String user) {

		InnerMethodName.set("deleteUser");

		JackrabbitSession session = null;
		try {

			session = (JackrabbitSession) repository.getRepository().login(Constants.JCR_CREDENTIALS);

			userHandler.deleteUser(session, user);
			
			session.save();
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} catch (RepositoryException re) {
			log.error("jcr error removing item", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error removing item", re));
		} finally {
			if (session != null)
				session.logout();
		}

		return user;
	}

	/**
	 * Get a list of groups for the specified user 
	 *
	 * @param user user name
	 * @return List of groups
	 */
	@GET
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "User found."),
		@ResponseCode ( code = 500, condition = "User does not exist."),
	})
	@Path("{user}/groups")
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getGroupsPerUser(@PathParam("user") final String user) {

		InnerMethodName.set("getGroupsPerUser");

		JackrabbitSession session = null;
		List<String> groups = new ArrayList<>();
		try {
			session = (JackrabbitSession) repository.getRepository().login(Constants.JCR_CREDENTIALS);

			groups = userHandler.getGroupsPerUser(session, user);
		} catch (RepositoryException re) {
			log.error("jcr error creating item", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error creating item", re));
		} finally {
			if (session != null)
				session.logout();
		}
		return groups;
	}

	
}
