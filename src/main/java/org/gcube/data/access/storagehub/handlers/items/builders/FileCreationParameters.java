package org.gcube.data.access.storagehub.handlers.items.builders;

import java.io.InputStream;
import java.util.Objects;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

public class FileCreationParameters extends CreateParameters {

	String name;
	String description; 
	FormDataContentDisposition fileDetails;
	InputStream stream;
	
	
	protected FileCreationParameters() {}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}
	
	public FormDataContentDisposition getFileDetails() {
		return fileDetails;
	}

	public InputStream getStream() {
		return stream;
	}

	@Override
	protected boolean isValid() {
		return Objects.nonNull(name) && Objects.nonNull(description) && Objects.nonNull(stream);
	}
	
	@Override
	public String toString() {
		return "FileCreationParameters [name=" + name + ", description=" + description + ", fileDetails=" + fileDetails
				+ ", parentId=" + parentId + ", user=" + user + "]";
	}

	public static FileCreationBuilder builder() {
		return new FileCreationBuilder();
	}
	
	public static class FileCreationBuilder extends ItemsParameterBuilder<FileCreationParameters>{

		private FileCreationBuilder() {
			super(new FileCreationParameters());
		}
		
		public FileCreationBuilder name(String name) {
			parameters.name = name;
			return this;		
		}

		public FileCreationBuilder description(String description) {
			parameters.description = description;
			return this;
		}
		
		public FileCreationBuilder stream(InputStream stream) {
			parameters.stream = stream;
			return this;
		}
		
		public FileCreationBuilder fileDetails(FormDataContentDisposition fileDetails) {
			parameters.fileDetails = fileDetails;
			return this;
		}
		
	}

	@Override
	public ManagedType getMangedType() {
		return ManagedType.FILE;
	}

}
