package org.gcube.data.access.storagehub.repository;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import javax.jcr.Repository;
import javax.jcr.Session;
import javax.jcr.nodetype.NodeType;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.jackrabbit.api.JackrabbitRepository;
import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.JackrabbitWorkspace;
import org.apache.jackrabbit.api.security.authorization.PrivilegeManager;
import org.apache.jackrabbit.commons.cnd.CndImporter;
import org.gcube.data.access.storagehub.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JackrabbitRepositoryImpl implements StoragehubRepository {

	private static final Logger log = LoggerFactory.getLogger(JackrabbitRepositoryImpl.class);
	
	private Repository repository;
	
	public JackrabbitRepositoryImpl(){
		try {
			InitialContext context = new InitialContext();
			Context environment = (Context) context.lookup("java:comp/env");
			repository = (Repository) environment.lookup("jcr/repository");
		}catch (Throwable e) {
			log.error("error initializing repository", e);
			throw new RuntimeException("error initializing repository",e);
		}
	}
	
	@Override
	public Repository getRepository() {
		return repository;
	}
	
	private boolean jackrabbitInitialized = false;


	@Override
	public synchronized void initContainerAtFirstStart() {
		try {
			JackrabbitSession ses = (JackrabbitSession) this.repository.login(Constants.JCR_CREDENTIALS);
			try {
				boolean notAlreadyDone = !jackrabbitInitialized && !ses.getRootNode().hasNode("Home");
				if (notAlreadyDone) 
					this.init(ses);
				else log.info("jackrabbit is already initialized");
			}finally {
				ses.logout();
			}
		} catch (Exception e) {
			log.warn("error initialising Jackrabbit",e);
		}
		jackrabbitInitialized = true;
	}
	
	@Override
	public void shutdown() {
		((JackrabbitRepository)repository).shutdown();
	}

	public void init(JackrabbitSession ses) throws Exception{
		log.info("init started");
		try {
			initNodeTypes(ses);
			ses.getRootNode().addNode("Home");
			ses.getRootNode().addNode("Share");
			PrivilegeManager pm = ((JackrabbitWorkspace) ses.getWorkspace()).getPrivilegeManager();
			pm.registerPrivilege("hl:writeAll", false, new String[0]);
			ses.save();
		}catch (Exception e) {
			log.error("init error", e);
			throw e;
		}
		log.info("init finished");
	}

	void initNodeTypes(Session ses) throws Exception{
		InputStream stream = this.getClass().getResourceAsStream("/init/NodeType.cnd");

		if (stream == null)
			throw new Exception("NodeType.cnd inputStream is null");

		InputStreamReader inputstream = new InputStreamReader(stream, Charset.forName("UTF-8"));
		// Register the custom node types defined in the CND file, using JCR Commons CndImporter
		
		log.info("start to register the custom node types defined in the CND file...");
		

		NodeType[] nodeTypes = CndImporter.registerNodeTypes(inputstream, ses, true);

		for (NodeType nt : nodeTypes) 
			log.info("Registered: {} ", nt.getName());
		

		log.info("custom node types registered");

	}
	
}
