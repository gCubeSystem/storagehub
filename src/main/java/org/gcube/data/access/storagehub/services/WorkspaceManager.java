package org.gcube.data.access.storagehub.services;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.jackrabbit.api.JackrabbitSession;
import org.gcube.common.gxrest.response.outbound.GXOutboundErrorResponse;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.common.storagehub.model.Excludes;
import org.gcube.common.storagehub.model.Paths;
import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.InvalidCallParameters;
import org.gcube.common.storagehub.model.exceptions.InvalidItemException;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.exceptions.UserNotAuthorizedException;
import org.gcube.common.storagehub.model.items.FolderItem;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.common.storagehub.model.items.SharedFolder;
import org.gcube.common.storagehub.model.items.TrashItem;
import org.gcube.common.storagehub.model.items.nodes.PayloadBackend;
import org.gcube.common.storagehub.model.service.ItemList;
import org.gcube.common.storagehub.model.service.ItemWrapper;
import org.gcube.common.storagehub.model.storages.MetaInfo;
import org.gcube.common.storagehub.model.storages.StorageBackend;
import org.gcube.common.storagehub.model.storages.StorageBackendFactory;
import org.gcube.data.access.storagehub.AuthorizationChecker;
import org.gcube.data.access.storagehub.Constants;
import org.gcube.data.access.storagehub.PathUtil;
import org.gcube.data.access.storagehub.Range;
import org.gcube.data.access.storagehub.StorageHubApplicationManager;
import org.gcube.data.access.storagehub.Utils;
import org.gcube.data.access.storagehub.handlers.PublicLinkHandler;
import org.gcube.data.access.storagehub.handlers.TrashHandler;
import org.gcube.data.access.storagehub.handlers.items.Item2NodeConverter;
import org.gcube.data.access.storagehub.handlers.items.Node2ItemConverter;
import org.gcube.data.access.storagehub.handlers.items.builders.FolderCreationParameters;
import org.gcube.data.access.storagehub.handlers.plugins.StorageBackendHandler;
import org.gcube.data.access.storagehub.handlers.vres.VRE;
import org.gcube.data.access.storagehub.handlers.vres.VREManager;
import org.gcube.data.access.storagehub.predicates.IncludeTypePredicate;
import org.gcube.data.access.storagehub.predicates.ItemTypePredicate;
import org.gcube.data.access.storagehub.query.sql2.evaluators.Evaluators;
import org.gcube.data.access.storagehub.repository.StoragehubRepository;
import org.gcube.data.access.storagehub.services.delegates.UserManagerDelegate;
import org.gcube.data.access.storagehub.storage.backend.impl.GCubeVolatileStorageBackendFactory;
import org.gcube.smartgears.annotations.ManagedBy;
import org.gcube.smartgears.utils.InnerMethodName;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.webcohesion.enunciate.metadata.DocumentationExample;
import com.webcohesion.enunciate.metadata.Ignore;
import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResourceMethodSignature;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * Manage the workspace
 */
@Path("/")
@ManagedBy(StorageHubApplicationManager.class)
@RequestHeaders({
		@RequestHeader(name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>"), })
public class WorkspaceManager extends Impersonable {

	private static final Logger log = LoggerFactory.getLogger(WorkspaceManager.class);

	private final StoragehubRepository repository = StoragehubRepository.repository;

	@Inject
	Evaluators evaluator;

	@Inject
	PathUtil pathUtil;

	@Inject
	VREManager vreManager;

	@Context
	ServletContext context;

	@Inject
	AuthorizationChecker authChecker;

	@Inject
	TrashHandler trashHandler;

	@Inject
	StorageBackendHandler storageBackendHandler;

	@Inject
	PublicLinkHandler publicLinkHandler;

	@Inject
	UserManagerDelegate userHandler;
	
	@RequestScoped
	@QueryParam("exclude")
	private List<String> excludes = Collections.emptyList();

	@Inject
	Node2ItemConverter node2Item;
	@Inject
	Item2NodeConverter item2Node;

	/**
	 * Get info of an item referred through its path
	 * 
	 * @param relPath relative path
	 * @param exclude a list of fields to exclude from the returned item<br>
	 *   <strong>Multivalued</strong><br>
	 *   <strong>Optional</strong>
	 * @return workspace item
	 * @pathExample /?relPath=folder1/folder2/file.txt&exclude=owner
	 */
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "Item retrieved."),
		@ResponseCode ( code = 406, condition = "Item not found."),
	})
	@ResourceMethodSignature(output = ItemWrapper.class, queryParams = { @QueryParam("relPath"), @QueryParam("exclude") })
	@Path("/")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ItemWrapper<Item> getWorkspace(@QueryParam("relPath") String relPath) {
		InnerMethodName.set("getWorkspace");
		Session ses = null;
		org.gcube.common.storagehub.model.Path absolutePath;
		if (relPath == null)
			absolutePath = pathUtil.getWorkspacePath(currentUser);
		else
			absolutePath = Paths.append(pathUtil.getWorkspacePath(currentUser), relPath);

		Item toReturn = null;
		try {
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			// TODO: remove when all user will have TRASH
			org.gcube.common.storagehub.model.Path trashPath = pathUtil.getTrashPath(currentUser, ses);
			if (!ses.nodeExists(trashPath.toPath())) {
				Node wsNode = ses.getNode(pathUtil.getWorkspacePath(currentUser).toPath());
				FolderCreationParameters trashFolderParameters = FolderCreationParameters.builder()
						.name(Constants.TRASH_ROOT_FOLDER_NAME).description("trash of " + currentUser)
						.author(currentUser).on(wsNode.getIdentifier()).with(ses).build();
				Utils.createFolderInternally(trashFolderParameters, null, true);
				ses.save();
			}
			Node node = ses.getNode(absolutePath.toPath());
			authChecker.checkReadAuthorizationControl(ses, currentUser, node.getIdentifier());
			toReturn = node2Item.getItem(node, excludes);
		} catch (RepositoryException re) {
			log.error("jcr error getting workspace item", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError(re));
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} 
		finally {
			if (ses != null)
				ses.logout();
		}
		return new ItemWrapper<Item>(toReturn);
	}

	/**
	 * Upload a file in the volatile area
	 * 
	 * @param file  multipart/form-data file parameter, with optional
	 *              'filename' and 'size' (see example below)
	 * @param exclude a list of fields to exclude from the returned item<br>
	 *   <strong>Multivalued</strong><br>
	 *   <strong>Optional</strong>
	 * @return public link to the created file 
	 * @responseExample https://data.dev.d4science.org/shub/VLT_V1...9PQ==
	 */
	@ResourceMethodSignature(output = String.class, formParams = { @FormParam("file") }, queryParams = { @QueryParam("exclude") })
	@DocumentationExample(value = "...\n\n--------boundaryString\n" +
		"Content-Disposition: form-data; name=\"file\"; filename=\"doc.pdf\"; size=426018;\n" +
		"Content-Type: application/pdf\n\n" +
		"(data)\n" +
		"--------boundaryString--")
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "File created."),
		@ResponseCode ( code = 400, condition = "File not provided."),
		@ResponseCode ( code = 415, condition = "Wrong content type."),
	  	@ResponseCode ( code = 500, condition = "Wrong set of parameters."),
	})
	@Path("volatile")
	public String uploadVolatileFile(@FormDataParam("file") InputStream file,
			@FormDataParam("file") FormDataContentDisposition fileDetail) {
		InnerMethodName.set("uploadToVolatileArea");

		log.info("uploading file {} of size {} to volatile area ({} - {})", fileDetail.getFileName(),
				fileDetail.getSize(), fileDetail.getName(), fileDetail.getParameters().toString());

		String toReturn = null;
		try {

			long size = fileDetail.getSize();

			PayloadBackend payloadBackend = new PayloadBackend(GCubeVolatileStorageBackendFactory.NAME, null);
			StorageBackendFactory sbf = storageBackendHandler.get(payloadBackend);
			StorageBackend sb = sbf.create(payloadBackend);

			log.info("UPLOAD: call started with file size {}", size);
			MetaInfo info = sb.upload(file, null, fileDetail.getFileName(), currentUser);
			log.debug("UPLOAD: call finished");

			toReturn = publicLinkHandler.getForVolatile(info.getStorageId(), GCubeVolatileStorageBackendFactory.NAME,
					context);
		} catch (StorageHubException se) {
			log.error("error uploading file to volatile area", se);
			GXOutboundErrorResponse.throwException(se);
		} catch (Throwable e) {
			log.error("error uploading file to volatile area", e);
			GXOutboundErrorResponse.throwException(new BackendGenericError(e));
		}
		return toReturn;
	}

	/**
	 * Get info on the VRE folder associated to the current token
	 * 
	 * @param exclude a list of fields to exclude from the returned item<br>
	 *   <strong>Multivalued</strong><br>
	 *   <strong>Optional</strong>
	 * @return VRE folder info
	 */
	@ResourceMethodSignature(output = ItemWrapper.class, queryParams = { @QueryParam("exclude") })
	@Path("vrefolder")
	@GET
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "Success."),
	})
	@Produces(MediaType.APPLICATION_JSON)
	public ItemWrapper<Item> getCurrentVreFolder() {
		InnerMethodName.set("getCurrentVreFolder");
		JackrabbitSession ses = null;
		Item vreItem = null;
		try {
			ses = (JackrabbitSession) repository.getRepository().login(Constants.JCR_CREDENTIALS);
			
			Secret secret = SecretManagerProvider.get();
			
			String groupName= vreManager.retrieveGroupNameFromContext(secret.getContext());
			
			if (!userHandler.getGroupsPerUser(ses, currentUser).contains(groupName))
				throw new InvalidCallParameters("error getting VRE with user "+currentUser+" for group "+groupName);
			
			vreItem = vreManager.getVreFolderItemByGroupName(ses, groupName, excludes).getVreFolder();
		} catch (RepositoryException re) {
			log.error("jcr error getting vrefolder", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError(re));
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} finally {
			if (ses != null)
				ses.logout();
		}
		return new ItemWrapper<Item>(vreItem);
	}

	/**
	 * Get a list of recent documents from VRE folders 
	 * 
	 * @param exclude a list of fields to exclude from the returned item<br>
	 *   <strong>Multivalued</strong><br>
	 *   <strong>Optional</strong>
	 * @return list of recent documents
	 */
	@ResourceMethodSignature(output = ItemList.class, queryParams = { @QueryParam("exclude") })
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "Success."),
	})
	@Path("vrefolder/recents")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ItemList getVreFolderRecentsDocument() {
		InnerMethodName.set("getVreFolderRecents");
		JackrabbitSession ses = null;
		List<Item> recentItems = Collections.emptyList();
		try {
			ses = (JackrabbitSession) repository.getRepository().login(Constants.JCR_CREDENTIALS);

			Secret secret = SecretManagerProvider.get();
			
			String groupName= vreManager.retrieveGroupNameFromContext(secret.getContext());
			
			if (!userHandler.getGroupsPerUser(ses, currentUser).contains(groupName))
				throw new InvalidCallParameters("error getting VRE with user "+currentUser+" for group "+groupName);
			
			VRE vre = vreManager.getVreFolderItemByGroupName(ses, groupName, excludes);
			
			log.trace("VRE retrieved {}", vre.getVreFolder().getTitle());
			recentItems = vre.getRecents();
			log.trace("recents retrieved {}", vre.getVreFolder().getTitle());
			return new ItemList(recentItems);
		} catch (RepositoryException re) {
			log.error("jcr error getting recents", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError(re));
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} finally {
			if (ses != null)
				ses.logout();
		}

		return new ItemList(recentItems);

	}

	/**
	 * Get info on the trash folder 
	 * 
	 * @param exclude a list of fields to exclude from the returned item<br>
	 *   <strong>Multivalued</strong><br>
	 *   <strong>Optional</strong>
	 * @return info on the trash folder
	 */
	@ResourceMethodSignature(output = ItemWrapper.class, queryParams = { @QueryParam("exclude") })
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "Success."),
	})
	@Path("trash")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ItemWrapper<Item> getTrashRootFolder() {
		InnerMethodName.set("getTrashRootFolder");
		Session ses = null;

		Item item = null;
		try {
			long start = System.currentTimeMillis();
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			org.gcube.common.storagehub.model.Path trashPath = pathUtil.getTrashPath(currentUser, ses);
			log.info("time to connect to repo  {}", (System.currentTimeMillis() - start));

			Node folder = ses.getNode(trashPath.toPath());
			item = node2Item.getItem(folder, excludes);
		} catch (RepositoryException re) {
			log.error("jcr error getting trash", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError(re));
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} finally {
			if (ses != null)
				ses.logout();
		}

		return new ItemWrapper<Item>(item);
	}

	/**
	 * Empty the trash folder 
	 * 
	 * @param exclude a list of fields to exclude from the returned item<br>
	 *   <strong>Multivalued</strong><br>
	 *   <strong>Optional</strong>
	 * @return trash folder id
	 * @responseExample text/plain 5f4b3b4e-4b3b- ... -4b3b4e4b3b4e
	 */
	@ResourceMethodSignature(output = String.class, queryParams = { @QueryParam("exclude") })
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "Trash emptied."),
	})
	@Path("trash/empty")
	@Produces(MediaType.TEXT_PLAIN)
	@DELETE
	public String emptyTrash() {
		InnerMethodName.set("emptyTrash");
		Session ses = null;
		String toReturn = null;
		try {
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			org.gcube.common.storagehub.model.Path trashPath = pathUtil.getTrashPath(currentUser, ses);
			Node trashNode = ses.getNode(trashPath.toPath());
			List<Item> itemsToDelete = Utils.getItemList(trashNode, Excludes.ALL, null, true, null);
			trashHandler.removeNodes(ses, itemsToDelete);
			toReturn = trashNode.getIdentifier();
		} catch (RepositoryException re) {
			log.error("jcr error emptying trash", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError(re));
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} finally {
			if (ses != null)
				ses.logout();
		}

		return toReturn;
	}

	/**
	 * Restore a trashed item.
	 *
	 * @param trashedItemId item id
	 * @param destinationId destination folder id
	 * @param exclude a list of fields to exclude from the returned item<br>
	 *   <strong>Multivalued</strong><br>
	 *   <strong>Optional</strong>
	 * @return id of the restored item 
	 * @responseExample text/plain 5f4b3b4e-4b3b- ... -4b3b4e4b3b4e
	 */
	@ResourceMethodSignature(output = String.class, queryParams = { @QueryParam("exclude")}, formParams = { @FormParam("trashedItemId"), @FormParam("destinationId") })
	@DocumentationExample("...\n\ntrashedId=17dae181-f33c- ... - 3fa22198dd30&destinationId=19863b4e-b33f- ... -5b6d2e0e1eee")
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "Item restored."),
		@ResponseCode ( code = 406, condition = "Source or destination item does not exist."),
		@ResponseCode ( code = 409, condition = "Source and destination are the same item."),
		@ResponseCode ( code = 415, condition = "Wrong content type."),
	})
	@PUT
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("trash/restore")
	public String restoreItem(@FormParam("trashedItemId") String trashedItemId,
			@FormParam("destinationId") String destinationFolderId) {
		InnerMethodName.set("restoreItem");
		Session ses = null;
		String toReturn = null;
		try {

			log.info("restoring node with id {}", trashedItemId);
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);

			final Node nodeToRestore = ses.getNodeByIdentifier(trashedItemId);

			Item itemToRestore = node2Item.getItem(nodeToRestore, Excludes.ALL);

			if (!(itemToRestore instanceof TrashItem))
				throw new InvalidItemException("Only trash items can be restored");

			org.gcube.common.storagehub.model.Path trashPath = pathUtil.getTrashPath(currentUser, ses);
			if (!itemToRestore.getPath().startsWith(trashPath.toPath()))
				throw new UserNotAuthorizedException("this item is not in the user " + currentUser + " trash");

			Item destinationItem = null;
			if (destinationFolderId != null) {
				destinationItem = node2Item.getItem(ses.getNodeByIdentifier(destinationFolderId), Excludes.ALL);
				if (!(destinationItem instanceof FolderItem))
					throw new InvalidCallParameters("destintation item is not a folder");
				toReturn = trashHandler.restoreItem(ses, (TrashItem) itemToRestore, (FolderItem) destinationItem,
						currentUser);
			} else
				toReturn = trashHandler.restoreItem(ses, (TrashItem) itemToRestore, null, currentUser);

		} catch (RepositoryException re) {
			log.error("error restoring item with id {}", trashedItemId, re);
			GXOutboundErrorResponse.throwException(new BackendGenericError(re));
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} finally {
			if (ses != null) {
				ses.logout();
			}
		}

		return toReturn;
	}

	/**
	 * Get a list of VRE folders for which the user is member
	 * 
	 * @param exclude a list of fields to exclude from the returned item<br>
	 *   <strong>Multivalued</strong><br>
	 *   <strong>Optional</strong>
	 * @return list of VRE folders
	 */
	@ResourceMethodSignature(output = ItemList.class, queryParams = { @QueryParam("exclude") })
	@Path("vrefolders")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ItemList getVreFolders() {
		InnerMethodName.set("getVreFolders");
		Session ses = null;
		List<? extends Item> toReturn = null;
		org.gcube.common.storagehub.model.Path vrePath = null;
		try {
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			vrePath = pathUtil.getVREsPath(currentUser, ses);
			log.info("vres folder path is {}", vrePath.toPath());

			toReturn = Utils.getItemList(ses.getNode(vrePath.toPath()), excludes, null, false, null);
		} catch (RepositoryException re) {
			log.error("error reading the node children of {}", vrePath, re);
			GXOutboundErrorResponse.throwException(new BackendGenericError(re));
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} finally {
			if (ses != null)
				ses.logout();
		}

		return new ItemList(toReturn);
	}

	/**
	 * Get a paged list of VRE folders for which the user is member
	 * 
	 * @param start start index, counting from 0 <br>
	 *   <strong>Possible values:</strong> integers
	 * @param limit maximum number of items returned <br>
	 *   <strong>Possible values:</strong> integers
	 * @param exclude a list of fields to exclude from the returned item<br>
	 *   <strong>Multivalued</strong><br>
	 *   <strong>Optional</strong>
	 * @return list of VRE folders
	 * @pathExample /vrefolders/paged?start=0&limit=10
	 */
	@ResourceMethodSignature(output = ItemList.class, queryParams = { @QueryParam("start"), @QueryParam("limit"), @QueryParam("exclude") })
	@Path("vrefolders/paged")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ItemList getVreFoldersPaged(@QueryParam("start") Integer start, @QueryParam("limit") Integer limit) {
		InnerMethodName.set("getVreFoldersPaged");
		Session ses = null;
		org.gcube.common.storagehub.model.Path vrePath = null;
		List<? extends Item> toReturn = null;
		try {
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			vrePath = pathUtil.getVREsPath(currentUser, ses);
			toReturn = Utils.getItemList(ses.getNode(vrePath.toPath()), excludes, new Range(start, limit), false, null);
		} catch (RepositoryException re) {
			log.error("(paged) error reading the node children of {}", vrePath, re);
			GXOutboundErrorResponse.throwException(new BackendGenericError(re));
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} finally {
			if (ses != null)
				ses.logout();
		}

		return new ItemList(toReturn);
	}


	// TODO: boh! Sviluppo a metà. Valutare se finirlo o ammazzarlo male
	@Ignore
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "List retrieved."),
		@ResponseCode ( code = 406, condition = "Error retrieving shared-with-me folder."),
	})
	@Path("shared-with-me")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ItemList getSharedWithMeFolders() {
		InnerMethodName.set("getSharedWithMeFolders");
		Session ses = null;
		List<? extends Item> toReturn = null;
		org.gcube.common.storagehub.model.Path sharedPath = null;
		try {
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			sharedPath = pathUtil.getSharedWithMePath(currentUser);
			log.info("shared With Me folder path is {}", sharedPath.toPath());
			ItemTypePredicate itemPredicate = new IncludeTypePredicate(SharedFolder.class);
			toReturn = Utils.getItemList(ses.getNode(sharedPath.toPath()), excludes, null, false, itemPredicate);
		} catch (RepositoryException re) {
			log.error("error reading shared with me folder ({})", sharedPath, re);
			GXOutboundErrorResponse.throwException(new BackendGenericError(re));
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} finally {
			if (ses != null)
				ses.logout();
		}

		return new ItemList(toReturn);
	}

	@Ignore
	@Path("count")
	@GET
	@Deprecated
	public String getTotalItemsCount() {
		InnerMethodName.set("getTotalItemsCount");
		Session ses = null;
		try {
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			Node wsNode = ses.getNode(pathUtil.getWorkspacePath(currentUser).toPath());
			return Long.toString(Utils.getFolderInfo(wsNode).getCount());
		} catch (RepositoryException re) {
			log.error("error getting workspace total item count", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError(re));
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} finally {
			if (ses != null)
				ses.logout();
		}

		return "0";
	}

	@Ignore
	@Path("size")
	@GET
	@Deprecated
	public String getTotalVolume() {
		InnerMethodName.set("getTotalSize");
		Session ses = null;
		try {
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			Node wsNode = ses.getNode(pathUtil.getWorkspacePath(currentUser).toPath());
			return Long.toString(Utils.getFolderInfo(wsNode).getSize());
		} catch (RepositoryException re) {
			log.error("error getting workspace total size", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError(re));
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} finally {
			if (ses != null)
				ses.logout();
		}
		return "0";
	}


}
