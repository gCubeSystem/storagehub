package org.gcube.data.access.storagehub.handlers.plugins;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.gcube.common.storagehub.model.exceptions.PluginNotFoundException;
import org.gcube.common.storagehub.model.items.nodes.PayloadBackend;
import org.gcube.common.storagehub.model.storages.StorageBackendFactory;
import org.gcube.common.storagehub.model.storages.StorageNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

@Singleton
public class StorageBackendHandler {

	private static Logger log = LoggerFactory.getLogger(StorageBackendHandler.class);

	public static PayloadBackend getDefaultPayloadForFolder() {
		return new PayloadBackend(StorageNames.DEFAULT_S3_STORAGE, null);
	}

	@Inject
	Instance<StorageBackendFactory> factories;

	Map<String, StorageBackendFactory> storagebackendMap = new HashMap<String, StorageBackendFactory>();

	@PostConstruct
	void init() {
		if (factories != null)
			for (StorageBackendFactory connector : factories) {
				if (storagebackendMap.containsKey(connector.getName())) {
					log.error("multiple storage backend with the same name");
					throw new RuntimeException("multiple storage backend with the same name");
				}
				storagebackendMap.put(connector.getName(), connector);
			}
		else
			throw new RuntimeException("storage backend implementation not found");
	}

	public StorageBackendFactory get(PayloadBackend payload) throws PluginNotFoundException {
		if (payload == null || !storagebackendMap.containsKey(payload.getStorageName()))
			throw new PluginNotFoundException(
					String.format("implementation for storage %s not found", payload.getStorageName()));
		return storagebackendMap.get(payload.getStorageName());
	}

	public StorageBackendFactory get(String storageName) throws PluginNotFoundException {
		return storagebackendMap.get(storageName);
	}

	public Collection<StorageBackendFactory> getAllImplementations() {
		return storagebackendMap.values();
	}
}
