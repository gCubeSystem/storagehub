package org.gcube.data.access.storagehub.handlers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jakarta.inject.Singleton;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.version.Version;
import javax.jcr.version.VersionHistory;
import javax.jcr.version.VersionIterator;
import javax.jcr.version.VersionManager;

import org.apache.jackrabbit.JcrConstants;
import org.gcube.common.storagehub.model.NodeConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class VersionHandler {

	private static final Logger logger = LoggerFactory.getLogger(VersionHandler.class);

	public void makeVersionableContent(Node node){
		try {
			Node contentNode = node.getNode(NodeConstants.CONTENT_NAME);
			contentNode.addMixin(JcrConstants.MIX_VERSIONABLE);
		}catch(Throwable e ) {
			logger.warn("cannot create versioned content node",e);
		}
	}

	public void checkinContentNode(Node node){
		try {
			Session session = node.getSession();
			Node contentNode = node.getNode(NodeConstants.CONTENT_NAME);
			VersionManager versionManager = session.getWorkspace().getVersionManager();
			versionManager.checkin(contentNode.getPath());
		}catch(Exception e ) {
			logger.warn("cannotcheckinNode content node",e);
		}
	}

	public void checkoutContentNode(Node node){
		try {
			Session session = node.getSession();
			Node contentNode = node.getNode(NodeConstants.CONTENT_NAME);
			VersionManager versionManager = session.getWorkspace().getVersionManager();
			versionManager.checkout(contentNode.getPath());
		}catch(Exception e ) {
			logger.warn("cannot checkoutNode content node",e);
		}
	}

	public Version getCurrentVersion(Node node) throws RepositoryException{
		Session session = node.getSession();
		Node contentNode = node.getNode(NodeConstants.CONTENT_NAME);
		VersionManager versionManager = session.getWorkspace().getVersionManager();
		return versionManager.getBaseVersion(contentNode.getPath());
	}

	public List<Version> getContentVersionHistory(Node node) {
		try {
			Session session = node.getSession();
			Node contentNode = node.getNode(NodeConstants.CONTENT_NAME);
			VersionManager versionManager = session.getWorkspace().getVersionManager();
			VersionHistory history = versionManager.getVersionHistory(contentNode.getPath());
			VersionIterator iterator = history.getAllVersions();
			iterator.skip(1);
			List<Version> versions = new ArrayList<>();
			while (iterator.hasNext()) {
				Version version = iterator.nextVersion();
				versions.add(version);
				logger.debug("version name {} with nodeType {}",version.getName(),version.getPrimaryNodeType().getName());
			}
			return versions;
		}catch(Throwable e ) {
			logger.warn("cannot get version history content node",e);
			return Collections.emptyList();
		}
	}

	public void removeContentVersion(Node node, String versionName) throws RepositoryException{
		Node contentNode = node.getNode(NodeConstants.CONTENT_NAME);
		VersionHistory history = contentNode.getSession().getWorkspace().getVersionManager().getVersionHistory(contentNode.getPath());
		history.removeVersion(versionName);

	}

}
