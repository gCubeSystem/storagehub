package org.gcube.data.access.storagehub.handlers;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import jakarta.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.version.Version;

import org.gcube.common.storagehub.model.Excludes;
import org.gcube.common.storagehub.model.Path;
import org.gcube.common.storagehub.model.Paths;
import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.items.AbstractFileItem;
import org.gcube.common.storagehub.model.items.FolderItem;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.common.storagehub.model.storages.StorageBackend;
import org.gcube.common.storagehub.model.storages.StorageBackendFactory;
import org.gcube.data.access.storagehub.Utils;
import org.gcube.data.access.storagehub.accounting.AccountingHandler;
import org.gcube.data.access.storagehub.handlers.plugins.StorageBackendHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CompressHandler {

	private Logger logger = LoggerFactory.getLogger(CompressHandler.class);
	
	@Inject
	StorageBackendHandler storageBackendHandler;
	
	@Inject
	VersionHandler versionHandler;
	
	public Deque<Item> getAllNodesForZip(FolderItem directory, String login, Session session, AccountingHandler accountingHandler, List<String> excludes) throws RepositoryException, BackendGenericError{
		Deque<Item> queue = new LinkedList<Item>();
		Node currentNode = session.getNodeByIdentifier(directory.getId());
		queue.push(directory);
		Deque<Item> tempQueue = new LinkedList<Item>();
		logger.trace("adding directory {}",currentNode.getPath());
		for (Item item : Utils.getItemList(currentNode,Excludes.GET_ONLY_CONTENT, null, false, null)){
			if (excludes.contains(item.getId())) continue;
			if (item instanceof FolderItem) 
				tempQueue.addAll(getAllNodesForZip((FolderItem) item, login, session, accountingHandler, excludes));
			else if (item instanceof AbstractFileItem fileItem){
				logger.trace("adding file {}",item.getPath());
				String versionName = null;
				try {
					Version version = versionHandler.getCurrentVersion((Node) item.getRelatedNode());
					versionName = version.getName();
				}catch(RepositoryException e) {
					logger.warn("current version of {} cannot be retreived", item.getId());
				}
				accountingHandler.createReadObj(fileItem.getTitle(), versionName, session, (Node) item.getRelatedNode(), login, false);
				queue.addLast(item);
			}
		}
		queue.addAll(tempQueue);
		return queue;
	}


	public void zipNode(ZipOutputStream zos, Deque<Item> queue, Path originalPath) throws Exception{
		logger.trace("originalPath is {}",originalPath.toPath());
		Path actualPath = Paths.getPath("");
		while (!queue.isEmpty()) {
			Item item = queue.pop();
			if (item instanceof FolderItem) {
				actualPath = Paths.getPath(item.getPath());
				logger.trace("actualPath is {}",actualPath.toPath());
				String name = Paths.remove(actualPath, originalPath).toPath().replaceFirst("/", "");
				logger.trace("writing dir {}",name);
				if (name.isEmpty()) continue;
				try {
					zos.putNextEntry(new ZipEntry(name));
				}finally {
					zos.closeEntry();
				}
			} else if (item instanceof AbstractFileItem fileItem){
				try {
										
					StorageBackendFactory sbf = storageBackendHandler.get(fileItem.getContent().getPayloadBackend());
					
					StorageBackend sb = sbf.create(fileItem.getContent().getPayloadBackend());
					
					InputStream streamToWrite = sb.download(fileItem.getContent());
					if (streamToWrite == null){
						logger.warn("discarding item {} ",item.getName());
						continue;
					}
					try(BufferedInputStream is = new BufferedInputStream(streamToWrite)){
						String name = (Paths.remove(actualPath, originalPath).toPath()+item.getName()).replaceFirst("/", "");
						logger.trace("writing file {}",name);
						zos.putNextEntry(new ZipEntry(name));
						Utils.copyStream(is, zos);
					}catch (Exception e) {
						logger.warn("error writing item {}", item.getName(),e);
					} finally{
						zos.closeEntry();
					}
					zos.flush();
				}catch (Throwable e) {
					logger.warn("error reading content for item {}", item.getPath(),e);
				}
			}
		}
		zos.close();
	}
}
