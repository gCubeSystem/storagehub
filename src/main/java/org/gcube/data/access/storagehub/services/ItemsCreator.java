package org.gcube.data.access.storagehub.services;

import static org.gcube.data.access.storagehub.Roles.INFRASTRUCTURE_MANAGER_ROLE;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.compress.archivers.ArchiveException;
import org.gcube.common.authorization.control.annotations.AuthorizationControl;
import org.gcube.common.gxrest.response.outbound.GXOutboundErrorResponse;
import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.items.GCubeItem;
import org.gcube.common.storagehub.model.plugins.PluginParameters;
import org.gcube.data.access.storagehub.Constants;
import org.gcube.data.access.storagehub.StorageHubApplicationManager;
import org.gcube.data.access.storagehub.handlers.items.ItemHandler;
import org.gcube.data.access.storagehub.handlers.items.builders.ArchiveStructureCreationParameter;
import org.gcube.data.access.storagehub.handlers.items.builders.FileCreationParameters;
import org.gcube.data.access.storagehub.handlers.items.builders.FolderCreationParameters;
import org.gcube.data.access.storagehub.handlers.items.builders.GCubeItemCreationParameters;
import org.gcube.data.access.storagehub.handlers.items.builders.ItemsParameterBuilder;
import org.gcube.data.access.storagehub.handlers.items.builders.URLCreationParameters;
import org.gcube.data.access.storagehub.repository.StoragehubRepository;
import org.gcube.smartgears.annotations.ManagedBy;
import org.gcube.smartgears.utils.InnerMethodName;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.webcohesion.enunciate.metadata.DocumentationExample;
import com.webcohesion.enunciate.metadata.Ignore;
import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResourceMethodSignature;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

import jakarta.inject.Inject;
import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * Manage item creation.
 */
@Path("items")
@ManagedBy(StorageHubApplicationManager.class)
@RequestHeaders({
		@RequestHeader(name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>")
})
public class ItemsCreator extends Impersonable {

	private static final Logger log = LoggerFactory.getLogger(ItemsCreator.class);

	@Context
	ServletContext context;

	private final StoragehubRepository repository = StoragehubRepository.repository;

	@Inject
	ItemHandler itemHandler;

	/**
	 * Create a folder.
	 *
	 * @param id          destination parent folder id
	 * @param name        destination folder name
	 * @param description description meta-info for the created folder
	 * @param hidden      hidden folder if true<br>
	 *  <strong>Possible values:</strong> <code>true</code>, <code>false</code> <br>
	 * 	<strong>Optional</strong> default: <code>false</code>
	 * @return id of the created folder
	 * @responseExample text/plain 5f4b3b4e-4b3b- ... -4b3b4e4b3b4e
	 */
	@ResourceMethodSignature(output = String.class, pathParams = { @PathParam("id") }, formParams = {
		@FormParam("name"), @FormParam("description"), @FormParam("hidden") })
	@DocumentationExample(" ...\n\nname=sampleFolder&description=This+is+a+sample+folder&hidden=false")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "Folder created."),
		@ResponseCode ( code = 400, condition = "Wrong set of parameters."),
		@ResponseCode ( code = 415, condition = "Wrong content type."),
	})
	@Path("/{id}/create/FOLDER")
	public Response createFolder(@PathParam("id") String id, @FormParam("name") String name,
			@FormParam("description") String description, @FormParam("hidden") boolean hidden) {
		InnerMethodName.set("createItem(FOLDER)");
		log.info("create folder item called");
		Session ses = null;
		String toReturn = null;
		try {
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			ItemsParameterBuilder<FolderCreationParameters> builder = FolderCreationParameters.builder().name(name)
					.description(description).hidden(hidden).on(id).with(ses).author(currentUser);
			toReturn = itemHandler.create(builder.build());
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} catch (RepositoryException re) {
			log.error("jcr error creating item", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error creating item", re));
		} catch (Throwable e) {
			log.error("unexpected error", e);
			GXOutboundErrorResponse.throwException(new BackendGenericError(e));
		} finally {
			if (ses != null)
				ses.logout();

		}
		return Response.ok(toReturn).build();
	}

	@Ignore
	@POST
	@AuthorizationControl(allowedRoles = { INFRASTRUCTURE_MANAGER_ROLE })
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/{id}/create/EXTERNALFOLDER")
	public Response createExternalFolder(@PathParam("id") String id, @FormParam("name") String name,
			@FormParam("description") String description, @FormParam("hidden") boolean hidden,
			@FormParam("pluginName") String pluginName,
			@Context HttpServletRequest request) {
		InnerMethodName.set("createItem(EXTERNALFOLDER)");
		log.info("create folder item called");
		Session ses = null;
		String toReturn = null;
		try {
			Iterator<String> paramIt = request.getParameterNames().asIterator();
			Iterable<String> iterable = () -> paramIt;
			Stream<String> targetStream = StreamSupport.stream(iterable.spliterator(), false);

			PluginParameters pluginParams = new PluginParameters();

			targetStream.filter(v -> v.startsWith("plugin."))
					.forEach(v -> pluginParams.add(v.replace("plugin.", ""), request.getParameter(v)));

			log.debug("parameters for external folder with plugin {} are {}", pluginName, pluginParams.toString());
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			ItemsParameterBuilder<FolderCreationParameters> builder = FolderCreationParameters.builder().name(name)
					.description(description).onRepository(pluginName).withParameters(pluginParams.getParameters())
					.hidden(hidden).on(id).with(ses).author(currentUser);
			toReturn = itemHandler.create(builder.build());
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} catch (RepositoryException re) {
			log.error("jcr error creating item", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error creating item", re));
		} catch (Throwable e) {
			log.error("unexpected error", e);
			GXOutboundErrorResponse.throwException(new BackendGenericError(e));
		} finally {
			if (ses != null)
				ses.logout();

		}
		return Response.ok(toReturn).build();
	}

	/**
	 * Create a URL.
	 *
	 * @param id          destination parent folder id
	 * @param name        destination URL name
	 * @param description description meta-info for the created URL
	 * @param value       URL address
	 * @return id of the created URL
	 * @responseExample text/plain 5f4b3b4e-4b3b- ... -4b3b4e4b3b4e
	 */
	@DocumentationExample(" ...\n\nname=d4science&description=D4Science+URL&value=www.d4science.org")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "URL created."),
	  	@ResponseCode ( code = 400, condition = "Wrong set of parameters."),
		@ResponseCode ( code = 406, condition = "Unable to create URL."),
		@ResponseCode ( code = 415, condition = "Wrong content type."),
	})
	@Path("/{id}/create/URL")
	public Response createURL(@PathParam("id") String id, @FormParam("name") String name,
			@FormParam("description") String description, @FormParam("value") URL value) {
		InnerMethodName.set("createItem(URL)");
		log.info("create url called");
		Session ses = null;
		String toReturn = null;
		try {
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);

			ItemsParameterBuilder<URLCreationParameters> builder = URLCreationParameters.builder().name(name)
					.description(description).url(value).on(id).with(ses).author(currentUser);

			toReturn = itemHandler.create(builder.build());
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} catch (RepositoryException re) {
			log.error("jcr error creating item", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error creating item", re));
		} catch (Throwable e) {
			log.error("unexpected error", e);
			GXOutboundErrorResponse.throwException(new BackendGenericError(e));
		} finally {
			if (ses != null)
				ses.logout();

		}
		return Response.ok(toReturn).build();
	}

	@Ignore
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{id}/create/GCUBEITEM")
	public String createGcubeItem(@PathParam("id") String id, GCubeItem item) {
		InnerMethodName.set("createItem(GCUBEITEM)");
		log.info("create Gcube item called");
		Session ses = null;
		String toReturn = null;

		try {
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			ItemsParameterBuilder<GCubeItemCreationParameters> builder = GCubeItemCreationParameters.builder()
					.item(item).on(id).with(ses).author(currentUser);

			toReturn = itemHandler.create(builder.build());
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} catch (RepositoryException re) {
			log.error("jcr error creating item", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error creating item", re));
		} catch (Throwable e) {
			log.error("unexpected error", e);
			GXOutboundErrorResponse.throwException(new BackendGenericError(e));
		} finally {
			if (ses != null)
				ses.logout();
		}
		return toReturn;
	}

	/**
	 * Upload a file retrieved from the provided url.
	 *
	 * @param id          destination folder id
	 * @param name        destination file name
	 * @param description description meta-info for the created file
	 * @param url         address of the file to be uploaded
	 * @return id of the created file
	 * @responseExample text/plain 5f4b3b4e-4b3b- ... -4b3b4e4b3b4e
	 */
	@DocumentationExample(" ...\n\nname=d4science_logo.png&description=D4Science+logo&url=\"https://www.d4science.org/image/layout_set_logo?img_id=12630&t=1720538711657\"")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "File created."),
	  	@ResponseCode ( code = 400, condition = "Wrong set of parameters."),
		@ResponseCode ( code = 406, condition = "Unable to create file."),
		@ResponseCode ( code = 415, condition = "Wrong content type."),
	})
	@Path("/{id}/create/FILE")
	public String createFileItemFromUrl(@PathParam("id") String id, @FormParam("name") String name,
			@FormParam("description") String description,
			@FormParam("url") String url) {
		InnerMethodName.set("createItem(FILEFromUrl)");

		Session ses = null;
		String toReturn = null;
		try {

			log.debug("UPLOAD: call started");
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);

			URLConnection connectionURL = new URI(url).toURL().openConnection();

			long fileLength = connectionURL.getContentLengthLong();

			try (InputStream stream = connectionURL.getInputStream()) {
				ItemsParameterBuilder<FileCreationParameters> builder = FileCreationParameters.builder().name(name)
						.fileDetails(FormDataContentDisposition.name(name).size(fileLength).build())
						.description(description).stream(stream)
						.on(id).with(ses).author(currentUser);
				toReturn = itemHandler.create(builder.build());
			}
			log.debug("UPLOAD: call finished");
		} catch (RepositoryException re) {
			log.error("jcr error creating file item", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error creating file item", re));
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} catch (Throwable e) {
			log.error("unexpected error", e);
			GXOutboundErrorResponse.throwException(new BackendGenericError(e));
		} finally {
			if (ses != null && ses.isLive()) {
				log.info("session closed");
				ses.logout();
			}
		}
		return toReturn;

	}

	/**
	 * Upload the provided file.
	 *
	 * @param id          destination folder id
	 * @param name        destination file name
	 * @param description description meta-info for the created file
	 * @param file        multipart/form-data file parameter, with optional
	 *                    'filename' and 'size' (see example below)
	 * @return id of the created file
	 * @responseExample text/plain 5f4b3b4e-4b3b- ... -4b3b4e4b3b4e
	 */
	@ResourceMethodSignature(output = String.class, pathParams = { @PathParam("id") }, formParams = {
			@FormParam("name"), @FormParam("description"), @FormParam("file") })
	@DocumentationExample(value = "...\n\n--------boundaryString\n" +
		"Content-Disposition: form-data; name=\"file\"; filename=\"doc.pdf\"; size=426018;\n" +
		"Content-Type: application/pdf\n\n" +
		"(data)\n" +
		"--------boundaryString\n" +
		"Content-Disposition: form-data; name=\"name\"\n\n" +
		"doc.pdf\n" +
		"--------boundaryString\n" +
		"Content-Disposition: form-data; name=\"description\"\n\n" +
		"\"This is just a sample PDF file\"\n" +
		"--------boundaryString--")
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "File created."),
	  	@ResponseCode ( code = 400, condition = "Wrong set of parameters."),
		@ResponseCode ( code = 406, condition = "Unable to create file."),
		@ResponseCode ( code = 415, condition = "Wrong content type."),
	})
	@Path("/{id}/create/FILE")
	public String createFileItem(@PathParam("id") String id, @FormDataParam("name") String name,
			@FormDataParam("description") String description,
			@FormDataParam("file") InputStream file,
			@FormDataParam("file") FormDataContentDisposition fileDetail) {
		InnerMethodName.set("createItem(FILE)");

		Session ses = null;
		String toReturn = null;
		try (InputStream is = new BufferedInputStream(file)) {

			long size = fileDetail.getSize();

			log.info("UPLOAD: call started with file size {}", size);
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			ItemsParameterBuilder<FileCreationParameters> builder = FileCreationParameters.builder().name(name)
					.description(description).stream(file).fileDetails(fileDetail)
					.on(id).with(ses).author(currentUser);
			log.debug("UPLOAD: item prepared");
			toReturn = itemHandler.create(builder.build());
			log.debug("UPLOAD: call finished");
		} catch (RepositoryException re) {
			log.error("jcr error creating file item", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error creating file item", re));
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} catch (Throwable e) {
			log.error("unexpected error", e);
			GXOutboundErrorResponse.throwException(new BackendGenericError(e));
		} finally {
			if (ses != null && ses.isLive()) {
				log.info("session closed");
				ses.logout();
			}
		}
		return toReturn;

	}

	/**
	 * Upload an archive from the provided url and extract its content on-the-fly in a new folder.
	 *
	 * @param id               destination folder id
	 * @param parentFolderName name of the newly-created folder containing extracted
	 *                         files
	 * @param url              address of the archive to be uploaded
	 * @return id of the created folder containing extracted files
	 * @responseExample text/plain 5f4b3b4e-4b3b- ... -4b3b4e4b3b4e
	 */
	@DocumentationExample(" ...\n\nname=sampleZip.zip&description=This+is+a+sample+zip&url=\"https://getsamplefiles.com/download/zip/sample-1.zip\"")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "Archive extracted."),
	  	@ResponseCode ( code = 400, condition = "Wrong set of parameters."),
		@ResponseCode ( code = 406, condition = "Unable to extract archive."),
		@ResponseCode ( code = 415, condition = "Wrong content type."),
	})
	@Path("/{id}/create/ARCHIVE")
	public String uploadArchiveFromURL(@PathParam("id") String id,
			@FormParam("parentFolderName") String parentFolderName,
			@FormParam("url") String url) {
		InnerMethodName.set("createItem(ARCHIVEFromURL)");

		Session ses = null;
		String toReturn = null;
		try {

			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			try (InputStream stream = new URI(url).toURL().openStream()) {
				ItemsParameterBuilder<ArchiveStructureCreationParameter> builder = ArchiveStructureCreationParameter
						.builder().parentName(parentFolderName).stream(stream)
						.on(id).with(ses).author(currentUser);
				toReturn = itemHandler.create(builder.build());
			}

		} catch (RepositoryException | ArchiveException | IOException re) {
			log.error("jcr error extracting archive", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error extracting archive", re));
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} catch (Throwable e) {
			log.error("unexpected error", e);
			GXOutboundErrorResponse.throwException(new BackendGenericError(e));
		} finally {
			if (ses != null)
				ses.logout();

		}
		return toReturn;
	}

	/**
	 * Upload the provided archive and extract its content on-the-fly in a new folder.
	 *
	 * @param id               destination folder id
	 * @param parentFolderName name of the newly-created folder containing extracted
	 *                         files
	 * @param file             multipart/form-data file parameter, with optional
	 *                         'filename' and 'size' (see example below)
	 * @return id of the created folder containing extracted files
	 * @responseExample text/plain 5f4b3b4e-4b3b- ... -4b3b4e4b3b4e
	 */
	@ResourceMethodSignature(output = String.class, pathParams = { @PathParam("id") }, formParams = {
		@FormParam("parentFolderName"), @FormParam("file") })
	@DocumentationExample(value = "...\n\n--------boundaryString\n" +
		"Content-Disposition: form-data; name=\"file\"; filename=\"archive.zip\"; size=1560238;\n" +
		"Content-Type: application/zip\n\n" +
		"(compressed data)\n" +
		"--------boundaryString\n" +
		"Content-Disposition: form-data; name=\"parentFolderName=\"\n\n" +
		"my documents\n" +
		"--------boundaryString--")
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "Archive extracted."),
	  	@ResponseCode ( code = 400, condition = "Wrong set of parameters."),
		@ResponseCode ( code = 406, condition = "Unable to extract archive."),
		@ResponseCode ( code = 415, condition = "Wrong content type."),
	})
	@Path("/{id}/create/ARCHIVE")
	public String uploadArchive(@PathParam("id") String id, @FormDataParam("parentFolderName") String parentFolderName,
			@FormDataParam("file") InputStream file,
			@FormDataParam("file") FormDataContentDisposition fileDetail) {
		InnerMethodName.set("createItem(ARCHIVE)");

		Session ses = null;
		String toReturn = null;
		try (InputStream is = new BufferedInputStream(file)) {
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);

			ItemsParameterBuilder<ArchiveStructureCreationParameter> builder = ArchiveStructureCreationParameter
					.builder().parentName(parentFolderName).stream(is).fileDetails(fileDetail)
					.on(id).with(ses).author(currentUser);

			toReturn = itemHandler.create(builder.build());

		} catch (RepositoryException | ArchiveException | IOException re) {
			log.error("jcr error extracting archive", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error extracting archive", re));
		} catch (StorageHubException she) {
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		} catch (Throwable e) {
			log.error("unexpected error", e);
			GXOutboundErrorResponse.throwException(new BackendGenericError(e));
		} finally {
			if (ses != null)
				ses.logout();

		}
		return toReturn;
	}

}
