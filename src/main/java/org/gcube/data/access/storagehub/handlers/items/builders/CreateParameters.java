package org.gcube.data.access.storagehub.handlers.items.builders;

import javax.jcr.Session;

public abstract class CreateParameters {

	public enum ManagedType {
		FILE, 
		FOLDER, 
		ARCHIVE, 
		URL, 
		GCUBEITEM;
	}
	
	String parentId;
	Session session;
	String user;
	
	protected CreateParameters() {}

	public abstract ManagedType getMangedType();
	
	public String getParentId() {
		return parentId;
	}

	public Session getSession() {
		return session;
	}
	
	public String getUser() {
		return user;
	}

	protected abstract boolean isValid();
	
	public abstract String toString();
}
