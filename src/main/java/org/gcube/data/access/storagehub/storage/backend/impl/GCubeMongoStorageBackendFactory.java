package org.gcube.data.access.storagehub.storage.backend.impl;

import org.gcube.common.storagehub.model.exceptions.InvalidCallParameters;
import org.gcube.common.storagehub.model.items.nodes.PayloadBackend;
import org.gcube.common.storagehub.model.storages.StorageBackend;
import org.gcube.common.storagehub.model.storages.StorageBackendFactory;
import org.gcube.common.storagehub.model.storages.StorageNames;

import jakarta.inject.Singleton;

@Singleton
public class GCubeMongoStorageBackendFactory implements StorageBackendFactory {

	@Override
	public String getName() {
		return StorageNames.MONGO_STORAGE;
	}

	@Override
	public boolean isSystemStorage() {
		return true;
	}
	
	@Override
	public StorageBackend create(PayloadBackend payloadConfiguration) throws InvalidCallParameters {
		return new GCubeMongoStorageBackend(payloadConfiguration);
	}
	
}
