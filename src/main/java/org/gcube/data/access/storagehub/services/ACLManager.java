package org.gcube.data.access.storagehub.services;

import java.util.Collections;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.gcube.common.gxrest.response.outbound.GXOutboundErrorResponse;
import org.gcube.common.storagehub.model.Excludes;
import org.gcube.common.storagehub.model.acls.AccessType;
import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.InvalidCallParameters;
import org.gcube.common.storagehub.model.exceptions.InvalidItemException;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.exceptions.UserNotAuthorizedException;
import org.gcube.common.storagehub.model.items.FolderItem;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.common.storagehub.model.items.SharedFolder;
import org.gcube.common.storagehub.model.items.VreFolder;
import org.gcube.common.storagehub.model.types.ACLList;
import org.gcube.data.access.storagehub.AuthorizationChecker;
import org.gcube.data.access.storagehub.Constants;
import org.gcube.data.access.storagehub.PathUtil;
import org.gcube.data.access.storagehub.StorageHubApplicationManager;
import org.gcube.data.access.storagehub.handlers.ACLHandler;
import org.gcube.data.access.storagehub.handlers.UnshareHandler;
import org.gcube.data.access.storagehub.handlers.items.Node2ItemConverter;
import org.gcube.data.access.storagehub.repository.StoragehubRepository;
import org.gcube.data.access.storagehub.services.interfaces.ACLManagerInterface;
import org.gcube.smartgears.annotations.ManagedBy;
import org.gcube.smartgears.utils.InnerMethodName;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.webcohesion.enunciate.metadata.DocumentationExample;
import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResourceMethodSignature;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * Manage the Access Control List of shared folders
 */
@Path("items")
@ManagedBy(StorageHubApplicationManager.class)
@RequestHeaders({
	  @RequestHeader( name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>"),
	})
public class ACLManager extends Impersonable {

	private static final Logger log = LoggerFactory.getLogger(ACLManager.class);

	StoragehubRepository repository = StoragehubRepository.repository;
	
	@Inject
	ACLHandler aclHandler;
	
	

	@RequestScoped
	@PathParam("id") 
	String id;

	@Inject
	AuthorizationChecker authChecker;

	@Inject
	PathUtil pathUtil; 

	@Context 
	ServletContext context;

	@Inject Node2ItemConverter node2Item;

	@Inject UnshareHandler unshareHandler;

	@Inject ACLManagerInterface aclManagerDelegate;

	/**
	 * returns the AccessType for all the users in a shared folder
	 *
	 * @param id id of the shared folder
	 */
	@ResourceMethodSignature(output = ACLList.class, pathParams = { @PathParam("id") })
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "Shared folder found."),
		@ResponseCode ( code = 500, condition = "This item does not exist."),
	})
	@GET
	@Path("{id}/acls")
	@Produces(MediaType.APPLICATION_JSON)
	public ACLList getACL() {
		InnerMethodName.set("getACLById");
		Session ses = null;
		try{
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			Item item  = node2Item.getItem(ses.getNodeByIdentifier(id), Excludes.ALL);
			authChecker.checkReadAuthorizationControl(ses, currentUser, id);
			return new ACLList(aclManagerDelegate.getByItem(item, ses));
		}catch(RepositoryException re){
			log.error("jcr error getting acl", re);
			throw new WebApplicationException(new BackendGenericError("jcr error getting acl", re));
		}catch(StorageHubException she ){
			log.error(she.getErrorMessage(), she);
			throw new WebApplicationException(she, Response.Status.fromStatusCode(she.getStatus()));
		}finally{
			if (ses!=null)
				ses.logout();
		}

	}


	/**
	 * Set a new AccessType for a user in a shared folder or VRE folder
	 *
	 * @param id id of the shared folder
	 * @param user user id
	 * @param access access type<br>
	 *   <strong>Possible values:</strong> <code>READ_ONLY</code>, <code>WRITE_OWNER</code>, <code>WRITE_ALL</code>, <code>ADMINISTRATOR</code>
	 */
	@ResourceMethodSignature(output = void.class, pathParams = { @PathParam("id") }, formParams = {
			@FormParam("user"), @FormParam("access") })
	@StatusCodes({
		@ResponseCode ( code = 204, condition = "Access type updated."),
		@ResponseCode ( code = 400, condition = "User does not exist."),
		@ResponseCode ( code = 415, condition = "Wrong content type."),
		@ResponseCode ( code = 500, condition = "This shared item does not exist or wrong access type."),
	})
	@DocumentationExample(value = "...\n\n--------boundaryString\n" +
		"Content-Disposition: form-data; name=\"user\"\n\n" +
		"user2\n" +
		"--------boundaryString\n" +
		"Content-Disposition: form-data; name=\"accessType\"\n\n" +
		"WRITE_OWNER\n" +
		"--------boundaryString--")
	@PUT
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Path("{id}/acls")
	public void updateACL(@FormDataParam("user") String user, @FormDataParam("access") AccessType accessType) {
		InnerMethodName.set("setACLById");
		Session ses = null;
		try {

			if (user==currentUser) throw new InvalidCallParameters("own ACLs cannot be modified");

			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			Node node = ses.getNodeByIdentifier(id);

			Item item = node2Item.getItem(node, Excludes.ALL);

			if (!(item instanceof SharedFolder))
				throw new InvalidItemException("the item is not a shared folder");

			if (item.getOwner().equals(user))
				throw new UserNotAuthorizedException("owner acl cannot be changed");

			SharedFolder folder = (SharedFolder) item;

			authChecker.checkAdministratorControl(ses, currentUser, folder);

			if (item instanceof VreFolder || folder.isVreFolder())
				throw new InvalidCallParameters("acls in vreFolder cannot be updated with this method");

			NodeIterator sharedSet = node.getSharedSet();
			boolean found = false;
			while (sharedSet.hasNext() && !found) {
				Node current = sharedSet.nextNode();
				if (current.getPath().startsWith(pathUtil.getWorkspacePath(user).toPath()))
					found = true;
			}
			if (!found) 
				throw new InvalidCallParameters("shared folder with id "+folder.getId()+" is not shared with user "+user);


			aclManagerDelegate.update(user, node, accessType, ses);

		}catch(RepositoryException re){
			log.error("jcr error extracting archive", re);
			throw new WebApplicationException(new BackendGenericError("jcr error setting acl", re));
		}catch(StorageHubException she ){
			log.error(she.getErrorMessage(), she);
			throw new WebApplicationException(she, Response.Status.fromStatusCode(she.getStatus()));
		}finally{
			if (ses!=null)
				ses.logout();
		}

	}

	/**
	 * Remove a user from the shared folder
	 *
	 * @param id id of the shared folder
	 * @param user user id
	 */
	@ResourceMethodSignature(output = void.class, pathParams = { @PathParam("id"), @PathParam("user") })
	@StatusCodes({
		@ResponseCode ( code = 204, condition = "User removed."),
		@ResponseCode ( code = 415, condition = "Wrong content type."),
		@ResponseCode ( code = 500, condition = "This shared item does not exist."),
	})
	@DELETE
	@Consumes(MediaType.TEXT_PLAIN)
	@Path("{id}/acls/{user}")
	public void removeACL(@PathParam("user") String user) {
		InnerMethodName.set("removeACLById");
		Session ses = null;
		try{
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);

			Node node = ses.getNodeByIdentifier(id);

			Item item = node2Item.getItem(node, Excludes.ALL);

			if (!(item instanceof SharedFolder))
				throw new InvalidItemException("the item is not a shared folder");

			if (item instanceof VreFolder || ((SharedFolder) item).isVreFolder())
				throw new InvalidCallParameters("acls in vreFolder cannot be removed with this method");

			authChecker.checkAdministratorControl(ses, currentUser, (SharedFolder) item);

			unshareHandler.unshare(ses, Collections.singleton(user), node, currentUser);	

		}catch(RepositoryException re){
			log.error("jcr error extracting archive", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error removing acl", re));
		}catch(StorageHubException she ){
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		}finally{
			if (ses!=null)
				ses.logout();
		}
	}

	/**
	 * Check if the current user can write on the shared folder 
	 *
	 * @param id id of the shared folder
	 * @return true if the current user can write on the shared folder, false otherwise
	 * @responseExample text/plain true
	 */
	@ResourceMethodSignature(output = Boolean.class, pathParams = { @PathParam("id") })
	@StatusCodes({
		@ResponseCode ( code = 200, condition = "Shared folder found."),
		@ResponseCode ( code = 406, condition = "This shared folder does not exist."),
	})
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("{id}/acls/write")
	public Boolean canWriteInto() {
		InnerMethodName.set("canWriteIntoFolder");
		Session ses = null;
		Boolean canWrite = false;
		try{
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			Node node = ses.getNodeByIdentifier(id);
			Item item = node2Item.getItem(node, Excludes.ALL);
			if (!(item instanceof FolderItem))
				throw new InvalidItemException("this method can be applied only to folder");

			try {
				authChecker.checkWriteAuthorizationControl(ses, currentUser, id,  true);
			}catch (UserNotAuthorizedException e) {
				return false;
			}
			return true;
		}catch(RepositoryException re){
			log.error("jcr error getting acl", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error getting acl", re));
		}catch(StorageHubException she ){
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		}finally{
			if (ses!=null)
				ses.logout();
		}
		return canWrite;	
	}

}
