package org.gcube.data.access.storagehub;

import jakarta.inject.Singleton;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.gcube.common.storagehub.model.Path;
import org.gcube.common.storagehub.model.Paths;

@Singleton
public class PathUtil {
	
	public  Path getWorkspacePath(String login){
		return Paths.getPath(String.format("/Home/%s/%s",login,Constants.WORKSPACE_ROOT_FOLDER_NAME));
	}

	public Path getHome(String login){
		return Paths.getPath(String.format("/Home/%s",login));
	}

	public Path getInboxPath(String login) {
		return Paths.append(getHome(login),Constants.INBOX_FOLDER_NAME);
	}
	
	public Path getOutboxPath(String login) {
		return Paths.append(getHome(login),Constants.OUTBOX_FOLDER_NAME);
	}

	@Deprecated
	private  Path getOldTrashPath(String login){
		return Paths.append(getWorkspacePath(login),Constants.TRASH_ROOT_FOLDER_NAME);
	}
	
	private  Path getNewTrashPath(String login){
		return Paths.append(getHome(login), Constants.TRASH_ROOT_FOLDER_NAME);
	}
	
	@Deprecated
	private  Path getOldVREsPath(String login){
		return Paths.append(getWorkspacePath(login),Constants.OLD_VRE_FOLDER_PARENT_NAME);
	}
	
	private  Path getNewVREsPath(String login){
		return Paths.append(getHome(login),Constants.PERSONAL_VRES_FOLDER_PARENT_NAME);
	}
	
	public  Path getSharedWithMePath(String login){
		return Paths.append(getHome(login),Constants.SHARED_WITH_ME_PARENT_NAME);
	}

	/*
	public  Path getMySharedPath(String login){
		return Paths.append(getHome(login),Constants.MYSHARED_PARENT_NAME);
	}*/
	
	public Path getVREsPath(String login, Session session) throws RepositoryException {
		Path home = getHome(login);
		Node node = session.getNode(home.toPath());
		if (node.hasProperty(Constants.HOME_VERSION_PROP) && node.getProperty(Constants.HOME_VERSION_PROP).getLong()>0) 
			return getNewVREsPath(login);
		else 
			return getOldVREsPath(login);
	}
	
	public Path getTrashPath(String login, Session session) throws RepositoryException {
		Path home = getHome(login);
		Node node = session.getNode(home.toPath());
		if (node.hasProperty(Constants.HOME_VERSION_PROP) && node.getProperty(Constants.HOME_VERSION_PROP).getLong()>0) 
			return getNewTrashPath(login);
		else 
			return getOldTrashPath(login);
	}
	
}
