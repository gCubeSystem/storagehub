package org.gcube.data.access.storagehub.storage.backend.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.gcube.common.storagehub.model.Metadata;
import org.gcube.common.storagehub.model.exceptions.InvalidCallParameters;
import org.gcube.common.storagehub.model.items.nodes.PayloadBackend;
import org.gcube.common.storagehub.model.storages.StorageBackend;
import org.gcube.common.storagehub.model.storages.StorageBackendFactory;
import org.gcube.common.storagehub.model.storages.StorageNames;
import org.gcube.smartgears.ContextProvider;
import org.gcube.smartgears.context.application.ApplicationContext;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Singleton;

@Singleton
public class GcubeS3StorageBackendFactory implements StorageBackendFactory {

	private static final String PROP_PREFIX = "default."; 
	
	private Metadata baseParameters;
	
	@PostConstruct
	public void init(){
		baseParameters = getParameters();
	}
	
	@Override
	public String getName() {
		return StorageNames.GCUBE_STORAGE;
	}

	@Override
	public boolean isSystemStorage() {
		return true;
	}

	@Override
	public StorageBackend create(PayloadBackend payloadConfiguration) throws InvalidCallParameters {
		if (payloadConfiguration.getParameters().isEmpty()) 
			throw new InvalidCallParameters(getName()+": null or empty parameter not allowed");
		String bucketName = (String)payloadConfiguration.getParameters().get("bucketName");
		if (bucketName == null || bucketName.isBlank())
			throw new InvalidCallParameters(getName()+": 'bucketName' cannot be blank or empty");
		
		Metadata metadata = new Metadata(new HashMap<>(baseParameters.getMap()));
		metadata.getMap().putAll(payloadConfiguration.getParameters());
		
		S3Backend backend = new S3Backend(new PayloadBackend(getName(), metadata), (String) -> UUID.randomUUID().toString());
		//Setting this only to show only the bucket name and not all the information on the item since it uses the default configuration
		backend.setPayloadConfiguration(new PayloadBackend(getName(),  new Metadata(payloadConfiguration.getParameters())));
		return backend;
	}

	private Metadata getParameters(){
		ApplicationContext context = ContextProvider.get();
		String folderPath = context.appSpecificConfigurationFolder().toString();
		try (InputStream input = new FileInputStream(Paths.get(folderPath, "storage-settings.properties").toFile())) {
            Properties prop = new Properties();
           
            prop.load(input);
    		
            Map<String, Object> params = new HashMap<String, Object>();
                
            
            prop.forEach((k,v) -> { if (k.toString().startsWith(PROP_PREFIX)) params.put(k.toString().replace(PROP_PREFIX, ""), v);});
    		
            return new Metadata(params);
    
        } catch (IOException ex) {
            throw new RuntimeException("error initializing MinIO", ex);
        }
		
		
	}
}
