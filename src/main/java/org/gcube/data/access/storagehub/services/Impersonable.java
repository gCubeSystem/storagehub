package org.gcube.data.access.storagehub.services;


import static org.gcube.data.access.storagehub.Roles.INFRASTRUCTURE_MANAGER_ROLE;

import org.gcube.common.security.Owner;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Context;

@Path("")
public abstract class Impersonable {

	Logger log = LoggerFactory.getLogger(Impersonable.class);	
		
	String currentUser = null;
	
	@Inject
	public void setCurrentUser(@Context final HttpServletRequest request) {
		String impersonate = request!=null ? request.getParameter("impersonate") : null ; 
		Owner owner = SecretManagerProvider.get().getOwner();
		if(impersonate!=null && owner.getRoles().contains(INFRASTRUCTURE_MANAGER_ROLE)) {
			this.currentUser = impersonate;
		} else 
			this.currentUser = owner.getId();
		
		log.info("called with login {} and impersonate {}",owner.getId(), impersonate);
	}
	
}
