package org.gcube.data.access.storagehub.repository;

import javax.jcr.Repository;

public interface StoragehubRepository {

	static final StoragehubRepository repository = new JackrabbitRepositoryImpl();
	
	Repository getRepository();
	
	void shutdown();
	
	void initContainerAtFirstStart();
}
