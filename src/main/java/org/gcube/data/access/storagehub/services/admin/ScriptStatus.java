package org.gcube.data.access.storagehub.services.admin;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScriptStatus {

	enum Status {
		Running, Success, Failed
	}
	
	private static final DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss:SSS Z") ;
	
	private Status status;
	
	private String errorMessage;
	
	private String result;
	
	private long start;

	private long finished = -1;
	
	private String runningId;
	
	private String executionServer;
		
	public ScriptStatus(String runningId, String resultPath, String executionServer) {
		super();
		this.status = Status.Running;
		this.start = System.currentTimeMillis();
		this.runningId = runningId;
		this.result = resultPath;	
		this.executionServer = executionServer;
	}
	
	public ScriptStatus(String runningId, String executionServer) {
		super();
		this.status = Status.Running;
		this.start = System.currentTimeMillis();
		this.runningId = runningId;
		this.executionServer = executionServer;
	}
	
	public void setFailed(String error) {
		this.status = Status.Failed;
		this.errorMessage = error;
		this.finished = System.currentTimeMillis();
	}
	
	public void setSuccess() {
		this.status = Status.Success;
		this.finished = System.currentTimeMillis();
	}
	
	public void setSuccess(String result) {
		this.status = Status.Success;
		this.finished = System.currentTimeMillis();
		this.result = result;
	}
	
	public Status getStatus() {
		return status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getStartDate() {
		 Date date = new Date(this.start);
		 return dateFormat.format(date);
	}
	
	public long getDurationInMillis() {
		long toUse = finished;
		if (finished < 0) 
			toUse = System.currentTimeMillis();
		return toUse-start;
	}
	
	public String getHumanReadableDuration() {
		long toUse = finished;
		if (finished < 0) 
			toUse = System.currentTimeMillis();
		
		long duration = toUse - this.start;
		
		long minutes = (duration/1000)/60;
		long seconds = (duration/1000)%60;
				
		return String.format("%d minutes %d seconds", minutes, seconds);
	}

	public String getResult() {
		return result;
	}

	public String getRunningId() {
		return runningId;
	}

	public String getExecutionServer() {
		return executionServer;
	}
	
}
