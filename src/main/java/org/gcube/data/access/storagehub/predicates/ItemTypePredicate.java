package org.gcube.data.access.storagehub.predicates;

import java.util.function.Predicate;

import org.gcube.common.storagehub.model.items.RootItem;

public interface ItemTypePredicate extends Predicate<Class<? extends RootItem>> {

}
