package org.gcube.data.access.storagehub.handlers.items.builders;

import java.util.Objects;

import org.gcube.common.storagehub.model.items.GCubeItem;

public class GCubeItemCreationParameters extends CreateParameters{

	GCubeItem item;
	
	
	protected GCubeItemCreationParameters() {}

	public GCubeItem getItem() {
		return item;
	}

	@Override
	protected boolean isValid() {
		return Objects.nonNull(item);
	}

	@Override
	public String toString() {
		return "GCubeItemCreationParameters [item=" + item + ", parentId=" + parentId + ", user=" + user + "]";
	}

	public static GCubeItemCreationBuilder builder() {
		return new GCubeItemCreationBuilder();
	}
	
	public static class GCubeItemCreationBuilder extends ItemsParameterBuilder<GCubeItemCreationParameters>{

		private GCubeItemCreationBuilder() {
			super(new GCubeItemCreationParameters());
		}
		
		public GCubeItemCreationBuilder item(GCubeItem item) {
			parameters.item = item;
			return this;		
		}

	}
	
	@Override
	public ManagedType getMangedType() {
		return ManagedType.GCUBEITEM;
	}
}
