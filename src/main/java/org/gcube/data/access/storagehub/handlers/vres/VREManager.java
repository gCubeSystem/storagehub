package org.gcube.data.access.storagehub.handlers.vres;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.jackrabbit.api.JackrabbitSession;
import org.gcube.common.security.ContextBean;
import org.gcube.common.security.ContextBean.Type;
import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.data.access.storagehub.Constants;
import org.gcube.data.access.storagehub.PathUtil;
import org.gcube.data.access.storagehub.handlers.items.Node2ItemConverter;
import org.gcube.data.access.storagehub.repository.StoragehubRepository;
import org.gcube.data.access.storagehub.services.delegates.GroupManagerDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

@Singleton
public class VREManager {

	private static final Logger logger = LoggerFactory.getLogger(VREManager.class);
	
	private Map<String, VRE> vreMap = new HashMap<>();
	
	StoragehubRepository repository = StoragehubRepository.repository;
	
	@Inject
	Node2ItemConverter node2Item;
	
	@Inject
	PathUtil pathUtil;
	
	@Inject 
	GroupManagerDelegate groupHandler;
	
	ExecutorService executor = Executors.newFixedThreadPool(5);
	
		
	private synchronized VRE getVRE(String completeName) {
		logger.trace("requesting VRE {}",completeName);
		if (vreMap.containsKey(completeName))
			return vreMap.get(completeName);
		else 
			return null;
		
	}
	
	private synchronized VRE putVRE(Item vreFolder) {
		logger.trace("inserting VRE {}",vreFolder.getTitle());
		if (vreMap.containsKey(vreFolder.getTitle())) throw new RuntimeException("something went wrong (vre already present in the map)");
		else {
			VRE toReturn = new VRE(vreFolder, repository.getRepository(), Constants.JCR_CREDENTIALS, node2Item, executor);
			vreMap.put(vreFolder.getTitle(), toReturn);
			return toReturn;
		}
		
	}

	public String retrieveGroupNameFromContext(String context) throws StorageHubException{
		ContextBean bean = new ContextBean(context);
		if (!bean.is(Type.VRE)) throw new BackendGenericError("the current scope is not a VRE");
		String entireScopeName= bean.toString().replaceAll("^/(.*)/?$", "$1").replaceAll("/", "-");
		return entireScopeName;
	}
	
	
	public synchronized VRE getVreFolderItemByGroupName(JackrabbitSession ses, String groupName, List<String> excludes ) throws RepositoryException, StorageHubException{
		VRE vre = this.getVRE(groupName);
		if (vre!=null) return vre;
		else {
			Node vreFolderNode = groupHandler.getFolderNodeRelatedToGroup(ses, groupName);
			Item vreFolder = node2Item.getItem(vreFolderNode, excludes);
			return this.putVRE(vreFolder);
		}	
	}

	
}
