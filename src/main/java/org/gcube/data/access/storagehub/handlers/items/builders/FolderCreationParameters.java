package org.gcube.data.access.storagehub.handlers.items.builders;

import java.util.Map;
import java.util.Objects;

import org.gcube.common.storagehub.model.Metadata;
import org.gcube.common.storagehub.model.items.nodes.PayloadBackend;
import org.gcube.common.storagehub.model.plugins.PluginParameters;

public class FolderCreationParameters extends CreateParameters {
	
	private String name;
	private String description=""; 
	private boolean hidden = false;
	private PayloadBackend backend;
	
	
	protected FolderCreationParameters() {}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public boolean isHidden() {
		return hidden;
	}

	public PayloadBackend getBackend() {
		return backend;
	}
	
	@Override
	protected boolean isValid() {
		return Objects.nonNull(name) && Objects.nonNull(description);
	}

	@Override
	public String toString() {
		return "FolderCreationParameters [name=" + name + ", description=" + description + ", hidden=" + hidden
				+ ", parentId=" + parentId + ", session=" + session + ", user=" + user + "]";
	}
	
	
	public static FolderCreationBuilder builder() {
		return new FolderCreationBuilder();
	}
	
	public static class FolderCreationBuilder extends ItemsParameterBuilder<FolderCreationParameters>{

		private FolderCreationBuilder() {
			super(new FolderCreationParameters());
		}
		
		public FolderCreationBuilder name(String name) {
			parameters.name = name;
			return this;		
		}

		public FolderCreationBuilder description(String description) {
			parameters.description = description;
			return this;
		}
		
		public BackendCreationBuilder onRepository(String pluginName) {
			return new BackendCreationBuilder(pluginName, this);
		}
		
		public FolderCreationBuilder hidden(boolean hidden) {
			parameters.hidden = hidden;
			return this;
		}

	}
	
	public static class BackendCreationBuilder {
		
		FolderCreationBuilder cb;
		
		String plugin;
		
		protected BackendCreationBuilder(String pluginName, FolderCreationBuilder cb) {
			this.cb = cb;
			this.plugin = pluginName;
		}
		
		public FolderCreationBuilder withParameters(Map<String, Object> params){
			this.cb.parameters.backend = new PayloadBackend(plugin, new Metadata(params));
			return this.cb;
		}
		
		public FolderCreationBuilder withoutParameters(){
			this.cb.parameters.backend = new PayloadBackend(plugin, null);
			return this.cb;
		}
		
	}
	

	@Override
	public ManagedType getMangedType() {
		return ManagedType.FOLDER;
	}
}
