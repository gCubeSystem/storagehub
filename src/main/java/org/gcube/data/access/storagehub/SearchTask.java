package org.gcube.data.access.storagehub;

import java.util.List;
import java.util.LinkedList;
import java.util.concurrent.RecursiveTask;
import java.util.regex.Pattern;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.gcube.common.storagehub.model.Excludes;
import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.UserNotAuthorizedException;
import org.gcube.common.storagehub.model.items.FolderItem;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.common.storagehub.model.types.NodeProperty;
import org.gcube.data.access.storagehub.handlers.items.Node2ItemConverter;
import org.gcube.data.access.storagehub.predicates.ItemTypePredicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchTask extends RecursiveTask<List<Item>> {

	private static final Logger logger = LoggerFactory.getLogger(SearchTask.class);

    private final String user;
    private final AuthorizationChecker authChecker;
    private final Node parentNode;
    private final List<String> excludes;
    private final Range range;
    private final boolean showHidden;
    private final boolean excludeTrashed;
    private final ItemTypePredicate itemTypePredicate;
    private final Pattern pattern;
    private final Node2ItemConverter node2ItemConverter;
    private final Session session;

    public SearchTask(String user, AuthorizationChecker authChecker,
        Node parentNode, List<String> excludes, Range range, boolean showHidden,
        boolean excludeTrashed, ItemTypePredicate itemTypePredicate, Pattern pattern,
        Node2ItemConverter node2ItemConverter, Session session) {
        this.user = user;
        this.authChecker = authChecker;
        this.parentNode = parentNode;
        this.excludes = excludes;
        this.range = range;
        this.showHidden = showHidden;
        this.excludeTrashed = excludeTrashed;
        this.itemTypePredicate = itemTypePredicate;
        this.pattern = pattern;
        this.node2ItemConverter = node2ItemConverter;
        this.session = session;
    }

    @Override
    protected List<Item> compute() {
        LinkedList<SearchTask> subtasks = null;
		List<Item> returnList = null;
        // TODO: use range!

		NodeChildrenFilterIterator iterator = null;
        try {
		    iterator = new NodeChildrenFilterIterator(parentNode);
        } catch (BackendGenericError e) {
            logger.debug(e.getMessage());
            return returnList;
        }

		while(iterator.hasNext()) {
			Node node = iterator.next();

			try {
	            if (!node.hasProperty(NodeProperty.TITLE.toString()))
                    continue;
                String nodeTitle = node.getProperty(NodeProperty.TITLE.toString()).getString();

			    logger.trace("[SEARCH] evaluating node {} ", nodeTitle);

                Item minItem = node2ItemConverter.getItem(node, Excludes.ALL);
	            if (minItem instanceof FolderItem) {
			        // exclude node if not authorized
				    authChecker.checkReadAuthorizationControl(session, user, node.getIdentifier());
                    
	                if (subtasks == null) {
	                    subtasks = new LinkedList<>();
	                }
	                SearchTask subTask = new SearchTask(user, authChecker, node, excludes, 
                        range, showHidden, excludeTrashed, itemTypePredicate,
                        pattern, node2ItemConverter, session);
	                subTask.fork();
	                subtasks.add(subTask);
	            }
                
				// check if name matches search pattern
				if (!pattern.matcher(nodeTitle).find())
                    continue;
                
				// (possibly) exclude hidden nodes
				if (Utils.isToExclude(node, showHidden))
					continue;
	
				// (possibly) exclude trashed nodes
				if (minItem == null || (minItem.isTrashed() && excludeTrashed))
					continue;

                if (returnList == null) {
                    returnList = new LinkedList<>();
                }
				Item item = node2ItemConverter.getFilteredItem(node, excludes, itemTypePredicate);
				returnList.add(item);
				logger.trace("[SEARCH] added node {}", nodeTitle);
			} catch (UserNotAuthorizedException | BackendGenericError | RepositoryException e) {
                logger.debug(e.getMessage());
				continue;
			}
		}
        
        if (subtasks != null) {
            for (SearchTask subtask : subtasks) {
                List<Item> subtaskResult = subtask.join();
                if (subtaskResult != null) {
                    if (returnList == null) {
                        returnList = new LinkedList<>();
                    }
                    returnList.addAll(subtaskResult);
                }
            }
        }

		return returnList;
    }

}