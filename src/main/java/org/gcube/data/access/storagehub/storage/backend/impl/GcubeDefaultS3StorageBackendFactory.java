package org.gcube.data.access.storagehub.storage.backend.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.gcube.common.storagehub.model.Metadata;
import org.gcube.common.storagehub.model.exceptions.InvalidCallParameters;
import org.gcube.common.storagehub.model.items.nodes.PayloadBackend;
import org.gcube.common.storagehub.model.storages.StorageBackend;
import org.gcube.common.storagehub.model.storages.StorageBackendFactory;
import org.gcube.common.storagehub.model.storages.StorageNames;
import org.gcube.smartgears.ContextProvider;
import org.gcube.smartgears.context.application.ApplicationContext;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Singleton;

@Singleton
public class GcubeDefaultS3StorageBackendFactory implements StorageBackendFactory {
	
	private StorageBackend singleton;
	
	private static final String PROP_PREFIX = "default."; 
	
	@PostConstruct
	public void init(){
		S3Backend s3Backend =  new S3Backend(new PayloadBackend(getName(), getParameters()), (String) -> UUID.randomUUID().toString());
		s3Backend.setPayloadConfiguration(new PayloadBackend(getName(),null));
		this.singleton = s3Backend;
	}
	
	@Override
	public String getName() {
		return StorageNames.DEFAULT_S3_STORAGE;
	}
	
	@Override
	public boolean isSystemStorage() {
		return true;
	}

	@Override
	public StorageBackend create(PayloadBackend payloadConfiguration) throws InvalidCallParameters {
		if (payloadConfiguration.getParameters().isEmpty()) 
			return singleton;
		
		throw new InvalidCallParameters("S3 created with not empty parameters");
	}

	private Metadata getParameters(){
		ApplicationContext context = ContextProvider.get();
		String folderPath = context.appSpecificConfigurationFolder().toString();
		try (InputStream input = new FileInputStream(Paths.get(folderPath, "storage-settings.properties").toFile())) {

            Properties prop = new Properties();
           
            prop.load(input);
    		
            Map<String, Object> params = new HashMap<String, Object>();
                
            
            prop.forEach((k,v) -> { if (k.toString().startsWith(PROP_PREFIX)) params.put(k.toString().replace(PROP_PREFIX, ""), v);});
    		
            return new Metadata(params);
    
        } catch (IOException ex) {
            throw new RuntimeException("error initializing S3", ex);
        }
		
		
	}
	
}
