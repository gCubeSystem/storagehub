package org.gcube.data.access.storagehub.services;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.lock.LockException;
import javax.jcr.security.AccessControlManager;
import javax.jcr.security.Privilege;

import org.apache.jackrabbit.api.security.JackrabbitAccessControlList;
import org.apache.jackrabbit.commons.jackrabbit.authorization.AccessControlUtils;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.common.gxrest.response.outbound.GXOutboundErrorResponse;
import org.gcube.common.storagehub.model.Excludes;
import org.gcube.common.storagehub.model.NodeConstants;
import org.gcube.common.storagehub.model.acls.AccessType;
import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.InvalidCallParameters;
import org.gcube.common.storagehub.model.exceptions.InvalidItemException;
import org.gcube.common.storagehub.model.exceptions.ItemLockedException;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.items.FolderItem;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.common.storagehub.model.items.SharedFolder;
import org.gcube.common.storagehub.model.types.NodeProperty;
import org.gcube.common.storagehub.model.types.PrimaryNodeType;
import org.gcube.data.access.storagehub.AuthorizationChecker;
import org.gcube.data.access.storagehub.Constants;
import org.gcube.data.access.storagehub.PathUtil;
import org.gcube.data.access.storagehub.Utils;
import org.gcube.data.access.storagehub.accounting.AccountingHandler;
import org.gcube.data.access.storagehub.handlers.UnshareHandler;
import org.gcube.data.access.storagehub.handlers.items.Item2NodeConverter;
import org.gcube.data.access.storagehub.handlers.items.Node2ItemConverter;
import org.gcube.data.access.storagehub.repository.StoragehubRepository;
import org.gcube.smartgears.utils.InnerMethodName;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.webcohesion.enunciate.metadata.DocumentationExample;
import com.webcohesion.enunciate.metadata.Ignore;
import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResourceMethodSignature;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;


/**
 * Manage item sharing.
 */
@Path("items")
@RequestHeaders({
	  @RequestHeader( name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>"),
	})
public class ItemSharing extends Impersonable{

	private static final Logger log = LoggerFactory.getLogger(ItemSharing.class);

	private final StoragehubRepository repository = StoragehubRepository.repository;

	@Inject 
	AccountingHandler accountingHandler;

	@RequestScoped
	@PathParam("id") 
	String id;

	@Context
	ServletContext context;

	@Inject
	AuthorizationChecker authChecker;

	@Inject
	PathUtil pathUtil;

	@Inject
	UnshareHandler unshareHandler;

	@Inject Node2ItemConverter node2Item;
	@Inject Item2NodeConverter item2Node;

	// TODO: Remove this method - not used by anyone
	@Ignore
	@POST
	@SuppressWarnings("unchecked")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("{id}/share")
	public String shareWithMap(@FormParam("mapUserPermission") String mapUserPermissionString, @FormParam("defaultAccessType") String defaultAccessTypeString){
		InnerMethodName.set("shareFolder");
		HashMap<String,String> mapUserPermission;

		Session ses = null;
		String toReturn = null;
		try{
			try { 
				mapUserPermission = (HashMap<String,String>)new ObjectMapper().readValue(mapUserPermissionString, HashMap.class);
			}catch (Exception e) {
				throw new InvalidCallParameters("invalid map passed");
			}
			AccessType defaultAccessType;
			try { 
				defaultAccessType = AccessType.fromValue(defaultAccessTypeString);
			}catch (IllegalArgumentException e) {
				throw new InvalidCallParameters("invalid default accessType");
			}

			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			authChecker.checkWriteAuthorizationControl(ses, currentUser, id, false);

			Item item = node2Item.getItem(ses.getNodeByIdentifier(id), Excludes.ALL);


			if (mapUserPermission==null || mapUserPermission.isEmpty())
				throw new InvalidCallParameters("users is empty");

			Node nodeToShare = ses.getNodeByIdentifier(id);

			boolean alreadyShared = false;

			Node sharedFolderNode;
			if (!node2Item.checkNodeType(nodeToShare, SharedFolder.class))
				sharedFolderNode= shareFolder(nodeToShare, ses);
			else {
				sharedFolderNode = nodeToShare;
				alreadyShared = true;
			}
			ses.save();

			try {
				ses.getWorkspace().getLockManager().lock(sharedFolderNode.getPath(), true, true, 0,currentUser);
			}catch (LockException e) {
				throw new ItemLockedException(e);
			}
			try {

				AccessControlManager acm = ses.getAccessControlManager();
				JackrabbitAccessControlList acls = AccessControlUtils.getAccessControlList(acm, sharedFolderNode.getPath());

				if (!alreadyShared) {
					Privilege[] adminPrivileges = new Privilege[] { acm.privilegeFromName(AccessType.ADMINISTRATOR.getValue()) };
					addUserToSharing(sharedFolderNode, ses, currentUser, item,  adminPrivileges, acls);
					mapUserPermission.remove(currentUser);
				}


				for (Entry<String, String> entry: mapUserPermission.entrySet() ) 
					try {
						AccessType accessType = defaultAccessType;
						try {
							if (entry.getValue()!=null)
								accessType = AccessType.fromValue(entry.getValue());
						}catch (IllegalArgumentException e) { log.warn("an illegal accessType passed to sharing ({})", entry.getValue());}
						Privilege[] userPrivileges = new Privilege[] { acm.privilegeFromName(accessType.getValue()) };
						addUserToSharing(sharedFolderNode, ses, entry.getKey(), null,  userPrivileges, acls);
					}catch(Exception e){
						log.warn("error adding user {} to sharing of folder {}", entry.getKey(), sharedFolderNode.getName());
					}

				acm.setPolicy(sharedFolderNode.getPath(), acls);

				String title = sharedFolderNode.getProperty(NodeProperty.TITLE.toString()).getString();
				accountingHandler.createShareFolder(title, mapUserPermission.keySet(), ses, sharedFolderNode, currentUser, false);

				ses.save();

				toReturn =  sharedFolderNode.getIdentifier();
			} finally {
				if (!ses.hasPendingChanges())
					ses.getWorkspace().getLockManager().unlock(sharedFolderNode.getPath());
			}

		}catch(RepositoryException re){
			log.error("jcr sharing", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error sharing folder", re));
		}catch(StorageHubException she ){
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		}finally{
			if (ses!=null)
				ses.logout();
		}

		return toReturn;
	}



	/**
	 * Share an item with some users and set its access type.
	 *
	 * @param id                id of the item
	 * @param users             set of users
	 * @param defaultAccessType default access type<br>
	 *    <strong>Possible values:</strong> <code>READ_ONLY</code>, <code>WRITE_OWNER</code>, <code>WRITE_ALL</code>, <code>ADMINISTRATOR</code>
	 * @return id of the shared item
	 * @responseExample text/plain 5f4b3b4e-4b3b- ... -4b3b4e4b3b4e
	 */
	@ResourceMethodSignature(output = String.class, pathParams = { @PathParam("id") }, formParams = {
			@FormParam("users"), @FormParam("defaultAccessType") })
	@DocumentationExample("...\n\n--------boundaryString\n" +
			"Content-Disposition: form-data; name=\"users\"\n\n" +
			"user1\n" +
			"--------boundaryString\n" +
			"Content-Disposition: form-data; name=\"users\"\n\n" +
			"user2\n" +
			"--------boundaryString\n" +
			"Content-Disposition: form-data; name=\"defaultAccessType\"\n\n" +
			"READ_ONLY\n" +
			"------boundaryString--")
	@PUT
	@Path("{id}/share")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	public String share(@FormDataParam("users") Set<String> users, @FormDataParam("defaultAccessType") AccessType accessType){
		InnerMethodName.set("shareFolder");
		Session ses = null;
		String toReturn = null;
		try{
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			authChecker.checkWriteAuthorizationControl(ses, currentUser, id, false);

			Item item = node2Item.getItem(ses.getNodeByIdentifier(id), Excludes.ALL);

			if (accessType==null)
				accessType = AccessType.READ_ONLY;

			if (users==null || users.isEmpty())
				throw new InvalidCallParameters("users is empty");

			log.info("shared method called with users {} and default access type {} ", users, accessType.getValue());

			Node nodeToShare = ses.getNodeByIdentifier(id);

			boolean alreadyShared = false;

			Node sharedFolderNode;
			if (!node2Item.checkNodeType(nodeToShare, SharedFolder.class))
				sharedFolderNode= shareFolder(nodeToShare, ses);
			else {
				sharedFolderNode = nodeToShare;
				alreadyShared = true;
			}
			ses.save();

			try {
				ses.getWorkspace().getLockManager().lock(sharedFolderNode.getPath(), true, true, 0,currentUser);
			}catch (LockException e) {
				throw new ItemLockedException(e);
			}
			try {

				AccessControlManager acm = ses.getAccessControlManager();
				JackrabbitAccessControlList acls = AccessControlUtils.getAccessControlList(acm, sharedFolderNode.getPath());

				if (!alreadyShared) {
					Privilege[] adminPrivileges = new Privilege[] { acm.privilegeFromName(AccessType.ADMINISTRATOR.getValue()) };
					addUserToSharing(sharedFolderNode, ses, currentUser, item,  adminPrivileges, acls);
					users.remove(currentUser);
				}

				Privilege[] userPrivileges = new Privilege[] { acm.privilegeFromName(accessType.getValue()) };
				for (String user : users) 
					try {
						addUserToSharing(sharedFolderNode, ses, user, null,  userPrivileges, acls);
						log.info("added user {} to the shared node",user);
					}catch(Exception e){
						log.warn("error adding user {} to sharing of folder {}", user, sharedFolderNode.getName(),e);
					}

				acm.setPolicy(sharedFolderNode.getPath(), acls);


				accountingHandler.createShareFolder(sharedFolderNode.getProperty(NodeProperty.TITLE.toString()).getString(), users, ses, sharedFolderNode, currentUser, false);

				ses.save();

				toReturn =  sharedFolderNode.getIdentifier();
			} finally {
				if (!ses.hasPendingChanges())
					ses.getWorkspace().getLockManager().unlock(sharedFolderNode.getPath());
			}

		}catch(RepositoryException re){
			log.error("jcr sharing", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error sharing folder", re));
		}catch(StorageHubException she ){
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		}catch(Exception e){
			log.error("jcr sharing", e);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error sharing folder", e));
		}finally{
			if (ses!=null)
				ses.logout();
		}

		return toReturn;
	}



	private Node shareFolder(Node node, Session ses) throws RepositoryException, BackendGenericError, StorageHubException{

		if (!node2Item.checkNodeType(node, FolderItem.class) || Utils.hasSharedChildren(node) || !node.getProperty(NodeProperty.PORTAL_LOGIN.toString()).getString().equals(currentUser))
			throw new InvalidItemException("item with id "+id+" cannot be shared");

		String sharedFolderName = node.getIdentifier();

		String newNodePath = Constants.SHARED_FOLDER_PATH+"/"+sharedFolderName;

		ses.move(node.getPath(),newNodePath);

		Node sharedFolderNode = ses.getNode(newNodePath);

		sharedFolderNode.setPrimaryType(PrimaryNodeType.NT_WORKSPACE_SHARED_FOLDER);

		return sharedFolderNode;
	}

	private void addUserToSharing(Node sharedFolderNode, Session ses, String user, Item itemToShare, Privilege[] userPrivileges, JackrabbitAccessControlList acls) throws RepositoryException{
		try {
			String userRootWSId;
			String userPath;
			if (itemToShare==null) {
				String userRootWS = pathUtil.getWorkspacePath(user).toPath(); 
				userRootWSId = ses.getNode(userRootWS).getIdentifier();
				userPath = String.format("%s%s",userRootWS,sharedFolderNode.getProperty(NodeProperty.TITLE.toString()).getString());
			}
			else {
				userPath = itemToShare.getPath();
				userRootWSId = itemToShare.getParentId();
			}


			log.info("cloning directory to {} ",userPath);

			ses.getWorkspace().clone(ses.getWorkspace().getName(), sharedFolderNode.getPath(), userPath , false);

			acls.addAccessControlEntry(AccessControlUtils.getPrincipal(ses, user), userPrivileges );
			Node usersNode =null; 
			if (sharedFolderNode.hasNode(NodeConstants.USERS_NAME))
				usersNode = sharedFolderNode.getNode(NodeConstants.USERS_NAME);
			else 
				usersNode = sharedFolderNode.addNode(NodeConstants.USERS_NAME);
			usersNode.setProperty(user, String.format("%s/%s",userRootWSId,sharedFolderNode.getProperty(NodeProperty.TITLE.toString()).getString()));
		}catch (Exception e) {
			log.error("error sharing node with user {}",user,e);
			throw new RepositoryException(e);
		}
	}


	/**
	 * Unshare an item with some users.
	 *
	 * @param id                id of the item
	 * @param users             set of users
	 * @return id of the unshared item
	 * @responseExample text/plain 5f4b3b4e-4b3b- ... -4b3b4e4b3b4e
	 */
	@ResourceMethodSignature(output = String.class, pathParams = { @PathParam("id") }, formParams = {
			@FormParam("users") })
	@DocumentationExample("...\n\n--------boundaryString\n" +
			"Content-Disposition: form-data; name=\"users\"\n\n" +
			"user1,user2,user3\n" +
			"--------boundaryString--")
	@PUT
	@Path("{id}/unshare")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	public String unshare(@FormDataParam("users") Set<String> users){
		InnerMethodName.set("unshareFolder");
		Session ses = null;
		String toReturn = null;
		try {
			log.debug("unsharing folder with id {} with users {}", id, users);
			ses = repository.getRepository().login(Constants.JCR_CREDENTIALS);
			Node sharedNode = ses.getNodeByIdentifier(id); 
			toReturn = unshareHandler.unshare(ses, users, sharedNode, currentUser);
			if(toReturn == null ) throw new InvalidItemException("item with id "+id+" cannot be unshared");
		}catch(RepositoryException re){
			log.error("jcr unsharing", re);
			GXOutboundErrorResponse.throwException(new BackendGenericError("jcr error extracting archive", re));
		}catch(StorageHubException she ){
			log.error(she.getErrorMessage(), she);
			GXOutboundErrorResponse.throwException(she, Response.Status.fromStatusCode(she.getStatus()));
		}finally{

			if (ses!=null)
				ses.logout();
		}
		return toReturn;
	}





}
