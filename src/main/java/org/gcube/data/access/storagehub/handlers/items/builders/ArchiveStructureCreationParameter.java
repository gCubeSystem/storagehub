package org.gcube.data.access.storagehub.handlers.items.builders;

import java.io.InputStream;
import java.util.Objects;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

public class ArchiveStructureCreationParameter extends CreateParameters {

	String parentFolderName; 
	FormDataContentDisposition fileDetails;
	InputStream stream;
	
	
	protected ArchiveStructureCreationParameter() {}

	public String getParentFolderName() {
		return parentFolderName;
	}
	
	public FormDataContentDisposition getFileDetails() {
		return fileDetails;
	}

	public InputStream getStream() {
		return stream;
	}

	@Override
	protected boolean isValid() {
		return Objects.nonNull(parentFolderName) && Objects.nonNull(stream);
	}
	
	@Override
	public String toString() {
		return "FileCreationParameters [parentFolder=" + parentFolderName + ", fileDetails=" + fileDetails
				+ ", parentId=" + parentId + ", user=" + user + "]";
	}

	public static ArchiveCreationBuilder builder() {
		return new ArchiveCreationBuilder();
	}
	
	public static class ArchiveCreationBuilder extends ItemsParameterBuilder<ArchiveStructureCreationParameter>{

		private ArchiveCreationBuilder() {
			super(new ArchiveStructureCreationParameter());
		}
		
		public ArchiveCreationBuilder parentName(String name) {
			parameters.parentFolderName = name;
			return this;		
		}
	
		
		public ArchiveCreationBuilder stream(InputStream stream) {
			parameters.stream = stream;
			return this;
		}
		
		public ArchiveCreationBuilder fileDetails(FormDataContentDisposition fileDetails) {
			parameters.fileDetails = fileDetails;
			return this;
		}
		
	}

	@Override
	public ManagedType getMangedType() {
		return ManagedType.ARCHIVE;
	}

	

}