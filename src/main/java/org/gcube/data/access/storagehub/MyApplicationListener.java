package org.gcube.data.access.storagehub;

import org.gcube.data.access.storagehub.repository.StoragehubRepository;
import org.glassfish.jersey.server.monitoring.ApplicationEvent;
import org.glassfish.jersey.server.monitoring.ApplicationEventListener;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.ext.Provider;

@Provider
public class MyApplicationListener implements ApplicationEventListener {

	private static final Logger log = LoggerFactory.getLogger(MyApplicationListener.class);
	
	StoragehubRepository repository = StoragehubRepository.repository;
	
	@Override
	public void onEvent(ApplicationEvent event) {
		switch (event.getType()) {
		case DESTROY_FINISHED:
			log.info("Destroying application storageHub");
			repository.shutdown();
			log.info("Jackrabbit repository stopped");
		default:
			break;
		}
	}

	@Override
	public RequestEventListener onRequest(RequestEvent requestEvent) {
		return null;
	}

	
}