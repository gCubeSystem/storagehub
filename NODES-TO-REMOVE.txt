// TO REMOVE
[nthl:ExternalLink] > nthl:workspaceLeafItem
   - hl:value (String) mandatory 
   
[nthl:externalUrl] > nthl:workspaceLeafItem 

[nthl:query] > nthl:workspaceLeafItem
 	
[nthl:aquamapsItem] > nthl:workspaceLeafItem

[nthl:timeSeriesItem] > nthl:workspaceLeafItem

[nthl:report] > nthl:workspaceLeafItem

[nthl:reportTemplate] > nthl:workspaceLeafItem 

[nthl:workflowReport] > nthl:workspaceLeafItem 

[nthl:workflowTemplate] > nthl:workspaceLeafItem 

[nthl:gCubeMetadata] > nthl:workspaceLeafItem
 
[nthl:gCubeDocument] > nthl:workspaceLeafItem

[nthl:gCubeDocumentLink] > nthl:workspaceLeafItem

[nthl:gCubeImageDocumentLink] > nthl:workspaceLeafItem

[nthl:gCubePDFDocumentLink] > nthl:workspaceLeafItem
	
[nthl:gCubeImageDocument] > nthl:workspaceLeafItem

[nthl:gCubePDFDocument] > nthl:workspaceLeafItem

[nthl:gCubeURLDocument] > nthl:workspaceLeafItem

[nthl:gCubeAnnotation] > nthl:workspaceLeafItem

[nthl:externalResourceLink] > nthl:workspaceLeafItem
 	
[nthl:tabularDataLink] > nthl:workspaceLeafItem   

[nthl:documentAlternativeLink] > nt:base
	- hl:parentUri (String) mandatory
	- hl:uri (String) mandatory
	- hl:name (String) mandatory
	- hl:mimeType (String) mandatory

[nthl:documentPartLink] > nthl:documentAlternativeLink
	 	  
[nthl:documentItemContent] > nthl:workspaceLeafItemContent
  	- hl:collectionName (String) mandatory
	- hl:oid (String) mandatory  
	+ hl:metadata (nt:unstructured)
	= nt:unstructured
	mandatory autocreated
	+ hl:annotations (nt:unstructured)
	= nt:unstructured
	mandatory autocreated
	+ hl:alternatives (nt:unstructured)
	= nt:unstructured
	mandatory autocreated
	+ hl:parts (nt:unstructured)
	= nt:unstructured
	mandatory autocreated
	
[nthl:metadataItemContent] > nthl:workspaceLeafItemContent, nthl:file
	- hl:schema (String) mandatory
	- hl:language (String) mandatory
	- hl:collectionName (String) mandatory
	- hl:oid (String) mandatory
	
[nthl:annotationItemContet] > nthl:workspaceLeafItemContent
	- hl:oid (String) mandatory
	+ hl:annotations (nt:unstructured)
	= nt:unstructured
	mandatory autocreated
	
[nthl:queryItemContent] > nthl:workspaceLeafItemContent
	- hl:query (String) mandatory
	- hl:queryType (String) mandatory
			 	
[nthl:aquamapsItemContent] > nthl:workspaceLeafItemContent, nthl:file
 	- hl:mapName (String) mandatory
	- hl:mapType (String) mandatory
	- hl:author (String) mandatory 
	- hl:numberOfSpecies (Long) mandatory
	- hl:boundingBox (String) mandatory
	- hl:PSOThreshold (Double) mandatory
	- hl:numberOfImages (Long) mandatory
	+ hl:images(nt:unstructured)
	= nt:unstructured
	mandatory autocreated
	
[nthl:timeSeriesItemContent] > nthl:workspaceLeafItemContent, nthl:file
	- hl:id (String) mandatory
	- hl:title (String) mandatory
	- hl:description (String) mandatory
	- hl:creator (String) mandatory
	- hl:created (String) mandatory
	- hl:publisher (String) mandatory
	- hl:sourceId (String) mandatory
	- hl:sourceName (String) mandatory
	- hl:rights (String) mandatory
	- hl:dimension (Long) mandatory
	- hl:headerLabels (String) 	

[nthl:reportItemContent] > nthl:workspaceLeafItemContent, nthl:file
	- hl:created (Date) mandatory
	- hl:lastEdit (Date) mandatory
	- hl:author (String) mandatory
	- hl:lastEditBy (String) mandatory
	- hl:templateName (String) mandatory
	- hl:numberOfSection (Long) mandatory
	- hl:status (String) mandatory

[nthl:reportTemplateContent] > nthl:workspaceLeafItemContent, nthl:file
	- hl:created (Date) mandatory
	- hl:lastEdit (Date) mandatory
	- hl:author (String) mandatory
	- hl:lastEditBy (String) mandatory
	- hl:numberOfSection (Long) mandatory
	- hl:status (String) mandatory

[nthl:externalResourceLinkContent] > nthl:workspaceLeafItemContent
	- hl:mimeType (String) 
	- hl:size (long) mandatory
	- hl:resourceId (String) mandatory
	- hl:servicePlugin (String) mandatory
	
[nthl:tabularDataLinkContent] > nthl:workspaceLeafItemContent
	- hl:tableID (String) mandatory
	- hl:tableTemplateID (String) mandatory
	- hl:provenance (String) mandatory
	- hl:runtimeResourceID (String) mandatory
	- hl:operator (String)
	 	 		
[nthl:smartFolderContent] > nt:base
	- hl:query (String) mandatory
	- hl:folderId (String)
    
[nthl:folderBulkCreator] > nt:base
	- hl:folderId (String) mandatory
	- hl:status (Long) 
	= '0'
	mandatory autocreated
	- hl:failures (Long) 
	= '0'
	mandatory autocreated 
	- hl:requests (Long) mandatory  

[nthl:rootFolderBulkCreator] > nt:folder
	+ * (nthl:folderBulkCreator)
	= nthl:folderBulkCreator
    