Introduction
============

StorageHub is a versatile service designed to provide seamless access
to various storage resources, ensuring data persistence and management. It acts
as an intermediary layer that can interface with any underlying storage
solution, such as Amazon S3 or MongoDB, offering a unified and flexible approach
to storage management.

Base URL
--------

In the production environment, its current value is 
https://api.d4science.org/workspace

Key Features
------------

Flexibility and Integration
~~~~~~~~~~~~~~~~~~~~~~~~~~~

StorageHub is designed to be highly flexible, allowing it to serve as an
intermediate layer for diverse storage solutions. This flexibility ensures that
it can adapt to different storage backends without requiring significant changes
to the applications that rely on it.

RESTful Interface
~~~~~~~~~~~~~~~~~

StorageHub exposes a RESTful API, which allows any application capable of making
HTTP requests to access it. This REST interface provides a standardized way to
interact with the storage resources, enabling easy integration with various
applications and services. See the available REST-API on `StorageHub API docs
<../api-docs/index.html>`_.

Metadata Management
~~~~~~~~~~~~~~~~~~~

StorageHub leverages a JackRabbit-based object store to manage all metadata
associated with the stored data. This ensures that metadata is efficiently
organized and easily retrievable, enhancing the overall data management
capabilities of the service.

Direct Payload Storage
~~~~~~~~~~~~~~~~~~~~~~

While metadata is handled by JackRabbit, the actual data payloads are stored
directly on the underlying storage solutions. This approach optimizes storage
efficiency and performance, ensuring that large data payloads are managed
effectively.

Primary Use Cases
-----------------

Workspace
~~~~~~~~~

The main application that interacts with StorageHub is the Workspace portlet,
which is easily accessible from the Virtual Research Environments (VREs). The
Workspace provides a "standard" HTML interface where users can perform all the
common operations available in a file system, such as creating, reading,
updating, and deleting files and directories.

In addition to these standard file system operations, the Workspace offers
features that are specific to VREs. These include publishing on the Catalogue,
sharing resources with other users, and managing versions of files. These
capabilities make the Workspace a versatile tool for managing data within the
VREs, leveraging the services provided by StorageHub.

Java Client
~~~~~~~~~~~

The methods of the Web Service can be called by writing your own REST client
application or by using already existing REST client plugins.

In case of a Java client, we provide the StorageHub Client Library, which is a
Java library designed to facilitate seamless interaction with StorageHub. It
abstracts the complexities of the REST API, providing a more intuitive and
convenient interface for Java developers.

The StorageHub Client Library allows developers to easily integrate StorageHub's
capabilities into their applications without dealing with the intricacies of
HTTP requests and responses. The library handles all the necessary communication
with StorageHub, allowing developers to focus on their application's core
functionality.

.. tip:: If you're coding in Java, it is recommended that you include the
   StorageHub Client Library into your project.

Authorization
-------------

D4Science adopts state-of-the-art industry standards for authentication and
authorization. Specifically, the implementation fully adopts `OIDC (OpenID
Connect) <https://openid.net/connect>`_ for authentication and UMA 2 (User
Managed Authorization) for authorization flows. `JSON Web Token (JWT) Access
token <https://jwt.io/>`_ are used for both authentication and authorization.

Obtain your Bearer token here: https://dev.d4science.org/how-to-access-resources
