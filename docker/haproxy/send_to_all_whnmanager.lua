local function broadcast_to_nodes(req)
    -- Get all servers in the backend
    local servers = pxn.get_servers("all")

    for _, server in ipairs(servers) do
        -- Forward the request to each server
        pxn.http_request({
            "PUT",                         -- Method
            server["address"],              -- Address of the server
            tonumber(server["port"]),       -- Port of the server
            req.method,                    -- HTTP method
            req.uri,                       -- URI
            req.headers,                   -- Headers
            req.body                       -- Body
        })
    end
        
 end

 core.register_service("broadcast_to_nodes", "http", broadcast_to_nodes)
    