#!/bin/bash

POSTGRES_USER=$1
POSTGRES_DB=$2

while [ ! -f /var/lib/postgresql/data/__init_done ]; do
    echo "Postgres initialization not done yet..."
    sleep 5
done

# Check PostgreSQL server status
pg_isready -U "${POSTGRES_USER}" -d "${POSTGRES_DB}"
status=$?

if [ $status -eq 0 ]; then
    echo "PostgreSQL is ready to accept connections."
elif [ $status -eq 1 ]; then
    echo "PostgreSQL is rejecting connections."
elif [ $status -eq 2 ]; then
    echo "Error in connection attempt."
else
    echo "PostgreSQL server is not running."
fi

exit $status