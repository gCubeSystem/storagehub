#!/bin/bash

set -e

NAME=storagehub
PORT=8100
DEBUG_PORT=5005
debug=false
compile=false

mvn clean package

docker compose -f docker-compose-standalone.yml build

docker compose -f docker-compose-standalone.yml up
